<?php

class m161103_093322_rules extends CDbMigration
{
	public function up(){
	    $this->execute("ALTER TABLE `user` CHANGE `role` `role` ENUM('admin','editor','super_admin') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'editor';ALTER TABLE `user` CHANGE `background_color` `background_color` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'skin-blue';ALTER TABLE `user` CHANGE `deleted` `deleted` TINYINT(1) NOT NULL DEFAULT '0';

");
	}

	public function down()
	{
		echo "m161103_093322_rules does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}