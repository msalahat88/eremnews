<?php

class m160831_125922_page_index extends CDbMigration
{
	public function up()
	{
	    $this->execute('ALTER TABLE `category` ADD `page_index` ENUM(\'main\',\'other\') NULL DEFAULT NULL AFTER `title`;
');
	}

	public function down()
	{
		echo "m160831_125922_page_index does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}