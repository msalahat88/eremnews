<?php

class m161023_091754_data_category_level extends CDbMigration
{
	public function up()
	{
	    $this->execute('INSERT INTO `category_level` (`id`, `source_id`, `predication`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 1, \'main[id=main] figure[class=entry-image-container] a\', \'2016-08-31 14:09:19\', \'2016-09-07 12:50:32\', 1, 1),
(2, 2, \'main[id=main] figure[class=entry-image-container] a\', \'2016-08-31 14:09:19\', \'2016-09-07 12:50:36\', 1, 1),
(3, 3, \'main[id=main] figure[class=entry-image-container] a\', \'2016-08-31 14:09:19\', \'2016-09-07 12:50:43\', 1, 1),
(4, 4, \'main[id=main] figure[class=entry-image-container] a\', \'2016-08-31 14:09:20\', \'2016-09-07 12:50:48\', 1, 1),
(5, 5, \'main[id=main] figure[class=entry-image-container] a\', \'2016-08-31 14:09:20\', \'2016-09-07 12:51:06\', 1, 1),
(6, 6, \'main[id=main] figure[class=entry-image-container] a\', \'2016-08-31 14:09:20\', \'2016-09-07 12:51:00\', 1, 1),
(7, 7, \'main[id=main] figure[class=entry-image-container] a\', \'2016-08-31 14:09:20\', \'2016-09-07 12:51:12\', 1, 1),
(8, 8, \'main[id=main] figure[class=entry-image-container] a\', \'2016-08-31 14:09:20\', \'2016-09-07 12:51:16\', 1, 1),
(9, 11, \'div[id=popular-posts-2] div[class=widget-content] a[class=entry-title]\', \'2016-09-04 17:13:28\', \'2016-09-04 20:49:55\', 1, 1);');
	}

	public function down()
	{
		echo "m161023_091754_data_category_level does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}