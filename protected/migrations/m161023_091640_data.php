<?php

class m161023_091640_data extends CDbMigration
{
	public function up()
	{
	    $this->execute('INSERT INTO `category` (`id`, `title`, `page_index`, `lang`, `url`, `deleted`, `active`, `type`, `url_rss`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, \'أخبار\', \'main\', \'ar\', \'http://www.eremnews.com/category/news\', 0, 1, \'dom\', NULL, \'2016-08-31 14:02:20\', \'2016-10-04 08:11:44\', 1, 1),
(2, \'اقتصاد\', \'main\', \'ar\', \'http://www.eremnews.com/category/economy\', 0, 1, \'dom\', NULL, \'2016-08-31 14:03:08\', \'2016-10-23 10:15:38\', 1, 1),
(3, \'منوعات\', \'main\', \'ar\', \'http://www.eremnews.com/category/entertainment\', 0, 1, \'dom\', NULL, \'2016-08-31 14:03:37\', \'2016-10-23 10:15:41\', 1, 1),
(4, \'علوم وتقنية\', \'main\', \'ar\', \'http://www.eremnews.com/category/sciences-technology\', 0, 1, \'dom\', NULL, \'2016-08-31 14:03:56\', \'2016-10-23 10:15:41\', 1, 1),
(5, \'السعودية\', \'main\', \'ar\', \'http://www.eremnews.com/category/news/arab-word/saudi-arabia\', 0, 1, \'dom\', NULL, \'2016-08-31 14:05:18\', \'2016-10-23 10:15:39\', 1, 1),
(6, \'الخليج\', \'main\', \'ar\', \'http://www.eremnews.com/category/news/arab-word/gcc\', 0, 1, \'dom\', NULL, \'2016-08-31 14:05:46\', \'2016-10-23 10:15:38\', 1, 1),
(7, \'اليمن\', \'main\', \'ar\', \'http://www.eremnews.com/category/news/arab-word/yemen\', 0, 1, \'dom\', NULL, \'2016-08-31 14:06:16\', \'2016-10-23 10:15:40\', 1, 1),
(8, \'رياضة\', \'other\', \'ar\', \'http://www.eremnews.com/category/sports\', 0, 1, \'dom\', NULL, \'2016-08-31 14:06:38\', \'2016-09-18 10:42:10\', 1, 1),
(11, \'رياضة\', \'main\', \'ar\', \'http://www.eremnews.com/\', 0, 1, \'dom\', NULL, \'2016-09-04 17:11:56\', \'2016-09-18 10:42:11\', 1, 1);
');
	}

	public function down()
	{
		echo "m161023_091640_data does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}