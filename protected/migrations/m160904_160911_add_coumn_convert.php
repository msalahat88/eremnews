<?php

class m160904_160911_add_coumn_convert extends CDbMigration
{
	public function up()
	{
	    $this->execute("ALTER TABLE `media_news` ADD `converted` BOOLEAN NULL DEFAULT FALSE AFTER `media`;ALTER TABLE `post_queue` ADD `template_id` INT NULL DEFAULT NULL AFTER `pinned`;");
	}

	public function down()
	{
		echo "m160904_160911_add_coumn_convert does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}