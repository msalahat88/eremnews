<?php

class m160901_133757_cover_profile extends CDbMigration
{
	public function up()
	{
	    $this->execute('ALTER TABLE `cover_photo` ADD `page_index` ENUM(\'main\', \'other\') NULL DEFAULT NULL AFTER `title`;
ALTER TABLE `profile_pic` ADD `page_index` ENUM(\'main\', \'other\') NULL DEFAULT NULL AFTER `title`

');
	}

	public function down()
	{
		echo "m160901_133757_cover_profile does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}