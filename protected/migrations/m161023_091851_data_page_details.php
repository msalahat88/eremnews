<?php

class m161023_091851_data_page_details extends CDbMigration
{
	public function up()
	{
	    $this->execute('INSERT INTO `page_details` (`id`, `source_id`, `page_type_id`, `predication`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 1, 1, \'div[itemprop=author] span[itemprop=name]\', \'2016-08-31 14:12:00\', NULL, 1, NULL),
(2, 2, 1, \'div[itemprop=author] span[itemprop=name]\', \'2016-08-31 14:12:00\', NULL, 1, NULL),
(3, 3, 1, \'div[itemprop=author] span[itemprop=name]\', \'2016-08-31 14:12:01\', NULL, 1, NULL),
(4, 4, 1, \'div[itemprop=author] span[itemprop=name]\', \'2016-08-31 14:12:01\', NULL, 1, NULL),
(5, 5, 1, \'div[itemprop=author] span[itemprop=name]\', \'2016-08-31 14:12:01\', NULL, 1, NULL),
(6, 6, 1, \'div[itemprop=author] span[itemprop=name]\', \'2016-08-31 14:12:01\', NULL, 1, NULL),
(7, 7, 1, \'div[itemprop=author] span[itemprop=name]\', \'2016-08-31 14:12:01\', NULL, 1, NULL),
(8, 8, 1, \'div[itemprop=author] span[itemprop=name]\', \'2016-08-31 14:12:01\', NULL, 1, NULL),
(9, 1, 4, \'meta[property=og:title]\', \'2016-08-31 14:13:44\', NULL, 1, NULL),
(10, 2, 4, \'meta[property=og:title]\', \'2016-08-31 14:13:44\', NULL, 1, NULL),
(11, 3, 4, \'meta[property=og:title]\', \'2016-08-31 14:13:44\', NULL, 1, NULL),
(12, 4, 4, \'meta[property=og:title]\', \'2016-08-31 14:13:45\', NULL, 1, NULL),
(13, 5, 4, \'meta[property=og:title]\', \'2016-08-31 14:13:45\', NULL, 1, NULL),
(14, 6, 4, \'meta[property=og:title]\', \'2016-08-31 14:13:45\', NULL, 1, NULL),
(15, 7, 4, \'meta[property=og:title]\', \'2016-08-31 14:13:45\', NULL, 1, NULL),
(16, 8, 4, \'meta[property=og:title]\', \'2016-08-31 14:13:45\', NULL, 1, NULL),
(17, 1, 6, \'meta[name=twitter:description]\', \'2016-08-31 14:15:15\', NULL, 1, NULL),
(18, 2, 6, \'meta[name=twitter:description]\', \'2016-08-31 14:15:15\', NULL, 1, NULL),
(19, 3, 6, \'meta[name=twitter:description]\', \'2016-08-31 14:15:15\', NULL, 1, NULL),
(20, 4, 6, \'meta[name=twitter:description]\', \'2016-08-31 14:15:15\', NULL, 1, NULL),
(21, 5, 6, \'meta[name=twitter:description]\', \'2016-08-31 14:15:16\', NULL, 1, NULL),
(22, 6, 6, \'meta[name=twitter:description]\', \'2016-08-31 14:15:16\', NULL, 1, NULL),
(23, 7, 6, \'meta[name=twitter:description]\', \'2016-08-31 14:15:16\', NULL, 1, NULL),
(24, 8, 6, \'meta[name=twitter:description]\', \'2016-08-31 14:15:16\', NULL, 1, NULL),
(25, 1, 6, \'div[itemprop=articleBody] p span\', \'2016-08-31 14:17:09\', NULL, 1, NULL),
(26, 3, 6, \'div[itemprop=articleBody] p span\', \'2016-08-31 14:17:09\', NULL, 1, NULL),
(27, 4, 6, \'div[itemprop=articleBody] p span\', \'2016-08-31 14:17:09\', NULL, 1, NULL),
(28, 6, 6, \'div[itemprop=articleBody] p span\', \'2016-08-31 14:17:09\', NULL, 1, NULL),
(29, 7, 6, \'div[itemprop=articleBody] p span\', \'2016-08-31 14:17:09\', NULL, 1, NULL),
(30, 8, 6, \'div[itemprop=articleBody] p span\', \'2016-08-31 14:17:09\', NULL, 1, NULL),
(31, 1, 6, \'h4[itemprop=alternativeHeadline]\', \'2016-08-31 14:18:38\', NULL, 1, NULL),
(32, 2, 6, \'h4[itemprop=alternativeHeadline]\', \'2016-08-31 14:18:38\', NULL, 1, NULL),
(33, 3, 6, \'h4[itemprop=alternativeHeadline]\', \'2016-08-31 14:18:38\', NULL, 1, NULL),
(34, 4, 6, \'h4[itemprop=alternativeHeadline]\', \'2016-08-31 14:18:38\', NULL, 1, NULL),
(35, 5, 6, \'h4[itemprop=alternativeHeadline]\', \'2016-08-31 14:18:38\', NULL, 1, NULL),
(36, 6, 6, \'h4[itemprop=alternativeHeadline]\', \'2016-08-31 14:18:39\', NULL, 1, NULL),
(37, 7, 6, \'h4[itemprop=alternativeHeadline]\', \'2016-08-31 14:18:39\', NULL, 1, NULL),
(38, 8, 6, \'h4[itemprop=alternativeHeadline]\', \'2016-08-31 14:18:39\', NULL, 1, NULL),
(39, 1, 7, \'div[itemprop=articleBody] p \', \'2016-08-31 14:20:45\', NULL, 1, NULL),
(40, 2, 7, \'div[itemprop=articleBody] p \', \'2016-08-31 14:20:45\', NULL, 1, NULL),
(41, 3, 7, \'div[itemprop=articleBody] p \', \'2016-08-31 14:20:46\', NULL, 1, NULL),
(42, 4, 7, \'div[itemprop=articleBody] p \', \'2016-08-31 14:20:46\', NULL, 1, NULL),
(43, 5, 7, \'div[itemprop=articleBody] p \', \'2016-08-31 14:20:46\', NULL, 1, NULL),
(44, 6, 7, \'div[itemprop=articleBody] p \', \'2016-08-31 14:20:46\', NULL, 1, NULL),
(45, 7, 7, \'div[itemprop=articleBody] p \', \'2016-08-31 14:20:46\', NULL, 1, NULL),
(46, 8, 7, \'div[itemprop=articleBody] p \', \'2016-08-31 14:20:46\', NULL, 1, NULL),
(47, 1, 8, \'meta[property=article:published_time]\', \'2016-08-31 14:24:03\', NULL, 1, NULL),
(48, 2, 8, \'meta[property=article:published_time]\', \'2016-08-31 14:24:03\', NULL, 1, NULL),
(49, 3, 8, \'meta[property=article:published_time]\', \'2016-08-31 14:24:03\', NULL, 1, NULL),
(50, 4, 8, \'meta[property=article:published_time]\', \'2016-08-31 14:24:03\', NULL, 1, NULL),
(51, 5, 8, \'meta[property=article:published_time]\', \'2016-08-31 14:24:03\', NULL, 1, NULL),
(52, 6, 8, \'meta[property=article:published_time]\', \'2016-08-31 14:24:03\', NULL, 1, NULL),
(53, 7, 8, \'meta[property=article:published_time]\', \'2016-08-31 14:24:03\', NULL, 1, NULL),
(54, 8, 8, \'meta[property=article:published_time]\', \'2016-08-31 14:24:03\', NULL, 1, NULL),
(55, 1, 9, \'meta[property=og:image]\', \'2016-08-31 14:26:37\', NULL, 1, NULL),
(56, 2, 9, \'meta[property=og:image]\', \'2016-08-31 14:26:37\', NULL, 1, NULL),
(57, 3, 9, \'meta[property=og:image]\', \'2016-08-31 14:26:37\', NULL, 1, NULL),
(58, 4, 9, \'meta[property=og:image]\', \'2016-08-31 14:26:37\', NULL, 1, NULL),
(59, 5, 9, \'meta[property=og:image]\', \'2016-08-31 14:26:38\', NULL, 1, NULL),
(60, 6, 9, \'meta[property=og:image]\', \'2016-08-31 14:26:38\', NULL, 1, NULL),
(61, 7, 9, \'meta[property=og:image]\', \'2016-08-31 14:26:38\', NULL, 1, NULL),
(62, 8, 9, \'meta[property=og:image]\', \'2016-08-31 14:26:38\', NULL, 1, NULL),
(63, 1, 11, \'div[itemprop=articleBody] p img\', \'2016-08-31 14:45:55\', NULL, 1, NULL),
(64, 2, 11, \'div[itemprop=articleBody] p img\', \'2016-08-31 14:45:55\', NULL, 1, NULL),
(65, 3, 11, \'div[itemprop=articleBody] p img\', \'2016-08-31 14:45:55\', NULL, 1, NULL),
(66, 4, 11, \'div[itemprop=articleBody] p img\', \'2016-08-31 14:45:55\', NULL, 1, NULL),
(67, 5, 11, \'div[itemprop=articleBody] p img\', \'2016-08-31 14:45:55\', NULL, 1, NULL),
(68, 6, 11, \'div[itemprop=articleBody] p img\', \'2016-08-31 14:45:56\', NULL, 1, NULL),
(69, 7, 11, \'div[itemprop=articleBody] p img\', \'2016-08-31 14:45:56\', NULL, 1, NULL),
(70, 8, 11, \'div[itemprop=articleBody] p img\', \'2016-08-31 14:45:56\', NULL, 1, NULL),
(73, 1, 10, \'object[type=application/x-shockwave-flash]\', \'2016-08-31 15:19:44\', NULL, 1, NULL),
(74, 2, 10, \'object[type=application/x-shockwave-flash]\', \'2016-08-31 15:19:44\', NULL, 1, NULL),
(75, 8, 10, \'object[type=application/x-shockwave-flash]\', \'2016-08-31 15:19:45\', NULL, 1, NULL),
(76, 1, 10, \'iframe\', \'2016-08-31 15:21:03\', NULL, 1, NULL),
(77, 2, 10, \'iframe\', \'2016-08-31 15:21:03\', NULL, 1, NULL),
(78, 3, 10, \'iframe\', \'2016-08-31 15:21:03\', NULL, 1, NULL),
(79, 4, 10, \'iframe\', \'2016-08-31 15:21:03\', NULL, 1, NULL),
(80, 5, 10, \'iframe\', \'2016-08-31 15:21:03\', NULL, 1, NULL),
(81, 6, 10, \'iframe\', \'2016-08-31 15:21:03\', NULL, 1, NULL),
(82, 7, 10, \'iframe\', \'2016-08-31 15:21:03\', NULL, 1, NULL),
(83, 8, 10, \'iframe\', \'2016-08-31 15:21:03\', NULL, 1, NULL),
(84, 11, 1, \'div[itemprop=author] span[itemprop=name]	\', \'2016-09-04 17:14:02\', NULL, 1, NULL),
(85, 11, 4, \'meta[property=og:title]	\', \'2016-09-04 17:14:17\', NULL, 1, NULL),
(86, 11, 6, \'meta[name=twitter:description]	\', \'2016-09-04 17:14:36\', NULL, 1, NULL),
(87, 11, 7, \'div[itemprop=articleBody] p	\', \'2016-09-04 17:15:25\', NULL, 1, NULL),
(88, 11, 8, \'meta[property=article:published_time]	\', \'2016-09-04 17:15:39\', NULL, 1, NULL),
(89, 11, 9, \'meta[property=og:image]	\', \'2016-09-04 17:15:54\', NULL, 1, NULL),
(90, 11, 9, \'div[itemprop=articleBody] p img	\', \'2016-09-04 17:16:07\', NULL, 1, NULL),
(91, 11, 11, \'div[itemprop=articleBody] p img	\', \'2016-09-04 17:16:47\', NULL, 1, NULL),
(101, 1, 13, \'div[class=entry-tags] a\', \'2016-09-06 12:49:17\', NULL, 1, NULL),
(102, 2, 13, \'div[class=entry-tags] a\', \'2016-09-06 12:49:17\', NULL, 1, NULL),
(103, 3, 13, \'div[class=entry-tags] a\', \'2016-09-06 12:49:17\', NULL, 1, NULL),
(104, 4, 13, \'div[class=entry-tags] a\', \'2016-09-06 12:49:17\', NULL, 1, NULL),
(105, 5, 13, \'div[class=entry-tags] a\', \'2016-09-06 12:49:17\', NULL, 1, NULL),
(106, 6, 13, \'div[class=entry-tags] a\', \'2016-09-06 12:49:17\', NULL, 1, NULL),
(107, 7, 13, \'div[class=entry-tags] a\', \'2016-09-06 12:49:17\', NULL, 1, NULL),
(108, 8, 13, \'div[class=entry-tags] a\', \'2016-09-06 12:49:17\', NULL, 1, NULL),
(109, 11, 13, \'div[class=entry-tags] a\', \'2016-09-06 12:49:17\', NULL, 1, NULL),
(110, 1, 11, \'div[id=carousel] ul[class=slides] li a img\', \'2016-09-06 16:05:34\', NULL, 1, NULL),
(111, 2, 11, \'div[id=carousel] ul[class=slides] li a img\', \'2016-09-06 16:05:34\', NULL, 1, NULL),
(112, 3, 11, \'div[id=carousel] ul[class=slides] li a img\', \'2016-09-06 16:05:34\', NULL, 1, NULL),
(113, 4, 11, \'div[id=carousel] ul[class=slides] li a img\', \'2016-09-06 16:05:34\', NULL, 1, NULL),
(114, 5, 11, \'div[id=carousel] ul[class=slides] li a img\', \'2016-09-06 16:05:34\', NULL, 1, NULL),
(115, 6, 11, \'div[id=carousel] ul[class=slides] li a img\', \'2016-09-06 16:05:34\', NULL, 1, NULL),
(116, 7, 11, \'div[id=carousel] ul[class=slides] li a img\', \'2016-09-06 16:05:34\', NULL, 1, NULL),
(117, 8, 11, \'div[id=carousel] ul[class=slides] li a img\', \'2016-09-06 16:05:34\', NULL, 1, NULL);');
	}

	public function down()
	{
		echo "m161023_091851_data_page_details does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}