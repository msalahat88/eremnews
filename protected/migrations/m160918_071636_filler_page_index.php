<?php

class m160918_071636_filler_page_index extends CDbMigration
{
	public function up()
	{
	    $this->execute('ALTER TABLE `filler_data` ADD `page_index` ENUM(\'main\',\'other\') NOT NULL DEFAULT \'main\' AFTER `type`;
');
	}

	public function down()
	{
		echo "m160918_071636_filler_page_index does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}