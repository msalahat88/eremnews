<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class BaseController extends CController
{
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/main';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();
    public $data_search = null;

    public $creator;
    public $sub_category;
    public $source;
    public $news;
    public $PostQueue;
    public $trim_hash;
    public $hash_tag;

    public $details;
    public $category;
    public $model;
    public $platform;
    public $parent = null;
    public $array_erem_words = array('| إرم نيوز','#إرم_نيوز','|إرم نيوز','| إرم نيوز');

    public function beforeAction($action) {

        if($action->getId() == 'mainUn'){
            Yii::app()->session['mains']='postQueue/mainUn';
        }elseif($action->getId() == 'main'){
            Yii::app()->session['mains']='postQueue/main';
        }elseif($action->getId() == 'mainPinned'){
            Yii::app()->session['mains']='postQueue/mainPinned';
        }elseif($action->getId() == 'mainPosted'){
            Yii::app()->session['mains']='postQueue/mainPosted';
        }

        if($action->getId() != 'login' && !isset($_POST['LoginForm'])){
            if (Yii::app()->user->isGuest){
                $this->redirect(array('/site/login'));
                return false;
            }else{
                return true;
            }
        }else{

            Yii::import("application.modules.settings.models.Settings", true);

            $settings = Settings::model()->findByPk(1);
            if(!empty($settings))
                Yii::app()->timeZone  = $settings->timezone;
            else
                Yii::app()->timeZone = 'Asia/Dubai';
            if(!empty($data))
                Yii::app()->timeZone = empty($data->timezone)?'Asia/Dubai':$data->timezone;
            return true;
        }
    }

    public function bitShort($url){
        $d = Yii::app()->bitly->shorten($url)->getResponseData();
        $d = CJSON::decode($d);
        if(isset($d['status_code']) && $d['status_code'] == 200){
            if(isset($d['data'])){
                $data = $d['data'];
                if(isset($data['url'])){
                    return $data['url'];
                }
            }
        }

        return null;
    }


    public function GoogleShort($url){

        $shortUrl = new ShortUrl();

        $url_short =  $shortUrl->short(urldecode($url));

        if(!empty($url_short)){
            return $url_short;
        }

        return null;
    }

    public function data_search($model){

        $model->visible_id = isset($this->data_search['visible_id'])?$this->data_search['visible_id']:$model->visible_id;
        $model->visible_url = isset($this->data_search['visible_url'])?$this->data_search['visible_url']:$model->visible_url;
        $model->visible_title = isset($this->data_search['visible_title'])?$this->data_search['visible_title']:$model->visible_title;
        $model->visible_created_at = isset($this->data_search['visible_created_at'])?$this->data_search['visible_created_at']:$model->visible_created_at ;
        $model->visible_active = isset($this->data_search['visible_active'])?$this->data_search['visible_active']:$model->visible_active ;
        $model->visible_created_by = isset($this->data_search['visible_created_by'])?$this->data_search['visible_created_by']:$model->visible_created_by ;
        $model->visible_updated_by = isset($this->data_search['visible_updated_by'])?$this->data_search['visible_updated_by']:$model->visible_updated_by ;
        $model->visible_created_at = isset($this->data_search['visible_created_at'])?$this->data_search['visible_created_at']:$model->visible_created_at ;
        $model->visible_updated_at = isset($this->data_search['visible_updated_at'])?$this->data_search['visible_updated_at']:$model->visible_updated_at ;
        $model->visible_category_id = isset($this->data_search['visible_category_id'])?$this->data_search['visible_category_id']:$model->visible_category_id ;
        $model->visible_f_name = isset($this->data_search['visible_f_name'])?$this->data_search['visible_f_name']:$model->visible_f_name ;
        $model->visible_l_name = isset($this->data_search['visible_l_name'])?$this->data_search['visible_l_name']:$model->visible_l_name ;
        $model->visible_email = isset($this->data_search['visible_email'])?$this->data_search['visible_email']:$model->visible_email ;
        $model->visible_name = isset($this->data_search['visible_name'])?$this->data_search['visible_name']:$model->visible_name ;
        $model->visible_username = isset($this->data_search['visible_username'])?$this->data_search['visible_username']:$model->visible_username ;
        $model->visible_gender = isset($this->data_search['visible_gender'])?$this->data_search['visible_gender']:$model->visible_gender ;
        $model->visible_profile_pic = isset($this->data_search['visible_profile_pic'])?$this->data_search['visible_profile_pic']:$model->visible_profile_pic ;
        $model->visible_background_color = isset($this->data_search['visible_background_color'])?$this->data_search['visible_background_color']:$model->visible_background_color ;
        $model->visible_from_date = isset($this->data_search['visible_from_date'])?$this->data_search['visible_from_date']:$model->visible_from_date ;
        $model->visible_to_date = isset($this->data_search['visible_to_date'])?$this->data_search['visible_to_date']:$model->visible_to_date ;
        $model->visible_media_url = isset($this->data_search['visible_media_url'])?$this->data_search['visible_media_url']:$model->visible_media_url ;
        $model->visible_schedule_date = isset($this->data_search['visible_schedule_date'])?$this->data_search['visible_schedule_date']:$model->visible_schedule_date ;
        $model->visible_platform_id = isset($this->data_search['visible_platform_id'])?$this->data_search['visible_platform_id']:$model->visible_platform_id ;
        $model->visible_type = isset($this->data_search['visible_type'])?$this->data_search['visible_type']:$model->visible_type ;
        $model->visible_text = isset($this->data_search['visible_text'])?$this->data_search['visible_text']:$model->visible_text ;
        $model->visible_start_date = isset($this->data_search['visible_start_date'])?$this->data_search['visible_start_date']:$model->visible_start_date ;
        $model->visible_end_date = isset($this->data_search['visible_end_date'])?$this->data_search['visible_end_date']:$model->visible_end_date ;
        $model->visible_last_generated= isset($this->data_search['visible_last_generated'])?$this->data_search['visible_last_generated']:$model->visible_last_generated ;
        $model->visible_media_url= isset($this->data_search['visible_media_url'])?$this->data_search['visible_media_url']:$model->visible_media_url ;
        $model->visible_publish_time= isset($this->data_search['visible_publish_time'])?$this->data_search['visible_publish_time']:$model->visible_publish_time ;
        $model->visible_catgory_id= isset($this->data_search['visible_catgory_id'])?$this->data_search['visible_catgory_id']:$model->visible_catgory_id ;
        $model->visible_is_posted= isset($this->data_search['visible_is_posted'])?$this->data_search['visible_is_posted']:$model->visible_is_posted ;

    }

    public function download_send_headers($filename) {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }

    public function uploader($model, $field_name, $name)
    {
        $uploadedFile = CUploadedFile::getInstance($model, $field_name);
        $s3 = new S3(Yii::app()->params['amazonAccessKey'], Yii::app()->params['amazonSecretKey']);
        $path = $uploadedFile->name;
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $s3->putBucket(Yii::app()->params['amazonBucket'], S3::ACL_PUBLIC_READ);
        $filename = $name . '.' . $ext;
        $fileUrl = false;
        if ($s3->putObjectFile($uploadedFile->tempName, Yii::app()->params['amazonBucket'], Yii::app()->params['amazonFolder'] . '/' . $filename, S3::ACL_PUBLIC_READ)) {
            $fileUrl = 'http://s3.amazonaws.com/' . Yii::app()->params['amazonBucket'] . '/' . Yii::app()->params['amazonFolder'] . '/' . $filename;
        }

        return $fileUrl;
    }

    public function uploaderFile($file,$name){
        Yii::import('application.components.S3');
        if(!empty($file)){
            $s3 = new S3(Yii::app()->params['amazonAccessKey'], Yii::app()->params['amazonSecretKey']);
            $path = $file;
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $s3->putBucket(Yii::app()->params['amazonBucket'], S3::ACL_PUBLIC_READ);
            $filename=$name.time().'.'.$ext;
            $fileUrl='';
            if ($s3->putObjectFile($file, Yii::app()->params['amazonBucket'], Yii::app()->params['amazonFolder'].'/'.$filename , S3::ACL_PUBLIC_READ)) {
                $fileUrl = 'http://s3.amazonaws.com/'.Yii::app()->params['amazonBucket'].'/'.Yii::app()->params['amazonFolder'].'/'.$filename;
            }
            unlink($file);
            return array(true,$fileUrl);
        }else{
            return array(false,'');
        }
    }

    public function DownloaderYoutube($url, $form)
    {
        $rx = '~^(?:https?://)?(?:www\.)?(?:youtube\.com|youtu\.be)/watch\?v=([^&]+)~x';

        if(preg_match($rx,$url, $matches)){
            $download_video_url = "'$url'";
            exec("youtube-dl -F $download_video_url", $out);
            if (!empty($out)) {

                if (isset(explode(' ', $out[count($out) - 1])[0])) {
                    $id = explode(' ', $out[count($out) - 1])[0];
                    $path = Yii::app()->params['webroot'] . '/video/youtube_' . $form . '_' . time() . '.mp4';
                    exec("youtube-dl -o '$path' $download_video_url -f '$id'", $output);
                    if (strpos(end($output), '100%') !== false) {
                        return array(true,$path);
                    } else {
                        if (is_dir($path))
                           // unlink($path);
                        return array( false,'fail');
                    }
                } else
                    return array(false,'url');
            } else
                return array(false, 'url');

        }
        return array(false,'url');

    }

    public function FileEmpty($model, $field_name)
    {
        $uploadedFile = CUploadedFile::getInstance($model, $field_name);
        if (!empty($uploadedFile)) {
            return true;
        } else {
            return false;
        }
    }

    public function FileIsExist($Url)
    {
        return file_exists($Url);
    }

    public function FileType($model, $field_name, $type, $file = null)
    {

        if ($file == null) {
            $file = explode('.', $file);
            $ext = strtolower(end($file));
            if ($ext == 'png' or $ext == 'jpg') {
                return 'image';
            } elseif ($ext == 'mp4') {
                return 'video';
            }
            return false;

        } else {
            $uploadedFile = CUploadedFile::getInstance($model, $field_name);
            if (!empty($uploadedFile)) {
                $upload_type = strtolower($uploadedFile->extensionName);
                if ($type == 'image') {
                    if (!($upload_type == 'png' or $upload_type == 'jpg')) {
                        return false;
                    }
                    return true;
                } elseif ($type == 'video') {
                    if (!($upload_type == 'mp4')) {
                        return false;
                    }
                    return true;
                } else {
                    return false;
                }

            } else {
                return false;
            }
        }
    }

    public function GetFileType($model, $field_name, $type, $file = null)
    {

        $valid = true;

        if($file == null){
            $ex= explode('.', $field_name);
            $ext = strtolower(end($ex));
        }
        else{
            $uploadedFile = CUploadedFile::getInstance($model, $field_name);
            $ext = strtolower($uploadedFile->extensionName);
        }
        if ($type == 'image') {
            if ($ext == 'png' or $ext == 'jpg') {
                return $valid;
            }
        } elseif ($type == 'video') {
            if ($ext == 'mp4') {
                return $valid;

            }

        }
        return $valid = false;
    }


    public function Youtube($model,$Youtube,$media,$type,$name_file){

        $valid =false;

            if(!empty($model->$Youtube)){
                list($res,$path) = $this->DownloaderYoutube($model->$Youtube,$name_file);
                if($res)
                {
                    $model->$type = 'video';
                    $model->$media = $path;
                    $valid =true;
                   /* list($res,$path) =$this->uploaderFile($path,$name_file);
                    if($res) {
                        $model->$type = 'video';
                        $model->$media = $path;
                        $valid =true;
                    }else{
                        $valid = false;
                        $model->addError($Youtube,"<b>oops!!! :( </b><br>Something went wrong .<br>Please try again.");
                    }*/
                }
                else
                    if($path == 'fail'){
                        $valid = false;
                        $model->addError($Youtube,"<b>oops!!! :( </b><br>Something went wrong .<br>Please try again.");
                    }elseif($path == 'url'){
                        $valid = false;
                        $model->addError($Youtube,"<b>oops!!! :( </b><br>This Seems to be an Invalid URL , Please make sure your URL is correct.<br>ex. : https://www.youtube.com/watch?v=<b>XXXXXXX</b>");
                    }
            }else{
                $valid = false;
                $model->addError($Youtube,"youtube link required.");
            }

        return $valid;

    }
    public function news($url){

        $link = Yii::app()->params['feedUrl'];

        if(!isset($url) && empty($url))
            exit();

        $check_link = strpos(' '.$url,$link);


        if($check_link){
            $category = explode($link,$url);
            if(is_array($category) && isset($category[1]) && !empty($category[1])){
                $split = explode('/',$category[1]);
                if(is_array($split) && isset($split[1]) && !empty($split[1]))
                    $category = Category::model()->findByAttributes(array('url' => trim(Yii::app()->params['feedUrl'].'/'.$split[1])));
                else
                    $category = null;
                if(is_array($split) && isset($split[2]) && !empty($split[2]))
                    $sub_category = SubCategories::model()->findByAttributes(array('url' => trim(Yii::app()->params['feedUrl'].'/'.$split[2])));
                else
                    $sub_category  = null;
            }else{
                $sub_category  = null;
                $category= null;
            }


            $info = array();

            $info['sub_category_id'] = null;
            $info['creator'] = null;
            if(isset($category->id)){
                $info['category_id'] = $category->id;
            }
            if(isset($sub_category->id)){
                $info['sub_category_id'] = $sub_category->id;
            }





            $info = array_merge($info,$this->get_info_feature($url,$this->category->id));

            $shorten_url =$this->bitShort(urldecode($info['link']));
            $info['shorten_url'] = $shorten_url;
            $info['link_md5'] = md5(urldecode($info['link']));

            return $this->AddNews($info);


        }

        return false;

    }

    public function get_info_feature($link,$id)
    {
        Yii::import('application.modules.features.models.*');

        $html = new SimpleHTMLDOM();
        $html_url = $html->file_get_html($link);
        if(!isset($id)) {
            $id=isset(Category::model()->find('id=1')->id)?Category::model()->find('id=1')->id:Category::model()->find()->id;
        }
            $page_details = PageDetails::model()->findAllByAttributes(array('source_id' => $id));
        if(empty($page_details)){
            $id=isset(Category::model()->find('id=1')->id)?Category::model()->find('id=1')->id:Category::model()->find()->id;
            $page_details = PageDetails::model()->findAllByAttributes(array('source_id' => $id));
        }
        $data['image']['src'] =Yii::app()->params['domain'].'image/general.jpg';
        $data['image']['type']='image';
        $data['title']  = null;
        $data['link']  = $link;
        $data['link_md5']  = md5($link);
        $data['description']  = null;
        $data['creator'] = null;
        $data['column'] = null;
        foreach($page_details as $detail) {

            //Author
            if (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'creator') {
                $data['creator'] = $this->clear_tags($html_url->find($detail->predication, 0)->content);

            }
            elseif (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'creator') {
                $data['creator'] = $this->clear_tags($html_url->find($detail->predication, 0)->plaintext);
            }
            //End author

            //title
            if (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'title') {
                foreach ($this->array_erem_words as $array_matches){
                    if (preg_match('/\b' . $array_matches . '\b/u',$data['title'], $matches)) {
                        $data['title'] = str_replace($array_matches,'',$data['title']);
                    }
                }
                $data['title'] = $this->clear_tags($html_url->find($detail->predication, 0)->plaintext);
            }
            elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'title') {
                foreach ($this->array_erem_words as $array_matches){
                    if (preg_match('/\b' . $array_matches . '\b/u',$data['title'], $matches)) {
                        $data['title'] = str_replace($array_matches,'',$data['title']);
                    }
                }
                $data['title'] = $this->clear_tags($html_url->find($detail->predication, 0)->content);

            }
            //End title


            //Body desc
      /*      if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'body_description') {
                $body_desc = $html_url->find($detail->predication);
                if (isset($body_desc)) {
                    foreach ($body_desc as $bodies) {
                        $data['body_description'] .= $bodies->plaintext;
                    }
                }
            }elseif (!empty($html_url->find($detail->predication,0)->plaintext) and isset($html_url->find($detail->predication,0)->plaintext) and $detail->pageType->title == 'body_description') {
                $data['body_description'] = $html_url->find($detail->predication,0)->plaintext;
            }*/


            //End body desc

            //Description
            if (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'description') {
                $data['description'] = $this->clear_tags($html_url->find($detail->predication, 0)->plaintext);

            }
            elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'description') {

                $data['description'] = $this->clear_tags($html_url->find($detail->predication, 0)->content);
            }

            //End Description

            //Publishing date
     /*       if (isset($html_url->find($detail->predication, 0)->datetime) and !empty($html_url->find($detail->predication, 0)->datetime) and $detail->pageType->title == 'publishing_date') {
                $data['publishing_date'] = $html_url->find($detail->predication, 0)->datetime;
            }
            if (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'publishing_date') {
                $data['publishing_date']  = $html_url->find($detail->predication, 0)->plaintext;
            } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'publishing_date') {
                $data['publishing_date']  = $html_url->find($detail->predication, 0)->content;
            }*/
            //End publishing date

            //All media
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'all_media') {
                $script = $html_url->find($detail->predication);
                if (!empty($script)) {
                    foreach ($script as $sc) {
                        $media = (array)json_decode($sc->innertext);
                    }
                    if (!empty($media)) {
                        if (isset($media['videos'])) {
                            $data['video']['src'] = $media['videos'][0];
                        }
                        if (isset($media['images'])) {
                            foreach ($media['images'] as $gall) {
                                $data['image']['src'] = Yii::app()->params['feedUrl'] . $media['images'][0];
                                if ($gall != $media['images'][0])
                                    $gallery[] = Yii::app()->params['feedUrl'] . $gall;
                            }
                        }
                    }
                }
            }
            //End all media

            //image
            if (isset($html_url->find($detail->predication, 0)->src) and !empty($html_url->find($detail->predication, 0)->src) and $detail->pageType->title == 'image') {
                if (strpos($html_url->find($detail->predication, 0)->src, 'http') === 0) {
                    $data['image']['src'] = $html_url->find($detail->predication, 0)->src;

                }else
                    $data['image']['src'] = Yii::app()->params['feedUrl'].$html_url->find($detail->predication, 0)->src;
            } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'image') {
                if (strpos($html_url->find($detail->predication, 0)->content, 'http') === 0)
                    $data['image']['src'] = $html_url->find($detail->predication, 0)->content;
                else
                    $data['image']['src'] = Yii::app()->params['feedUrl'].$html_url->find($detail->predication, 0)->content;

            }
            //end image

            //video
          /*  if (isset($html_url->find($detail->predication, 0)->src) and !empty($html_url->find($detail->predication, 0)->src) and $detail->pageType->title == 'video') {
                $data['video']['src'] = $html_url->find($detail->predication, 0)->src;
            } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'video') {
                $data['video']['src'] = $html_url->find($detail->predication, 0)->content;

            }else if (isset($html_url->find($detail->predication, 0)->data) and !empty($html_url->find($detail->predication, 0)->data) and $detail->pageType->title == 'video') {
                $data['video']['src'] = $html_url->find($detail->predication, 0)->data;
            }*/
            //end video

            //gallery

            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'gallery') {
                $gall = $html_url->find($detail->predication);
                $counter=1;
                foreach ($gall as $item) {
                    $data['gallary']['type']='gallery';
                    $data['gallary']['src'][0] = $data['image']['src'];
                    if (strpos($html_url->find($detail->predication, 0)->src, 'http') === 0) {

                        $data['gallary']['src'][$counter] =$item->src;
                    }else {
                        $data['gallary']['src'][$counter] = Yii::app()->params['feedUrl'].$item->src;
                    }
                    $counter++;
                }

            }
            //End gallery
        }

        return $data;
    }


    public function validateTypeAndPlatform($model,$platform,$type){

        $Type = $model->$type;

        $valid =true;

        if($platform  == 'Instagram' AND !in_array($Type,Yii::app()->params['rule_array']['instagram'])){
            $valid = false;
            $model->addError($type,"the instagram not support video use image .");
        }

        if($platform == 'Twitter' AND !in_array($Type,Yii::app()->params['rule_array']['twitter'])){
            $valid = false;
            $model->addError($type,"the Twitter not support $Type use <small>image,video,text,preview</small>.");
        }

        return $valid;

    }

    public function validateBetweenDate($model,$start,$end)
    {
        if (!((strtotime($model->$start)) > (strtotime($model->$end))))
        {
            return true;
        }
        $model->addError($start,"$start must be smaller than $end .");
        $model->addError($end,"$end must be greater than $start .");
        return false;
    }

    public function SizeVideo($model,$media,$size,$time,$type = false){

        Yii::import('application.extensions.getid3.getID3');
        $getID3 = new getID3;
        $valid =true;
        if($type){
            $uploadedFile=CUploadedFile::getInstance($model,$media);
            $file = $getID3->analyze($uploadedFile->tempName);

        }else{
            $video = Yii::app()->params['webroot'].'/video/size_video.mp4';
            file_put_contents($video,file_get_contents($model->$media));
            $file = $getID3->analyze($video);
        }
        if(isset($file['filesize'])){

            if($file['filesize']<$size){

                if(isset($file['playtime_string'])){

                    $times = explode(':',$file['playtime_string']);

                    if(isset($times[0]) && isset($times[1])){
                        $times[0] = $times[0] * 60;
                        $time_array = $times[0] + $times[1];
                        if($time_array<=$time){

                            return $valid;
                        }else{
                            $valid = false;
                            $model->addError($media,"Video size length is not compatible with Twitter guidelines <small>140 seconds</small>...");
                        }
                    }else{
                        $valid = false;
                        $model->addError($media,"Video size length is not compatible with Twitter guidelines <small>140 seconds</small>...");
                    }
                }else{
                    $valid = false;
                    $model->addError($media,"<b>oops!!! :( </b><br>Something went wrong .<br>Please try again.");
                }
            }else{
                $valid = false;
                $model->addError($media,"Video size length is not compatible with Twitter guidelines <small>100MB</small>...");
            }
        }else{
            $valid = false;
            $model->addError($media,"<b>oops!!! :( </b><br>Something went wrong .<br>Please try again.");
        }

        return $valid;

    }

    public function validateImageUploader($model,$media,$name)
    {
        $valid = true;

            if($this->FileEmpty($model,$media)){
                if(!$this->GetFileType($model,$media,'image','upload')){
                    $model->addError($media,"It seems you Didn't upload a proper image file ..");
                    $valid=false;
                }else{
                    $model->$media = $this->uploader($model,$media,$name);
                    if($model->$media == false){
                        $model->addError($media,"<b>oops!!! :( </b><br>Something went wrong .<br>Please try again.");
                        $valid=false;
                    }
                }
            }else{
                $model->addError($media,"Media cannot be blank. *required.");
                $valid=false;
            }
        return $valid;
    }

    public function validateProfileImageUploader($model,$media,$name)
    {
        $valid = true;

        if($this->FileEmpty($model,$media)){
            if(!$this->GetFileType($model,$media,'image','upload')){
                $model->addError($media,"It seems you Didn't upload a proper image file ..");
                $valid=false;
            }else{
                $model->$media = $this->uploader($model,$media,$name);
                if($model->$media == false){
                    $model->addError($media,"<b>oops!!! :( </b><br>Something went wrong .<br>Please try again.");
                    $valid=false;
                }
            }
        }
        return $valid;
    }

    public function validateImageUpdate($model,$media)
    {
        $valid = true;
            if($this->FileEmpty($model,$media)){
                if(!$this->GetFileType($model,$media,'image')){
                    $model->addError($media,"It seems you Didn't upload a proper image file ..");
                    $valid=false;
                }
            }else{
                $model->addError($media,"Media cannot be blank. *required.");
                $valid=false;
            }
        return $valid;
    }

    public function validateVideo($model,$media,$platform)
    {

        $valid = true;
        if($this->FileEmpty($model,$media)){

            if(!$this->GetFileType($model,$media,'video','upload')){
                $model->addError($media,"It seems you Didn't upload a proper video file ..");
                $valid=false;
            }else{
                if($platform == 'Twitter'){
                    if($this->SizeVideo($model,$media,536870912,140,true)){
                        $valid = true;
                    }else{
                        $valid =false;
                    }
                }else{
                    $valid = true;
                }
            }
        }else{
            $model->addError($media,"Media cannot be blank. *required.");
            $valid=false;
        }
        return $valid;
    }

    public function validateVideoUploader($model,$media,$platform,$name)
    {

        $valid = true;
        if($this->FileEmpty($model,$media)){

            if(!$this->GetFileType($model,$media,'video','upload')){
                $model->addError($media,"It seems you Didn't upload a proper video file ..");
                $valid=false;
            }else{
                if($platform == 'Twitter'){
                    if($this->SizeVideo($model,$media,536870912,140,true)){
                        $model->$media = $this->uploader($model,$media,$name);
                        if($model->$media == false){
                            $model->addError($media,"<b>oops!!! :( </b><br>Something went wrong .<br>Please try again.");
                            $valid=false;
                        }
                    }else{
                        $valid =false;
                    }
                }else{
                    $model->$media = $this->uploader($model,$media,$name);
                    if($model->$media == false){
                        $model->addError($media,"<b>oops!!! :( </b><br>Something went wrong .<br>Please try again.");
                        $valid=false;
                    }
                }
            }
        }else{
            $model->addError($media,"Media cannot be blank. *required.");
            $valid=false;
        }
        return $valid;
    }

    public function validateVideoUpdate($model,$media,$platform,$name)
    {
        $valid = true;
        if($this->FileEmpty($model,$media)){
            if(!$this->GetFileType($model,$media,'video','upload')){
                $model->addError($media,"It seems you Didn't upload a proper video file ..");
                $valid=false;
            }else{
                if($platform == 'Twitter'){
                    if($this->SizeVideo($model,$media,15000000,30,true)){
                        $valid =true;
                    }else{
                        $valid =false;
                    }
                }else{
                    $valid =true;
                }
            }
        }else{
            $model->addError($media,"Media cannot be blank. *required.");
            $valid=false;
        }
        return $valid;
    }

    public function validateDownloadVideoUploader($model,$media,$platform,$name,$type,$type_value)
    {
        $valid = true;

        if($this->FileIsExist($model->$media)){
            if(!$this->GetFileType($model,$model->$media,'video')){
                $model->addError($media,"It seems you Didn't upload a proper video file ..");
                $valid=false;
            }else{
                if($platform == 'Twitter'){

                    if($this->SizeVideo($model,$media,536870912,140)){
                        list($valid , $model->$media) = $this->uploaderFile($model->$media,$name);
                        if($model->$media == false){
                            $model->addError($media,"<b>oops!!! :( </b><br>Something went wrong .<br>Please try again.");
                            $valid=false;
                        }
                    }else{
                        $model->$type = $type_value;
                        $model->addError($type_value,$model->getError($media));
/*                  $model->*/
                        $valid =false;
                    }
                }else{
                    list($valid , $model->$media) = $this->uploaderFile($model->$media,$name);
                    if($model->$media == false){
                        $model->addError($media,"<b>oops!!! :( </b><br>Something went wrong .<br>Please try again.");
                        $valid=false;
                    }
                }
            }
        }else{
            $model->addError($media,"Media cannot be blank. *required.");
            $valid=false;
        }
        return $valid;
    }

    public function getTweetLength($tweet, $gotImage ,$video) {
        $tcoLengthHttp = 23;
        $tcoLengthHttps = 23;
        $twitterPicLength = 24;
        $twitterVideoLength = 24;
        $urlData = Twitter_Extractor::create($tweet)->extract();
        $tweetLength = mb_strlen($tweet,'utf-8');
        foreach ($urlData['urls_with_indices'] as $url) {
            $tweetLength -= mb_strlen($url['url']);
            $tweetLength += mb_stristr($url['url'], 'https') === 0 ? $tcoLengthHttps : $tcoLengthHttp;
        }
        if ($gotImage) $tweetLength += $twitterPicLength;
        if ($video) $tweetLength += $twitterVideoLength;
        return $tweetLength;
    }

    public function shorten($input, $length, $ellipses = true, $strip_html = true) {
        if ($strip_html) {
            $input = strip_tags($input);
        }
        if (mb_strlen($input,'UTF-8') <= $length) {
            return $input;
        }
        $last_space = strrpos(substr($input, 0, $length), ' ');
        $trimmed_text = mb_substr($input, 0, $last_space);
        if ($ellipses) {
            $trimmed_text .= '...';
        }
        return $trimmed_text;
    }

    public function clear_tags($string){

        $string= preg_replace('/\p{C}+/u', "", $string);

        $string = html_entity_decode($string);

        $string = str_replace('# #', '#', $string);

        $string = str_replace('##', '#', $string);

        $string = str_replace('…',' ... ',$string);

        $string = str_replace('”','"',$string);

        $string = str_replace('“','"',$string);
        $string = strip_tags($string);
        return $string;

        $string = preg_replace('/[^\p{Arabic}\da-z- \|\/\،\/\:\/\%\/\@\/\!\/\=\/\+\/\*\/\"\/\-\/\_\/\,\/\.\/\"\/]/ui', ' ', $string);

        $string = str_replace('  ','',$string);

        return $string;
    }

    public function get_date($date){

        return date('Y-m-d H:i:s',strtotime($date));

    }

    public function clear_author($string){

        $string = $this->clear_tags(strip_tags($string));

        return $string;
    }



    public function shorten_point($input, $point = '.') {
        $input_ex = explode($point,$input);
        $data = array_chunk($input_ex,2);
        if(is_array($data)){

            return (implode('. '.PHP_EOL,$data[0]));
        }
        return $input;
    }



    public function AddNews($data){

        $news = new News();
        $news->id = null;
        $news->title = $data['title'];
        $news->link = $data['link'];
        $news->description = $data['description'];
        $news->category_id = $data['category_id'];
        $news->sub_category_id = $data['sub_category_id'];
        $news->column = $data['column'];
        $news->creator = $data['creator'];
        $news->link_md5 = $data['link_md5'];
        $news->shorten_url = $data['shorten_url'];
        $news->publishing_date = empty($data['time'])?date('Y-m-d H:i:s'):$data['time'];
        $news->schedule_date =  empty($data['time'])?date('Y-m-d H:i:s'):$data['time'];
        $news->generated = 0;
        $news->setIsNewRecord(true);
        $news->created_at = date('Y-m-d H:i:s');

        if($news->save(false)){
            $media = new MediaNews();
            $media->id = null;
            $media->media = isset($data['image']['src'])?$data['image']['src']:null;
            $media->news_id = $news->id ;
            $media->setIsNewRecord(true);
            $media->save(false);

        }

        return array($news->id,isset($data['image']['src'])?$data['image']['src']:null);

    }


    public function check_emarate($link){

        $emarate_link   = 'http://www.eremnews.com/';
        $pos = strpos(' '.$link, $emarate_link );
        if(!$pos) {
            return false;
        }
        return true;
    }

}