<?php
class CategoryController extends BaseController
{



	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(

			array('allow',
				'actions'=>array('admin','delete','create','update','index','view','status','edit'),
				'users'=>array(Yii::app()->user->getState('type')),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{


		$subSections = new SubCategories();
		$subSections->category_id=$id;

		$news_table = new News();
		$news_table->category_id=$id;


		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'sub_Sections'=>$subSections,
			'news_table'=>$news_table,
		));

	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Category;

		// Uncomment the following line if AJAX validation is needed

		$this->performAjaxValidation($model);

		if(isset($_POST['Category']))
		{
			$model->attributes=$_POST['Category'];
			$model->created_at = date('Y-m-d H:i:s');
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionEdit(){
		if(isset($_POST)){
			if(isset($_POST['name']) && isset($_POST['value']) && isset($_POST['pk']) && isset($_POST['scenario'])){
				$model = $this->loadModel($_POST['pk']);

				if(!empty($_POST['name'])){
					$name = $_POST['name'];
					$value = $_POST['value'];
					$model->$name = $value;
					if($model->save())
						echo true;
					else
						echo false;
				}
			}

		}
	}

	public function actionStatus($id)
	{


		$model=$this->loadModel($id);

		if($model->active == 0){
			$model->active =1;
		}else{
			$model->active =0;
		}
		// Uncomment the following line if AJAX validation is needed




  			if($model->save()) {
				$this->redirect(array('admin','id'=>$model->id));

			}


	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        $models=new Category('search');
        $models->unsetAttributes();  // clear any default values
        if(isset($_GET['Category']))
            $models->attributes=$_GET['Category'];
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Category'])) {
            $model->attributes = $_POST['Category'];

            if ($model->save()) {
                $this->redirect(array('index'));
            }
        }

        $this->render('index',array(
            'model'=>$model,
            'models'=>$models
        ));
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$data  = $this->loadModel($id);

		$data->deleted = 1;

		$data->save();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
    public function actionIndex()
    {
        $model=new Category('category_source');

        $models=new Category('search');
        $models->unsetAttributes();  // clear any default values
        if(isset($_GET['Category']))
            $models->attributes=$_GET['Category'];


        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if(isset($_POST['Category']))
        {
            $model->attributes=$_POST['Category'];
            $model->created_at = date('Y-m-d H:i:s');
            if($model->save()){
                $this->redirect(array('index'),array(
                    'model'=>$model
                ));

            }
        }

        $this->render('index',array(
            'model'=>$model,
            'models'=>$models
        ));
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Category('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Category'])) {
			$this->data_search = $_GET['Category'];
			$this->data_search($model);
			$model->attributes = $_GET['Category'];
		}
		$this->render('admin',array(
			'model'=>$model,
		));
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Category the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Category::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Category $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='category-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
