<?php

class ProfileFormController extends BaseController
{


	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('update','updatePassword'),
				'users'=>array(Yii::app()->user->getState('type')),
                array('deny',  // deny all users
                    'users'=>array('*'),
                ),
			),

		);
	}
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}



	public function actionUpdatePassword($id){

		$model = new ProfileForm();
        $user = $this->loadModel($id);
        $model->setScenario('updatePassword');
		$this->performAjaxValidation($model);
        $valid=true;
		if(isset($_POST['ProfileForm'])) {
            if (md5($_POST['ProfileForm']['password']) != $user->password) {
                $model->addError('password', "<b>incorrect password</b>");
                $valid = false;
            }
            if($_POST['ProfileForm']['new_password'] != $_POST['ProfileForm']['confirm_password']){
                $model->addError('new_password', "<b>Passwords donot match</b>");
                $valid = false;
            }

            if ($valid) {
                $user->password = md5($_POST['ProfileForm']['new_password']);
                if ($user->save()) {
                    $_POST['updated'] = true;
                   /* $this->render('updatePassword', array(
                        'model' => $model,
                    ));*/
                }
            }
        }
		$this->render('updatePassword',array(
 			'model'=>$model,
		));

	}

	public function actionUpdate($id)
	{
        $model=$this->loadModel($id);

        if(!isset(Yii::app()->session['skin'])) {
            Yii::app()->session['skin'] = $model->background_color;

        }



        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        $valid=true;
        if(isset($_POST['User'])) {


            $Media = $model->profile_pic;


            if (!$this->FileEmpty($model, 'profile_pic')) {
                if (!$this->GetFileType($model, $Media, 'image')) {
                    /*                    $model->addError('profile_pic', 'It seems you Didn\'t upload a proper image file ..');*/
                    $valid = true;
                } else {
                    $model->profile_pic = $Media;
                }
            } else {
                if (!$this->GetFileType($model, 'profile_pic', 'image', 'upload')) {
                    $model->addError('profile_pic', "It seems you Didn't upload a proper image file ..");
                    $valid = false;
                } else
                    $model->profile_pic = $this->uploader($model, 'profile_pic', 'profile_pic_edit_id' . $model->id . '_' . time());
            }

            if(isset($_POST['User']['background_color']) && !empty($_POST['User']['background_color'])){
                Yii::app()->session['skin'] =$_POST['User']['background_color'];
            }

            $model->attributes = $_POST['User'];
            $model->background_color = Yii::app()->session['skin'];
            $model->gender = $_POST['User']['gender'];

            if ($valid) {
                if ($model->save()) {
                    $_POST['updated'] = true;

                }
            }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
	}
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='profile-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}