<?php

class PostQueueController extends BaseController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */


    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }


    public function actionUpload()
    {
        //Yii::import("ext.EAjaxUpload.qqFileUploader");

        $folder='uploads/image2/';// folder for uploaded files
        $allowedExtensions = array("jpg","png");//array("jpg","jpeg","gif","exe","mov" and etc...
                $sizeLimit = 10 * 1024 * 1024*1024*1024;// maximum file size in bytes
                $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
                $result = $uploader->handleUpload($folder);
                $result=htmlspecialchars(json_encode($result), ENT_NOQUOTES);
                echo $result;// it's array
	}







    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('MainUn', 'create', 'update', 'index', 'view', 'main', 'getAll','total_posts' ,'activatePost','getPosts','changeImage','push_post','update_image_quick','edit','update_text_quick','admin', 'delete','edit_text','mainPosted','re_post','update_posted','showDetailsUrl','getHashtags','edit_pined','mainPinned','getTwitterSize'),
                'users' => array(Yii::app()->user->getState('type')),
            ),
            array(
                'allow',
                'actions'=>array('restore_posted'),
                'users'=>array('super_admin')
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }
    public function actionUpdate_text_quick($id){
        $model = $this->loadModel($id);
        $model->attributes = $_POST['PostQueue'];
        if($model->save()){
            $this->redirect('/eremnews/postQueue/view/'.$model->id);
        }
        $this->redirect('/eremnews/postQueue/view/'.$model->id);

    }
    public function actionEdit(){
        if(isset($_POST)){
            if(isset($_POST['name']) && isset($_POST['value']) && isset($_POST['pk']) && isset($_POST['scenario'])){
                $model = $this->loadModel($_POST['pk']);

                if(!empty($_POST['name'])){
                    $name = $_POST['name'];
                    $value = $_POST['value'];
                    $model->$name = $value;
                    if($model->save())
                        echo true;
                    else
                        echo false;
                }
            }

        }
    }
    public function actionChangeImage(){



        $id= $_POST['id'];
        $media = $_POST['media'];
        $platform = $_POST['platforms'];
        if($platform == 0) {
            $model = PostQueue::model()->findByPk($id);
            $model = $model->GetPostImageChanger($model->id,$model->platform_id,$model->parent_id);

            foreach($model as $mod){

                $mod->media_url = $media;
                $mod->save();
                echo $mod->id;

            }
        }
        else {
            $model = PostQueue::model()->findByPk($id);
            $model->media_url = $media;
            $model->save();
            echo $model->id;
        }




    }
    public function actionEdit_pined(){
        if(isset($_POST['id'])) {
            $image = null;
            $model = $this->loadModel($_POST['id']);
            if($model->pinned == 0) {
                $model->pinned = 1;
            }
            else {
                $model->pinned = 0;
            }
            if($model->save()){
                echo $model->pinned;
            }

        }
    }
    public function actionUpdate_image_quick($id){


        $model = $this->loadModel($id);

        $this->performAjaxValidation($id);
        $model->scenario = 'update';
        $model->validate();
        $valid=true;
        $model->attributes = $_POST['PostQueue'];

        if ($this->FileEmpty($model, 'media_url')) {

            if (!$this->GetFileType($model, 'media_url', 'image', 'upload')) {
                $model->addError('media_url', "It seems you Didn't upload a proper image file ..");
                $this->redirect(array('view', 'id' => $model->id));
                $valid = false;
            } else
                $model->media_url = $this->uploader($model, 'media_url', 'post_queue_edit_id' . $model->id . '_' . time());
        }else{
            $model->addError('media_url', "It seems you Didn't upload a proper image file ..");
            $valid=false;

            $this->redirect(array('view', 'id' => $model->id));

        }

        $imagesNews = new MediaNews();

        if ($valid) {

            $imagesNews->media = $model->media_url;
            $imagesNews->news_id = $model->news_id;
            $imagesNews->id=null;
            $imagesNews->type = 'gallery';
            $imagesNews->created_at=date('Y-m-d H:i:s');
            $imagesNews->save(true);
            if(isset($_POST['upload_only'])){
                $this->redirect(array('view', 'id' => $model->id));
            }


            $news_id = $model->news_id;
            if(isset($_POST['facebook']))
                $platform = 1;
            elseif(isset($_POST['twitter']))
                $platform=2;
            elseif(isset($_POST['instagram']))
                $platform=3;
            elseif(isset($_POST['all_platforms']))
                $platform=0;

            if(isset($platform)) {
                if($platform == 0) {

                    $models_all = $model->GetPostImageChanger($model->id,$model->platform_id,$model->parent_id,false);

                    foreach($models_all as $mod){

                        $mod->media_url = $model->media_url;
                        if($mod->save());
                    }
                    $this->redirect(array('view', 'id' => $model->id));

                }
                else {

                    $models = $model->GetPostImageChanger($model->id,$platform,$model->parent_id,true);
                    if(empty($models)){
                        $valid=false;
                    }
                    if($valid) {
                        $models->media_url = $model->media_url;

                        if ($models->save()) {
                            $this->redirect(array('view', 'id' => $model->id));

                        }
                    }else{
                        $this->redirect(array('view', 'id' => $model->id));

                    }
                }
            }

        }

    }

    public function actionAlbumFb()
    {

        print_r($_POST);

    }
    public function actionGetTwitterSize(){
        if(isset($_GET)) {
            echo $this->getTweetLength($_GET['text'], $_GET['type'] == 'image' ? true : false, $_GET['type'] == 'video' ? true : false);
        }
    }

    public function actionMain()
    {

        $md = $this->Search(1, null);
        $this->render('main', $md);

    }
    public function actionRestore_posted($id){
        $model = PostQueue::model()->findByPk($id);
        $model->is_posted=0;
        $model->save();
    }
    public function actionMainPosted()
    {
       /* Yii::import('application.modules.split_report.components.SplitReportBaseModels');

        Yii::import('application.modules.split_report.models.*');*/

        $md = $this->Search(1, 'Posted_');
        $this->render('main', $md);

    }

    public function actionMainPinned()
    {

        $md = $this->Search(1, 'Pinned_');
        $this->render('main', $md);

    }

    public function actionMainUn()
    {
        $md = $this->Search(0, 'Un_');
        $this->render('main', $md);
    }

    public function Search($is_scheduled, $index)
    {


        $model = new PostQueue();
        $Sql_Platform = CHtml::listData(Platform::model()->findAll(), 'id', 'title');

        if(Yii::app()->controller->action->id == 'mainPosted')
            $model->pushed_posts = true;
        if(Yii::app()->controller->action->id == 'mainPinned')
            $model->pinned_posts = true;
        if(Yii::app()->controller->action->id == 'main')
            $model->not_posted = true;
        if(Yii::app()->controller->action->id == 'mainUn')
            $model->not_posted = true;
        if (isset($_GET['PostQueue'])) {
            $data = $_GET['PostQueue'];
            if (isset($data['platform']) and $data['platform'] != '') {
                $platform = explode(',', $data['platform']);
                if (is_array($platform)) {
                    $new_platform = array();
                    foreach ($platform as $item) {
                        $key = array_search($item, $Sql_Platform);
                        if ($key) {
                            $new_platform[$key] = $item;
                        }
                    }
                    Yii::app()->session->add($index . 'platform', $new_platform);
                }
            }

            $model->unsetAttributes();
            $model->catgory_id = $data['catgory_id'];
            $model->type = $data['type'];

            $model->is_scheduled = $is_scheduled;
            if ($data['schedule_date'] == '')
                $model->schedule_date = date('Y-m-d');

            if ($data['to'] == '')
                $model->to = date('Y-m-d');


            $model->schedule_date = $data['schedule_date'];
            $model->to = $data['to'];
            $model->sorts = $data['sorts'];
            $model->view_posts = $data['view_posts'];
            Yii::app()->session[$index . 'schedule_date'] = $model->schedule_date;
            Yii::app()->session[$index . 'to'] = $model->to;
            Yii::app()->session[$index . 'sorts'] = $data['sorts'];
            Yii::app()->session[$index . 'view_posts'] = $data['view_posts'];

            Yii::app()->session[$index . 'type'] = $data['type'];
            Yii::app()->session[$index . 'catgory_id'] = $model->catgory_id;
            Yii::app()->session[$index . 'is_scheduled'] = $model->is_scheduled;

        } else {
            $model->schedule_date = date('Y-m-d');
            $model->to = date('Y-m-d', strtotime(date('Y-m-d') . ' +1 day'));
            if(Yii::app()->controller->action->id == 'main' or Yii::app()->controller->action->id == 'mainPosted' or Yii::app()->controller->action->id == 'mainPinned')
                $model->is_scheduled = 1;
            else
                $model->is_scheduled = 0;
        }

        if (isset(Yii::app()->session[$index . 'schedule_date'])) {
            $model->schedule_date = Yii::app()->session[$index . 'schedule_date'];
        }


        if (isset(Yii::app()->session[$index . 'is_scheduled'])) {
            $model->is_scheduled = Yii::app()->session[$index . 'is_scheduled'];
        }
        if (isset(Yii::app()->session[$index . 'sorts'])) {
            $model->sorts = Yii::app()->session[$index . 'sorts'];
        }if (isset(Yii::app()->session[$index . 'view_posts'])) {
        $model->view_posts = Yii::app()->session[$index . 'view_posts'];
    }
        if (isset(Yii::app()->session[$index . 'to'])) {
            $model->to = Yii::app()->session[$index . 'to'];
        }

        if (isset(Yii::app()->session[$index . 'type'])) {
            $model->type = Yii::app()->session[$index . 'type'];
        }

        if (isset(Yii::app()->session[$index . 'catgory_id'])) {
            $model->catgory_id = Yii::app()->session[$index . 'catgory_id'];
        }


        $model->platform = 'Facebook,Twitter,Instagram';
        $md = array(
            'model' => $model,

        );
        if (isset(Yii::app()->session[$index . 'platform'])) {
            $key = array_search('Facebook', Yii::app()->session[$index . 'platform']);
            if ($key) {
                $model->platform_id = $key;
                    $md['Facebook'] = $model->custom_search(true,Yii::app()->session[$index . 'sorts'],Yii::app()->session[$index . 'view_posts']);

            }

            $key = array_search('Twitter', Yii::app()->session[$index . 'platform']);
            if ($key) {
                $model->platform_id = $key;
                    $md['Twitter'] = $model->custom_search(true,Yii::app()->session[$index . 'sorts'],Yii::app()->session[$index . 'view_posts']);

            }
            $key = array_search('Instagram', Yii::app()->session[$index . 'platform']);
            if ($key) {
                $model->platform_id = $key;
                    $md['Instagram'] = $model->custom_search(true,Yii::app()->session[$index . 'sorts'],Yii::app()->session[$index . 'view_posts']);

            }


            $counter = count(Yii::app()->session[$index . 'platform']);
            $plus = 1;
            $model->platform = '';
            foreach (Yii::app()->session[$index . 'platform'] as $row) {
                if ($plus < $counter) {
                    $model->platform .= $row . ',';
                } else {

                    $model->platform .= $row;
                }
                $plus++;

            }

        } else {

            $model->platform_id = array_search('Facebook', $Sql_Platform);
               $md['Facebook'] = $model->custom_search(true,Yii::app()->session[$index . 'sorts'],Yii::app()->session[$index . 'view_posts']);

            $model->platform_id = array_search('Twitter', $Sql_Platform);

             $md['Twitter'] = $model->custom_search(true,Yii::app()->session[$index . 'sorts'],Yii::app()->session[$index . 'view_posts']);

            $model->platform_id = array_search('Instagram', $Sql_Platform);
                $md['Instagram'] = $model->custom_search(true,Yii::app()->session[$index . 'sorts'],Yii::app()->session[$index . 'view_posts']);

            $model->platform = 'Facebook,Twitter,Instagram';
        }


        return $md;
    }

    public function actionEdit_text($id){
        $model = $this->loadModel($id);
        if (isset($_POST['post'])) {

            $model->post = $_POST['post'];
            if ($model->save()) {
                echo $model->get_hashtags_urls($model->platforms->title, $model->post);
            } elseif(empty(trim($_POST['post']))){
                echo 'You cannot leave it blank';
            } elseif ($model->platform_id = 2) {
                echo 'Text is very long for twitter';
            }
        }


    }

    public function actionCreate()
    {
        $model = new PostQueue('create');
        $model->setScenario('create');
        $parent=true;
        $valid=true;
        $parent_id=null;
         $this->performAjaxValidation($model);
         if (isset($_POST['PostQueue'])) {
             $type_post =1;

             $model->attributes = $_POST['PostQueue'];
            $platforms = $_POST['PostQueue'];
            if(empty($platforms['platform_id'] )){
                $model->addError('platform_id', 'you must select at least one platform');

            }

            if(!empty($platforms['platform_id'])) {
                foreach(array_reverse($platforms['platform_id']) as $item){
                    $model->platform_id = $item;
                    if($model->platforms->title == 'Twitter'){
                        break;
                    }
                 }


                    if ($this->validateTypeAndPlatform($model, $model->platforms->title, 'type')) {
                        if ($model->type == 'image') {
                            $valid = $this->validateImageUploader($model, 'media_url', 'image_PostQueue_' . time());
                        }
                        if ($model->type == 'video') {
                            if($this->validateVideo($model,'media_url',$model->platforms->title)){

                                $valid = $this->validateVideoUploader($model, 'media_url', $model->platforms->title, 'video_PostQueue_' . time());
                            }else{
                                $valid = false;
                            }
                        }


                        if ($model->type == 'youtube') {
                            $valid = $this->Youtube($model, 'youtube', 'media_url', 'type', 'youtube_PostQueue');
                            if ($valid) $valid = $this->validateDownloadVideoUploader($model, 'media_url', $model->platforms->title, 'video_PostQueue_' . time(), 'type', 'youtube');
                        }
                        if ($model->type == 'preview') {


                            if ($this->FileEmpty($model, 'media_url')) {

                                if (!$this->GetFileType($model, 'media_url', 'image', 'upload')) {
                                    $model->addError('media_url', "It seems you Didn't upload a proper image file ..");
                                    $valid = false;
                                } else {
                                    $model->media_url = $this->uploader($model, 'media_url', 'post_queue_create_id' . $model->id . '_' . time());

                                }
                            }

                            if (!empty($model->link)) {

                                if($this->check_emarate($model->link)){

                                    $this->category = explode('www.eremnews.com',$model->link);
                                    if(is_array($this->category) && isset($this->category[1]) && !empty($this->category[1])){
                                        $this->category = explode('/',$this->category[1]);
                                        if(is_array($this->category) && isset($this->category[0]) && !empty($this->category[0])) {
                                            $this->category = Category::model()->findByAttributes(array('url' => trim(Yii::app()->params['feedUrl'] . '/' . $this->category[0])));
                                           $model->catgory_id = $this->category->id ;
                                        }
                                    }
                                    /*$this->details = $this->get_info($model->link);*/
                                    //$this->category = Category::model()->findByPk($model->catgory_id);
                                    list($id_news,$image_link) = $this->news($model->link);
                                    if(empty($model->media_url)){
                                        $model->media_url = $image_link;
                                    }
                                    $model->news_id = $id_news;
                                }

                                $shortUrl = new ShortUrl();
                                $model->link = $shortUrl->short(urldecode($model->link));
                            }

                        }
                    }else
                        $valid = false;

                $model->created_at = date('Y-m-d H:i:s');
                $model->generated = 'manual';
                $model->is_scheduled = 1;
                if($valid){
                    foreach ($platforms['platform_id'] as $item) {
                        $model->id = null;
                        $model->platform_id = $item;
                        $model->validate();

                        if ($parent == false) {
                            $model->parent_id = $parent_id;
                        }

                        if(isset($_POST['postnow'])){
                            $model->schedule_date= date("Y-m-d H:i:s", strtotime('-1 minutes'));
                        }
                        if ($valid) {
                            $model->setIsNewRecord(true);
                            if ($model->save()) {
                                if ($parent == true) {
                                    $parent_id = $model->id;
                                    $parent = false;
                                }
                            }
                        }
                    }
                }
            }



            if($model->id != null) {
                Yii::app()->user->setFlash('create', 'Thank you for add conetns ... . .... ');
                if(isset($_POST['postqueue']))
                    $this->redirect(array('postQueue/main'));
                elseif(isset($_POST['review']))
                    $this->redirect(array('view', 'id' => $model->id));
                elseif(isset($_POST['postnow']))
                    $this->redirect(array('view', 'id' => $model->id));
            }
             $model->platform_id = $platforms['platform_id'];

         }

        $this->render('create', array(
            'model' => $model,

        ));
    }


    /*
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if (isset($_POST['all'])) {

            if ($_POST['all']) {


                $model = PostQueue::model()->findByPk($id);

                if (!$model->parent_id) {

                    $update = Yii::app()->db->createCommand()->update('post_queue',
                        array(
                            'is_scheduled' => new CDbExpression('0'),
                        ),
                        'parent_id=:id',
                        array(':id' => $id)
                    );
                    if ($model === null)
                        throw new CHttpException(404, 'The requested page does not exist.');
                    if ($model->is_scheduled == 1) {
                        $model->is_scheduled = 0;
                        $model->save(false);
                        $url = Yii::app()->createUrl('/site') . '?id=' . $id;
                    } else {
                        $model->delete();
                        $url = array('/site');
                    }

                    echo json_encode(array('valid' => true, 'url' => $url, 'all' => true));

                } else {

                    $update = Yii::app()->db->createCommand()
                        ->update('post_queue',
                            array(
                                'is_scheduled' => new CDbExpression('0'),
                            ),
                            'parent_id=:id',
                            array(':id' => $model->parent_id)
                        );

                    $update = Yii::app()->db->createCommand()
                        ->update('post_queue',
                            array(
                                'is_scheduled' => new CDbExpression('0'),
                            ),
                            'id=:id',
                            array(':id' => $model->parent_id)
                        );

                    if ($model === null)
                        throw new CHttpException(404, 'The requested page does not exist.');
                    if ($model->is_scheduled == 1) {
                        $model->is_scheduled = 0;
                        $model->save(false);
                        $url = Yii::app()->createUrl('/site') . '?id=' . $id;
                    } else {
                        $model->delete();
                        $url = array('/site');
                    }

                    echo json_encode(array('valid' => true, 'url' => $url, 'all' => true));

                }

            } else {
                $model = PostQueue::model()->findByPk($id);
                if ($model === null)
                    throw new CHttpException(404, 'The requested page does not exist.');
                if ($model->is_scheduled == 1) {
                    $model->is_scheduled = 0;
                    $model->save(false);
                    $url = Yii::app()->createUrl('/site') . '?id=' . $id;
                } else {
                    $model->delete();
                    $url = array('/site');
                }


                echo json_encode(array('valid' => true, 'url' => $url, 'all' => false));

            }


        }
    }
    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new PostQueue('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['PostQueue']))
            $model->attributes = $_GET['PostQueue'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id)
    {
        if(isset($_POST['medias'])){
            Yii::app()->session['medi'] = $_POST['medias'];
        }


        $model = $this->loadModel($id);
        if($model->is_posted ==0 or $model->is_posted ==2) {
            $model->scenario = 'update';
            $valid = true;
            $this->performAjaxValidation($model);
            if (isset($_POST['PostQueue'])) {

                $error = false;
                if ($model->is_posted == 2 or $model->is_posted == 3) {
                    $error = true;
                }

                if ($error) {
                    $model->is_posted = 0;
                    $model->errors = null;
                }

                $Media = $model->media_url;

                $Type = $model->type;
                $Link = $model->link;
                $error = false;
                if ($model->is_posted == 2 or $model->is_posted == 3) {
                    $error = true;
                }
                $model->attributes = $_POST['PostQueue'];
                if ($error) {
                    $model->is_posted = 0;
                    $model->errors = null;
                }
                $model->validate();
                $model->attributes = $_POST['PostQueue'];

                $imagesNews = new MediaNews();
                $valid_images_news = false;

                if ($this->validateTypeAndPlatform($model, $model->platforms->title, 'type')) {
                    if ($model->type == 'image') {
                        if (!empty(Yii::app()->session['medi'])) {
                            if ($this->FileEmpty($model, 'media_url')) {
                                if (!$this->GetFileType($model, $Media, 'image')) {
                                    $model->media_url = Yii::app()->session['medi'];
                                    $valid_images_news = false;

                                } else {
                                    $model->media_url = $this->uploader($model, 'media_url', 'post_queue_edit_id' . $model->id . '_' . time());
                                    Yii::app()->session['medi'] = $model->media_url;
                                    $valid_images_news = true;


                                }
                            } else {
                                $model->media_url = Yii::app()->session['medi'];
                                $valid_images_news = false;

                            }
                        } else {
                            if (!$this->FileEmpty($model, 'media_url')) {
                                if (!$this->GetFileType($model, $Media, 'image')) {
                                    $model->addError('media_url', 'It seems you Didn\'t upload a proper image file ..');
                                    $valid = false;
                                } else {
                                    $model->media_url = $Media;
                                    $valid_images_news = false;

                                }
                            } else {
                                if (!$this->GetFileType($model, 'media_url', 'image', 'upload')) {
                                    $model->addError('media_url', "It seems you Didn't upload a proper image file ..");
                                    $valid = false;
                                } else
                                    $model->media_url = $this->uploader($model, 'media_url', 'post_queue_edit_id' . $model->id . '_' . time());
                                $valid_images_news = true;

                            }
                        }
                        if (isset($model->media_url)) {
                            $imagesNews->media = $model->media_url;


                        }
                    }
                    if ($model->type == 'video') {
                        if (!$this->FileEmpty($model, 'media_url')) {
                            if (!$this->GetFileType($model, $Media, 'video')) {
                                $model->addError('media_url', 'It seems you Didn\'t upload a proper video file ..');
                                $valid = false;
                            } else {
                                $model->media_url = $Media;
                            }
                        } else {
                            $valid = $this->validateVideoUploader($model, 'media_url', $model->platforms->title, 'post_queue_edit_id' . $model->id . '_' . time());
                        }

                    }
                    if ($model->type == 'youtube') {
                        $valid = $this->Youtube($model, 'youtube', 'media_url', 'type', 'youtube_PostQueue_');
                        if ($valid) $valid = $this->validateDownloadVideoUploader($model, 'media_url', $model->platforms->title, 'video_PostQueue_' . time(), 'type', 'youtube');
                    }

                    if ($model->type == 'preview') {
                        if (!empty(Yii::app()->session['medi'])) {
                            if ($this->FileEmpty($model, 'media_url')) {
                                if (!$this->GetFileType($model, $Media, 'image')) {
                                    $model->media_url = Yii::app()->session['medi'];
                                    $valid_images_news = false;

                                } else {
                                    $model->media_url = $this->uploader($model, 'media_url', 'post_queue_edit_id' . $model->id . '_' . time());
                                    $valid_images_news = true;


                                }
                            } elseif (isset(Yii::app()->session['medi'])) {
                                $model->media_url = Yii::app()->session['medi'];
                                $valid_images_news = false;
                            } else {
                                $model->media_url = null;
                                $valid_images_news = false;

                            }
                        } else {
                            if (!$this->FileEmpty($model, 'media_url')) {
                                if (!$this->GetFileType($model, $Media, 'image')) {
                                } else {
                                    $model->media_url = $Media;
                                    $valid_images_news = false;

                                }
                            } else {
                                if (!$this->GetFileType($model, 'media_url', 'image', 'upload')) {
                                    $model->addError('media_url', "It seems you Didn't upload a proper image file ..");
                                    $valid = false;
                                } else
                                    $model->media_url = $this->uploader($model, 'media_url', 'post_queue_edit_id' . $model->id . '_' . time());
                                $valid_images_news = true;

                            }
                        }
                        if (!empty($model->link)) {
                            if (strpos($model->link, 'goo.gl') == false) {
                                if (empty($Link)) {
                                    if ($model->type == 'preview') {
                                        if (!empty($model->link)) {
                                            $shortUrl = new ShortUrl();
                                            $model->link = $shortUrl->short(urldecode($model->link));
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else
                    $valid = false;
                /*
                            if (!empty($Media)) {
                                if ($model->type == 'image' and pathinfo($Media)['extension'] == 'mp4') {
                                    $model->media_url = $Media;
                                    $valid = true;
                                } elseif ($model->type == 'video' and pathinfo($Media)['extension'] == 'mp4') {
                                    $model->media_url = $Media;
                                    $valid = true;
                                }
                            }*/

                if (isset($_POST['postnow'])) {
                    $model->schedule_date = date("Y-m-d H:i:s", strtotime('-1 minutes'));
                    $model->is_scheduled = 1;
                }


                if (empty(Category::model()->findByPk($model->catgory_id)))
                    $model->catgory_id = 1;


                if ($valid) {
                    if ($valid_images_news) {
                        $imagesNews->media = $model->media_url;
                        $imagesNews->news_id = $model->news_id;
                        $imagesNews->id = null;
                        $imagesNews->type = 'gallery';
                        $imagesNews->created_at = date('Y-m-d H:i:s');
                        $imagesNews->save(true);
                    }

                    if ($model->save(true)) {
                        unset(Yii::app()->session['medi']);

                        Yii::app()->user->setFlash('update', 'Thank you for add conetns ... . .... ');
                        if (isset($_POST['postqueue']))
                            $this->redirect(array('postQueue/main'));
                        elseif (isset($_POST['review']))
                            $this->redirect(array('view', 'id' => $model->id));
                        elseif (isset($_POST['postnow']))
                            $this->redirect(array('view', 'id' => $model->id));

                    }
                }
            }
        }else{
            Yii::app()->user->setFlash('error', 'Thank you for add conetns ... . .... ');
        }
        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionUpdate_posted($id)
    {
        if(isset($_POST['medias'])){
            Yii::app()->session['medi'] = $_POST['medias'];
        }


        $model = $this->loadModel($id);
        $clone=new PostQueue();
        $model->scenario = 'update';
        $valid = true;
        $this->performAjaxValidation($model);
        if (isset($_POST['PostQueue'])) {

            $error = false;
            if($model->is_posted == 2){
                $error = true;
            }

            if($error){
                $model->is_posted =0;
                $model->errors = null;
            }

            $Media = $model->media_url;

            $Type = $model->type;
            $Link = $model->link;
            $error = false;
            if ($model->is_posted == 2) {
                $error = true;
            }
            $model->attributes = $_POST['PostQueue'];
            if ($error) {
                $model->is_posted = 0;
                $model->errors = null;
            }
            $model->validate();
            $model->attributes = $_POST['PostQueue'];

            $imagesNews = new MediaNews();
            $valid_images_news=false;

            if ($this->validateTypeAndPlatform($model, $model->platforms->title, 'type')) {
                if ($model->type == 'image') {
                    if(!empty(Yii::app()->session['medi'])) {
                        if ($this->FileEmpty($model, 'media_url')) {
                            if (!$this->GetFileType($model, $Media, 'image')) {
                                $model->media_url = Yii::app()->session['medi'];
                                $valid_images_news =false;

                            } else {
                                $model->media_url = $this->uploader($model, 'media_url', 'post_queue_edit_id' . $model->id . '_' . time());
                                Yii::app()->session['medi']=$model->media_url;
                                $valid_images_news =true;


                            }
                        }else{
                            $model->media_url = Yii::app()->session['medi'];
                            $valid_images_news =false;

                        }
                    }else {
                        if (!$this->FileEmpty($model, 'media_url')) {
                            if (!$this->GetFileType($model, $Media, 'image')) {
                                $model->addError('media_url', 'It seems you Didn\'t upload a proper image file ..');
                                $valid = false;
                            } else {
                                $model->media_url = $Media;
                                $valid_images_news =false;

                            }
                        } else {
                            if (!$this->GetFileType($model, 'media_url', 'image', 'upload')) {
                                $model->addError('media_url', "It seems you Didn't upload a proper image file ..");
                                $valid = false;
                            } else
                                $model->media_url = $this->uploader($model, 'media_url', 'post_queue_edit_id' . $model->id . '_' . time());
                            $valid_images_news =true;

                        }
                    }
                    if(isset($model->media_url)) {
                        $imagesNews->media = $model->media_url;





                    }
                }
                if ($model->type == 'video') {
                    if (!$this->FileEmpty($model, 'media_url')) {
                        if (!$this->GetFileType($model, $Media, 'video')) {
                            $model->addError('media_url', 'It seems you Didn\'t upload a proper video file ..');
                            $valid = false;
                        } else {
                            $model->media_url = $Media;
                        }
                    } else {
                        $valid = $this->validateVideoUploader($model, 'media_url', $model->platforms->title, 'post_queue_edit_id' . $model->id . '_' . time());
                    }

                }
                if ($model->type == 'youtube') {
                    $valid = $this->Youtube($model, 'youtube', 'media_url', 'type', 'youtube_PostQueue_');
                    if ($valid) $valid = $this->validateDownloadVideoUploader($model, 'media_url', $model->platforms->title, 'video_PostQueue_' . time(), 'type', 'youtube');
                }

                if ($model->type == 'preview') {
                    if(!empty(Yii::app()->session['medi'])) {
                        if ($this->FileEmpty($model, 'media_url')) {
                            if (!$this->GetFileType($model, $Media, 'image')) {
                                $model->media_url = Yii::app()->session['medi'];
                                $valid_images_news =false;

                            } else {
                                $model->media_url = $this->uploader($model, 'media_url', 'post_queue_edit_id' . $model->id . '_' . time());
                                $valid_images_news =true;


                            }
                        }elseif(isset(Yii::app()->session['medi'])){
                            $model->media_url = Yii::app()->session['medi'];
                            $valid_images_news =false;
                        }else{
                            $model->media_url =null;
                            $valid_images_news =false;

                        }
                    }else {
                        if (!$this->FileEmpty($model, 'media_url')) {
                            if (!$this->GetFileType($model, $Media, 'image')) {
                            } else {
                                $model->media_url = $Media;
                                $valid_images_news =false;

                            }
                        } else {
                            if (!$this->GetFileType($model, 'media_url', 'image', 'upload')) {
                                $model->addError('media_url', "It seems you Didn't upload a proper image file ..");
                                $valid = false;
                            } else
                                $model->media_url = $this->uploader($model, 'media_url', 'post_queue_edit_id' . $model->id . '_' . time());
                            $valid_images_news =true;

                        }
                    }
                    if (!empty($model->link)) {
                        if (strpos($model->link, 'goo.gl') == false) {
                            if (empty($Link)) {
                                if ($model->type == 'preview') {
                                    if (!empty($model->link)) {
                                        $shortUrl = new ShortUrl();
                                        $model->link = $shortUrl->short(urldecode($model->link));
                                    }
                                }
                            }
                        }
                    }
                }
            } else
                $valid = false;
            /*
                        if (!empty($Media)) {
                            if ($model->type == 'image' and pathinfo($Media)['extension'] == 'mp4') {
                                $model->media_url = $Media;
                                $valid = true;
                            } elseif ($model->type == 'video' and pathinfo($Media)['extension'] == 'mp4') {
                                $model->media_url = $Media;
                                $valid = true;
                            }
                        }*/

            if(isset($_POST['postnow'])){
                $model->schedule_date= date("Y-m-d H:i:s", strtotime('-1 minutes'));
                $model->is_scheduled =1;
            }
            if ($valid) {
                if($valid_images_news){
                    $imagesNews->media = $model->media_url;
                    $imagesNews->news_id = $model->news_id;
                    $imagesNews->id=null;
                    $imagesNews->type = 'gallery';
                    $imagesNews->created_at=date('Y-m-d H:i:s');
                    $imagesNews->save(true);
                }
                $clone->attributes=$model->attributes;
                $clone->id = null;
                $clone->parent_id=null;
                $clone->generated = 'clone';
                $clone->is_posted=0;
                if ($clone->save(true)) {
                    unset(Yii::app()->session['medi']);

                    Yii::app()->user->setFlash('update', 'Thank you for add conetns ... . .... ');
                    if(isset($_POST['postqueue']))
                        $this->redirect(array('postQueue/main'));
                    elseif(isset($_POST['review']))
                        $this->redirect(array('view', 'id' => $clone->id));
                    elseif(isset($_POST['postnow']))
                        $this->redirect(array('view', 'id' => $clone->id));

                }
            }
        }
        $this->render('update_posted', array(
            'model' => $model,
        ));
    }
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return PostQueue the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = PostQueue::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    /**
     * Performs the AJAX validation.
     * @param PostQueue $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'post-queue-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionGetAll()
    {
        if (isset($_POST['id'])) {

            $id = $_POST['id'];
            $platform = Platform::model()->findByPk($id);
            echo CJSON::encode($array = Yii::app()->params['rule_array'][strtolower($platform->title)]);
        }
    }

    public function actionActivatePost($id)
    {
        $this->layout = 'ajax';
        $reg_user = PostQueue::model()->findByPk($id);
        if ($_POST['all']) {

            if (!$reg_user->parent_id) {

                $reg_user->is_scheduled = 1;
                $reg_user->save();

                echo 'null<br>';
            }

            if ($reg_user->parent_id == null and $_POST['all']) {
                $update = Yii::app()->db->createCommand()
                    ->update('post_queue',
                        array(
                            'is_scheduled' => new CDbExpression('1'),
                        ),
                        'parent_id=:id and is_scheduled=:is',
                        array(':id' => $reg_user->id, ':is' => 0)
                    );
                echo $update;
            }
            if ($reg_user->parent_id and $_POST['all'] == 1) {

                $update = Yii::app()->db->createCommand()
                    ->update('post_queue',
                        array(
                            'is_scheduled' => new CDbExpression('1'),
                        ),
                        'parent_id=:id and is_scheduled=:is',
                        array(':id' => $reg_user->parent_id, ':is' => 0)
                    );
                $update = Yii::app()->db->createCommand()
                    ->update('post_queue',
                        array(
                            'is_scheduled' => new CDbExpression('1'),
                        ),
                        'id=:id and is_scheduled=:is',
                        array(':id' => $reg_user->parent_id, ':is' => 0)
                    );

                echo $update;

            }

            if ($reg_user->parent_id and $_POST['all'] == 0) {

                $update = Yii::app()->db->createCommand()
                    ->update('post_queue',
                        array(
                            'is_scheduled' => new CDbExpression('1'),
                        ),
                        'parent_id=:id and is_scheduled=:is',
                        array(':id' => $reg_user->parent_id, ':is' => 0)
                    );

                echo $update;

            }
        } else {
            $reg_user->is_scheduled = 1;
            $reg_user->save();

        }


    }

    public function actionGetPosts(){




        if($_POST['parent_id'] == null){



            $criteria=new CDbCriteria;
            $criteria->compare('parent_id',$_POST['id']);
            $posts =  PostQueue::model()->findAll($criteria);
            $array = array('all'=>true);
            foreach($posts as $post){

                if(date('Y-m-d H:i:s',strtotime($post->schedule_date)) < date('Y-m-d H:i:s')){
                    $array['all']=false;
                    break;
                }

            }

            echo CJSON::encode($array);

        }else{

             $criteria=new CDbCriteria;
            $criteria->addCondition('parent_id = '.$_POST['parent_id']. ' and platform_id !='.$_POST['platform_id'].' or id = '.$_POST['parent_id']);
            $posts =  PostQueue::model()->findAll($criteria);
            $array = array('all'=>true);
             foreach($posts as $post){

                if(date('Y-m-d H:i:s',strtotime($post->schedule_date)) < date('Y-m-d H:i:s')){
                    $array['all']=false;
                    break;
                }

            }

            echo CJSON::encode($array);
        }
    }

    public function actionPush_post($id)
    {
        $model = PostQueue::model()->findByPk($id);

        if ($model->is_posted == 0 or $model->is_posted == 2) {
        if (isset($_POST['all'])) {

            if ($_POST['all']) {



                    if (!$model->parent_id) {

                        $update = Yii::app()->db->createCommand()->update('post_queue',
                            array(
                                'schedule_date' => date("Y-m-d H:i:s", strtotime('-1 minutes')),

                            ),
                            'parent_id=:id',
                            array(':id' => $id)
                        );
                        if ($model === null)
                            throw new CHttpException(404, 'The requested page does not exist.');
                        $model->schedule_date = date("Y-m-d H:i:s", strtotime('-1 minutes'));
                        $model->save(false);
                        $url = Yii::app()->createUrl('/site') . '?id=' . $id;


                        echo json_encode(array('valid' => true, 'url' => $url, 'all' => true));

                    } else {

                        $update = Yii::app()->db->createCommand()
                            ->update('post_queue',
                                array(
                                    'schedule_date' => date("Y-m-d H:i:s", strtotime('-1 minutes')),

                                ),
                                'parent_id=:id',
                                array(':id' => $model->parent_id)
                            );

                        $update = Yii::app()->db->createCommand()
                            ->update('post_queue',
                                array(
                                    'schedule_date' => date("Y-m-d H:i:s", strtotime('-1 minutes')),

                                ),
                                'id=:id',
                                array(':id' => $model->parent_id)
                            );

                        if ($model === null)
                            throw new CHttpException(404, 'The requested page does not exist.');
                        $model->schedule_date = date("Y-m-d H:i:s", strtotime('-1 minutes'));

                        $model->save(false);
                        $url = Yii::app()->createUrl('/site') . '?id=' . $id;


                        echo json_encode(array('valid' => true, 'url' => $url, 'all' => true));


                    }

                } else {
                    $model = PostQueue::model()->findByPk($id);
                    if ($model === null)
                        throw new CHttpException(404, 'The requested page does not exist.');
                    $model->schedule_date = date("Y-m-d H:i:s", strtotime('-1 minutes'));

                    $model->save(false);
                    $url = Yii::app()->createUrl('/site') . '?id=' . $id;


                    echo json_encode(array('valid' => true, 'url' => $url, 'all' => true));


                }
            }


        }else{
            echo 'post is already posted';
        }
    }

    public function actionRe_post($id)
    {
        $parent = false;
        $parent_id=null;
        if (isset($_POST['all'])) {

            if ($_POST['all']) {


                $model = PostQueue::model()->findByPk($id);
                $model = $model->GetPostRePost($model->id,$model->platform_id,$model->parent_id);
                $clone = new PostQueue();

                foreach($model as $mod) {
                    $mod->schedule_date = date("Y-m-d H:i:s", strtotime('-1 minutes'));

                    $clone->attributes = $mod->attributes;
                    $clone->id = null;
                    $clone->is_posted = 0;
                    $clone->generated = 'clone';
                    if ($parent == false) {
                        $clone->parent_id = null;
                    } else
                        $clone->parent_id = $parent_id;
                    $clone->setIsNewRecord(true);
                    if ($clone->save(false)) {
                        if($parent==false)
                        $parent_id=$clone->id;

                        $parent = true;
                    }


                    $url = Yii::app()->createUrl('/site') . '?id=' . $id;


                    echo json_encode(array('valid' => true, 'url' => $url, 'all' => true));
                }


            } else {
                $model = PostQueue::model()->findByPk($id);
                $clone = new PostQueue();
                $model->schedule_date = date("Y-m-d H:i:s", strtotime('-1 minutes'));
                $clone->attributes=$model->attributes;
                $clone->id=null;
                $clone->is_posted =0;
                $clone->generated = 'clone';

                if ($parent == false){
                    $clone->parent_id = null;
                }
                else
                    $clone->parent_id=$parent_id;
                $clone->setIsNewRecord(true);
                if($clone->save(false)){
                    if($parent==false)
                    $parent_id=$clone->id;
                    $parent=true;
                }
                $url = Yii::app()->createUrl('/site') . '?id=' . $id;


                echo json_encode(array('valid' => true, 'url' => $url, 'all' => true));




            }


        }
    }
    protected function performAjaxValidation2()
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'generator-post-form') {
            echo CActiveForm::validate($this->model);
            Yii::app()->end();
        }
    }
    public function actionGetHashtags(){

     if(isset($_POST['post']) && !empty($_POST['post'])){
        $text = trim($_POST['post']);
        $text = ' '.$text.' ';
        $d = Hashtag::model()->findAll('deleted = 0');
        $trim_hash = array();
        $hash_tag = array();
        foreach ($d as $index=>$item) {
            $trim_hash[]=" #".str_replace(" ", "_",trim($item->title)).' ';
            $hash_tag[]=' '.trim($item->title).' ';
            $trim_hash[]=PHP_EOL."#".str_replace(" ", "_",trim($item->title)).' ';
            $hash_tag[]=PHP_EOL.trim($item->title);
            $trim_hash[]=" #".str_replace(" ", "_",trim($item->title)).PHP_EOL;
            $hash_tag[]=' '.trim($item->title).PHP_EOL;

        }
        $text = str_replace($hash_tag, $trim_hash,$text);
        echo  trim(str_replace('# ', '#', $text));
    }

    }


}
