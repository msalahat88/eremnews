<?php

class HashtagController extends BaseController
{


	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(

			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','index','view','edit','admin','delete'),
				'users'=>array(Yii::app()->user->getState('type')),
			),
		/*	array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array(Yii::app()->user->getState('type')),
			),*/
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Hashtag;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Hashtag']))
		{
			$model->attributes=$_POST['Hashtag'];
			$model->created_at = date('Y-m-d H:i:s');



			if($model->save()) {
				Yii::app()->user->setFlash('create', 'Thank you for add conetns ... . .... ');
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
	 $this->performAjaxValidation($model);

		if(isset($_POST['Hashtag']))
		{
			$model->attributes=$_POST['Hashtag'];
			$model->created_at = date('Y-m-d H:i:s');

			if($model->save()) {
				Yii::app()->user->setFlash('update', 'Thank you for add conetns ... . .... ');
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$model->deleted = 1;
		$model->save();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Hashtag('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Hashtag']))
			$model->attributes=$_GET['Hashtag'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$models=new Hashtag;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($models);

		if(isset($_POST['Hashtag']))
		{
			$models->attributes=$_POST['Hashtag'];
			$models->created_at = date('Y-m-d H:i:s');



			if($models->save()) {
				Yii::app()->user->setFlash('create', 'Thank you for add conetns ... . .... ');
				$this->redirect(array('admin'));

			}
		}

		$model=new Hashtag('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Hashtag'])) {
			$this->data_search = $_GET['Hashtag'];
			$this->data_search($model);
			$model->attributes = $_GET['Hashtag'];
		}
		$this->render('admin',array(
			'model'=>$model,
			'models'=>$models,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Hashtag the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Hashtag::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Hashtag $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='hashtag-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionEdit(){
		if(isset($_POST)){
			if(isset($_POST['name']) && isset($_POST['value']) && isset($_POST['pk']) && isset($_POST['scenario'])){
				$model = $this->loadModel($_POST['pk']);

				if(!empty($_POST['name'])){
					$name = $_POST['name'];
					$value = $_POST['value'];
					$model->$name = $value;
					if($model->save())
						echo true;
					else
						echo false;
				}
			}

		}
	}
}
