<?php
class ArchivesNewsCommand extends BaseCommand{

    public function run($args){
        $this->TimeZone();
        $data = $this->GetPostBeforeOneMonth();

        foreach ($data as $item) {

            if($this->SetNewArchives($item)){
                $item->command=false;
              echo $item->delete();
            }
        }

    }

    public function GetPostBeforeOneMonth(){

        $cond = new CDbCriteria();
        //

        $date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime(date("Y-m-d")." -1 month"))));

        $cond->condition = 'DATE_FORMAT(created_at,"%Y-%m-%d") <="'.$date.'"';

        return News::model()->findAll($cond);

    }

    public function SetNewArchives($data){
        $model = new ArchivesNews();
        $model->attributes = $data->attributes;
        $model->is_archives =1;
        $model->command=false;
        $model->setIsNewRecord(true);
        $model->id = null;
        return $model->save();

    }

}