<?php
class InstagramCommand extends BaseCommand
{
    private $id = null;

    private $id_instagram = null;

    private $post = null;

    private $error= null;

    public function run($args)
    {
        $this->TimeZone();
        $data = $this->News();


        if (!empty($data)) {
            foreach ($data as $value) {
                if(!empty($value->news_id)){
                    $this->platform_name ='Instagram';
                    $this->type_post=$value->type;
                    $this->check_and_update_news($value->news_id,$value->id,$value->platform_id,$value->template_id);

                    $value=PostQueue::model()->findByPk($value->id);

                }

                //catch the post before posting processing...
                $value->is_posted=3;
                $value->save(false);


                $this->id = $value->id;
                $valid = false;
                switch ($value->type) {
                    case 'Image':
                        $this->post = $value;
                        if ($this->image()) {
                            $valid = true;
                        }
                        break;
                    case 'Video':
                        $this->post = $value;
                        //  if( $this->video()){
                        // $valid = true;
                        // }
                        break;
                }

                if ($valid) {
                    $value->is_posted = 1;
                    $value->post_id = $this->id_instagram;
                    $value->command = false;
                    $value->save();
                }else{
                    $value->is_posted = 2;
                    $value->errors= $this->error;
                    $value->command = false;
                    //$this->send_email('instagram','error on instagram');

                    $value->save();
                }
                break;
            }
        }

        die;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @return PostQueue the News
     * @var $data PostQueue
     * @var $value PostQueue
     * @var $db PostQueue
     */

    public function News()
    {

        return PostQueue::model()->get_post_instagram();

    }

    public function image()
    {
        $valid = true;
        if (!empty($this->post->media_url)) {
            $image = Yii::app()->params['webroot'] . '/image/instagram_'.time().'.jpg';

            $this->getImageFromUrl($this->post->media_url,$image);
            $this->scaleImage($image);

        } else {
            $this->error = 'Image empty';

            if(isset($image) && file_exists($image))
                unlink($image);
            return false;
        }

        if($valid){
            $page = 'main';
            if(!empty($this->post->catgory_id))
                if(isset($this->post->catgory->page_index))
                    $page = $this->post->catgory->page_index;
            $data = $this->instagram_image($image,$this->post->post, Yii::app()->params[$page]['instagram_auth']);

            if(isset($data['status']) && $data['status'] == 'ok'){
                $this->id_instagram = $data['media']['id'];
                if(isset($image) && file_exists($image))
                    unlink($image);
                return true;
            }else{
                if(isset($image) && file_exists($image))
                    unlink($image);
                return false;
            }
        }

        return false;

    }


    private function getImageFromUrl($media,$image){

        $ch = curl_init($media);

        $fp = fopen($image, 'wb');


        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        $result = curl_exec($ch);

        curl_close($ch);

        fclose($fp);

        return $result;

    }
}
