<?php

class GeneratorsCommand extends BaseCommand
{
    // Please to use this command , run every 15 mint

    private $start_time;

    private $end_time;

    private $per_post_day;

    private $direct_push_start;

    private $direct_push_end;

    private $is_custom_settings;

    private $is_direct_push;

    private $scheduled_counter = 0;

    private $gap_time = 0;

    private $PlatFrom;

    private $rule;

    private $template;

    private $main_hash_tag;

    private $hash_tag;

    private $trim_hash;

    private $category;

    private $sub_category;

    private $settings;

    private $image_instagram_scheduled = false;

    public $array_match_pics = array('صور','الصور','بالصور','فيديو','بالفيديو','الفيديو','(فيديو)');

    public $array_erem_words = array('| إرم نيوز','#إرم_نيوز','|إرم نيوز','| إرم نيوز');


    private $general_image= null;

    public function run($args){

        $files = glob(Yii::app()->params['webroot'].'/video/*'); // get all file names

        foreach($files as $file){ // iterate files
            if(is_file($file))
                unlink($file); // delete file
        }

        Yii::import('application.modules.features.models.*');

        system("clear");

        $this->TimeZone();

        $this->template = PostTemplate::model()->findAll('deleted = 0 ORDER BY RAND()');

        if (empty($this->template)) {

                echo '[App] : Please insert template '.PHP_EOL;

            exit();
        }


        $this->general_image = Yii::app()->params['domain']. 'image/general.jpg';

        $this->rule = Yii::app()->params['rules'];

        $this->main_hash_tag = Yii::app()->params['HashTagRoot'];

        $this->category = Category::model()->findAllByAttributes(array('deleted' => 0, 'active' => 1));

        $this->sub_category = SubCategories::model()->findAllByAttributes(array('deleted' => 0, 'active' => 1));

        $this->settings = Settings::model()->findByPk(1);

        $this->PlatFrom = Platform::model()->findAllByAttributes(array('deleted' => 0));

        $this->per_post_day = $this->settings->how_many_posts;

        $this->start_time = $this->settings->start_date_time;

        $this->end_time = $this->settings->end_date_time;

        $this->gap_time = $this->settings->gap_time;

        $this->direct_push_start = $this->settings->direct_push_start;

        $this->direct_push_end = $this->settings->direct_push_end;

        $this->is_custom_settings = false;

        $this->is_direct_push = $this->settings->direct_push;


        $criteria = New CDbCriteria();

        $criteria->compare('is_scheduled', 1, true);

        $criteria->addCondition('parent_id IS NULL');

        $day = 0;

        $today = date('Y-m-d', strtotime(date('Y-m-d') . ' + ' . $day . ' day'));

        $start = date("Y-m-d H:i:s", strtotime($today . ' ' . $this->start_time));

        $end = date("Y-m-d H:i:s", strtotime($today . ' ' . $this->end_time));

        if ($end < $start)
            $criteria->addBetweenCondition('schedule_date', date('Y-m-d'), date('Y-m-d', strtotime(date('Y-m-d') . ' 1 day')), 'AND');
        else
            $criteria->compare('schedule_date', date('Y-m-d'), true);

        $this->scheduled_counter = PostQueue::model()->count($criteria);

        /** add cron*/

        if(isset($args[1]) and $args[1] == true)
            $this->debug = true;

        if(isset($args[0])){

            if ($args[0] == 'dom')
                $this->Feed_dom_category();
            else
                exit('[Generator] : Please add category or sub_category after command :  {[ php yiic generators rss ]} {[ php yiic generators dom ]} '.PHP_EOL);

        }else{
            exit('[Generator] : Please add category or sub_category after command : {[ php yiic generators rss ]} {[ php yiic generators dom ]}'.PHP_EOL);
        }

    }

    private function Feed_dom_category()
    {
        if (!is_array($this->category)) {
            echo '[Database] : category empty check please ' . PHP_EOL;
            exit();
        }

        $html = new SimpleHTMLDOM();
        if (!empty($this->category)) {
            foreach ($this->category as $sub) {
                if ($sub->type == 'dom'){
                    if (!empty($sub->url)) {
                        $link = $sub->url;
                        if(strpos($link,'?')=== false){
                            $link.='?clear='.time();
                        }else{
                            $link.='&clear='.time();
                        }
                        $html_url = $html->file_get_html($link);
                        if (isset($html_url)) {
                            $categories_predication = CategoryLevel::model()->findAllByAttributes(array('source_id' => $sub->id));
                            foreach ($categories_predication as $category_predict) {
                                $find = $category_predict->predication;
                                $list = $html_url->find($find);
                                if (isset($list)) {
                                    foreach ($list as $item) {
                                        $url = null;
                                        $create = null;
                                        $date = null;
                                        if (isset($item->href) && !empty($item->href)) {
                                            if (strpos($item->href, 'http') === 0) {
                                                $url = $item->href;
                                            } else
                                                $url = Yii::app()->params['feedUrl'] . $item->href;


                                            $data['sub'] = $sub;

                                            $data['sub_category_id'] = null;

                                            $data['category_id'] = $sub->id;

                                            $data['creator'] = null;

                                            $data['column'] = null;

                                            $data['num_read'] = null;

                                            $data['title'] = null;

                                            $data['link'] = $url;

                                            $data['link_md5'] = md5($url);

                                            $data['description'] = null;

                                            $data['body_description'] = null;

                                            $data['shorten_url'] = null;

                                            $data['publishing_date'] = $date;

                                            if (!empty($data['link'])) {

                                                list(
                                                    $title,
                                                    $description,
                                                    $body_description,
                                                    $video,
                                                    $image_og,
                                                    $gallery,
                                                    $publishing_date,
                                                    $author,
                                                    $tags
                                                    ) = $this->get_details($data['link'], $sub->id);

                                                $data['title'] = $title;

                                                $dates = str_replace("/", "-", $publishing_date);

                                                $date = date('Y-m-d', strtotime($dates));

                                                $data['publishing_date'] = $date;

                                                $data['description'] = $description;

                                                $data['body_description'] = $body_description;

                                                $data['creator'] = $author;

                                                $data['media']['video'] = $video;

                                                $data['media']['image'] = $image_og;

                                                $data['media']['gallery'] = $gallery;

                                                $data['tags'] = $tags;


                                                if ($data['publishing_date'] == date('Y-m-d')) {
                                                        $this->AddNews($data);
                                                } else{
                                                    echo '[Website] : This news is old date : ' . $data['publishing_date'] . ' Link is : ' . $url . PHP_EOL;
                                                    break;
                                                }
                                            } else {
                                                echo '[link] : Empty ' . $sub->url;

                                            }


                                        } else
                                            echo '[Website] : Url empty or does not exist in the current sub category id = ' . $sub->id . PHP_EOL;
                                    }
                                }
                            }
                        } else
                            echo '[Sub link] : There is no data in this sub category ' . $sub->title . PHP_EOL;


                    } else
                        echo '[Database] : Url empty in the current sub category id = ' . $sub->id . PHP_EOL;
                }
            }
        }
    }

    private function get_details($link,$id)
    {
        if(strpos($link,'?')=== false)
            $link.='?clear='.time();
        else
            $link.='&clear='.time();

        $html = new SimpleHTMLDOM();
        $html_url = $html->file_get_html($link);
        $page_details = PageDetails::model()->findAllByAttributes(array('source_id'=>$id));
        $author=null;
        $gallery = null;
        $publishing_date = null;
        $body_description = null;
        $description = null;
        $image = $this->general_image;
        $video =null;
        $title=null;
        $tags=null;
        foreach($page_details as $detail) {

            //Author
            if (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'creator') {
                $author = trim($this->clear_tags(trim($html_url->find($detail->predication, 0)->content)));

            }
            elseif (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'creator') {
                $author = trim($this->clear_tags(trim($html_url->find($detail->predication, 0)->plaintext)));
            }
            //End author

            //title
            if (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'title') {
                foreach ($this->array_erem_words as $array_matches){
                    if (preg_match('/\b' . $array_matches . '\b/u',$html_url->find($detail->predication, 0)->content, $matches)) {
                        $title = str_replace($array_matches,'',$html_url->find($detail->predication, 0)->content);
                    }
                }
                if(!empty($title))
                    $title = trim($this->clear_tags(trim($title)));
                else
                    $title= trim($this->clear_tags(trim($html_url->find($detail->predication, 0)->content)));
            }
            elseif (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'title') {
                foreach ($this->array_erem_words as $array_matches){
                    if (preg_match('/\b' . $array_matches . '\b/u',$html_url->find($detail->predication, 0)->content, $matches)) {
                        $title= str_replace($array_matches,'',$html_url->find($detail->predication, 0)->content);
                    }
                }
                if(!empty($title))
                    $title = trim($this->clear_tags(trim($title)));
                else
                    $title = trim($this->clear_tags(trim($html_url->find($detail->predication, 0)->plaintext)));
            }


            //End title


            //Body desc
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'body_description') {
                $body_desc = $html_url->find($detail->predication);
                if (isset($body_desc)) {
                    foreach ($body_desc as $bodies) {
                        $body_description .= $this->clear_tags(trim($bodies->plaintext));
                    }
                }
            }elseif (!empty($html_url->find($detail->predication,0)->plaintext) and isset($html_url->find($detail->predication,0)->plaintext) and $detail->pageType->title == 'body_description') {
                $body_description = $this->clear_tags(trim($html_url->find($detail->predication,0)->plaintext));
            }


            //End body desc

            //Description
            if (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'description') {
                $description = $this->clear_tags(trim($html_url->find($detail->predication, 0)->plaintext));
            }
            elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'description') {
                $description = $this->clear_tags(trim($html_url->find($detail->predication, 0)->content));
            }


            //End Description

            //Publishing date
            if (isset($html_url->find($detail->predication, 0)->datetime) and !empty($html_url->find($detail->predication, 0)->datetime) and $detail->pageType->title == 'publishing_date') {
                $publishing_date = $this->get_date($html_url->find($detail->predication, 0)->datetime);
            }
            if (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'publishing_date') {
                $publishing_date = $this->get_date($html_url->find($detail->predication, 0)->plaintext);
            } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'publishing_date') {
                $publishing_date = $this->get_date($html_url->find($detail->predication, 0)->content);
            }
            //End publishing date

            //All media
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'all_media') {
                $script = $html_url->find($detail->predication);
                if (!empty($script)) {
                    foreach ($script as $sc) {
                        $media = (array)json_decode($sc->innertext);
                    }
                    if (!empty($media)) {
                        if (isset($media['videos'])) {
                            $video = $media['videos'][0];
                        }
                        if (isset($media['images'])) {
                            foreach ($media['images'] as $gall) {
                                $image = Yii::app()->params['feedUrl'] . $media['images'][0];
                                if ($gall != $media['images'][0])
                                    $gallery[] = Yii::app()->params['feedUrl'] . $gall;
                            }
                        }
                    }
                }
            }
            //End all media


            //image
            if (isset($html_url->find($detail->predication, 0)->src) and !empty($html_url->find($detail->predication, 0)->src) and $detail->pageType->title == 'image') {
                if (strpos($html_url->find($detail->predication, 0)->src, 'http') === 0) {
                    $image = $html_url->find($detail->predication, 0)->src;

                }else
                    $image = Yii::app()->params['feedUrl'].$html_url->find($detail->predication, 0)->src;
            } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'image') {
                if (strpos($html_url->find($detail->predication, 0)->content, 'http') === 0)
                    $image = $html_url->find($detail->predication, 0)->content;
                else
                    $image = Yii::app()->params['feedUrl'].$html_url->find($detail->predication, 0)->content;

            }
            //end image

            //video
            if (isset($html_url->find($detail->predication, 0)->src) and !empty($html_url->find($detail->predication, 0)->src) and $detail->pageType->title == 'video') {
                $video = $html_url->find($detail->predication, 0)->src;
            } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'video') {
                $video = $html_url->find($detail->predication, 0)->content;

            }else if (isset($html_url->find($detail->predication, 0)->data) and !empty($html_url->find($detail->predication, 0)->data) and $detail->pageType->title == 'video') {
                $video = $html_url->find($detail->predication, 0)->data;
            }
            //end video

            //gallery
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'gallery') {
                $gall = $html_url->find($detail->predication);
                foreach ($gall as $item) {
                    if (strpos($html_url->find($detail->predication, 0)->src, 'http') === 0) {
                        if($image != $item->src)
                            $gallery[]['src'] =$item->src;
                    }else {
                        if ($image != Yii::app()->params['feedUrl']. $item->src)
                            $gallery[]['src'] = Yii::app()->params['feedUrl'].$item->src;
                    }
                }

            }
            //End gallery

            //Tags
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'tags') {
                $tags_counter = $html_url->find($detail->predication);
                foreach ($tags_counter as $tag) {
                        $tag_space = trim($tag->plaintext);
                        $tag_space = preg_replace("/[\s_]/", "_", $tag_space);
                        $tags[] = $this->clear_tags($tag_space);
                }

            }
            //Tags
        }

        return array($title,$description,$body_description,$video,$image,$gallery,$publishing_date,$author,$tags);
    }

    private function custom_settings($times,$platform_id,$cat_id){

        $this->is_custom_settings =true;

        $condition = new CDbCriteria();

        $condition->order = 'id Desc';

        $condition->limit = 1;

        $today = date('Y-m-d');

        $condition->condition = 'is_scheduled = 1 and settings="custom" and platform_id = '.$platform_id.' and catgory_id = '.$cat_id;

        $check = PostQueue::model()->find($condition);

        $final_date = strtotime(date('Y-m-d H:i:s'));

        $is_scheduled = 0;

        $obj = null ;

        foreach ($times as $timeItem) {

            $timeItem=(object)$timeItem;

            $obj = $timeItem->obj_id;
            if ($timeItem->is_direct_push) {

                $direct_start = strtotime(date('Y-m-d') . ' ' . $timeItem->direct_push_start_time);
                $direct_end = strtotime(date('Y-m-d') . ' ' . $timeItem->direct_push_end_time);

                if ($direct_end < $direct_start) {
                    $extra_day = 1;
                    $direct_end = strtotime(date('Y-m-d') . ' ' . $timeItem->direct_push_end_time . ' + ' . $extra_day . ' day');
                }
                $now = strtotime(date('Y-m-d H:i:s'));
                if (($now >= $direct_start) && ($now <= $direct_end)) {
                    $final_date = strtotime(date('Y-m-d H:i:s') . ' - 1 minute');
                    $is_scheduled = 1;
                    break;
                }

            }

            $start = strtotime(date('Y-m-d') . ' ' . $timeItem->start_time);

            $end = strtotime(date('Y-m-d') . ' ' . $timeItem->end_time);

            $now = strtotime(date('Y-m-d H:i:s') . ' + ' . $timeItem->gap_time . ' minutes');


            if ($end < $start) {
                $extra_day = 1;
                $end = strtotime(date('Y-m-d') . ' ' . $timeItem->end_time . ' + ' . $extra_day . ' day');
            }




            if (empty($check)) {
                if (($now <= $start)) {
                    $final_date = $start;
                    $is_scheduled = 1;

                } elseif ($now <= $end) {
                    $final_date = $now;
                    $is_scheduled = 1;

                }
            } else {
                $date = strtotime($check->schedule_date . ' + ' . $timeItem->gap_time . ' minutes');
                if ($date < $now) {
                    $date = $now;
                }

                if (($date <= $start)) {
                    $final_date = $start;
                    $is_scheduled = 1;

                } elseif ($date <= $end) {
                    $final_date = $date;
                    $is_scheduled = 1;

                }
            }

        }


        $post_date = date('Y-m-d H:i:s', $final_date);

        return array($post_date, $is_scheduled ,$obj);

    }

    private function date($cat_id,$platform_id){

        $times=  Yii::app()->db->createCommand("
                                      SELECT
                platform_category_settings.obj_id,
                platform_category_settings.platform_id,
                time_settings.start_time,
                time_settings.end_time,
                time_settings.is_direct_push,
                time_settings.direct_push_start_time,
                time_settings.direct_push_end_time,
                time_settings.gap_time
                FROM platform_category_settings
                
                INNER JOIN days_settings
                ON days_settings.platform_category_settings_id =platform_category_settings.id
                
                INNER JOIN day
                ON days_settings.day_id =day.id
                
                INNER JOIN time_settings
                ON time_settings.platform_category_settings_id =platform_category_settings.id
                INNER JOIN settings_obj
                ON settings_obj.id =platform_category_settings.obj_id
                
                 WHERE 
                 ( platform_category_settings.category_id = ".$cat_id." or platform_category_settings.category_id IS NULL )  
                 and 
                 platform_category_settings.platform_id = ".$platform_id."
                 and
                 day.title = DAYNAME(NOW())
                 and 
                 settings_obj.active = 1
                 ORDER BY category_id desc;
        ")->queryAll();


        if(!empty($times)){

            if($this->debug)
                echo '[App] : Custom Settings Platform id :  '.$platform_id.PHP_EOL;

            list($time,$is_scheduled ,$obj) =  $this->custom_settings($times,$platform_id,$cat_id);

            if($this->debug)
                echo '[App] : scheduled is '.$is_scheduled?'True':'false'.PHP_EOL;

            return array($time,$is_scheduled ,$obj);

        }

        $this->is_custom_settings = false;

        $final_date = null;

        $condition = new CDbCriteria();

        $condition->order = 'id Desc';

        $condition->limit = 1;

        $today = date('Y-m-d');

        $condition->condition = 'is_scheduled = 1 and settings="general" and platform_id = '.$platform_id;
        $check = PostQueue::model()->find($condition);
        $is_scheduled = 1;
        $now = strtotime(date('Y-m-d H:i:s') . ' + ' . $this->gap_time . ' minutes');
        $start = strtotime(date('Y-m-d') . ' ' . $this->start_time);
        $end = strtotime(date('Y-m-d') . ' ' . $this->end_time);

        if ($end < $start) {
            $extra_day = 1;
            $end = strtotime(date('Y-m-d') . ' ' . $this->end_time . ' + ' . $extra_day . ' day');
        }

        if (empty($check)) {
            if (($now <= $start)) {
                $final_date = $start;

            } elseif ($now <= $end) {
                $final_date = $now;
            } else {
                $final_date = $now;
                $is_scheduled = 0;
            }
        } else {
            $date = strtotime($check->schedule_date . ' + ' . $this->gap_time . ' minutes');
            if ($date < $now) {
                $date = $now;
            }

            if (($date <= $start)) {
                $final_date = $start;
            } elseif ($date <= $end) {
                $final_date = $date;
            } else {
                $final_date = $date;
                $is_scheduled = 0;
            }
        }

        if ($this->is_direct_push) {
            $direct_start = strtotime(date('Y-m-d') . ' ' . $this->direct_push_start);
            $direct_end = strtotime(date('Y-m-d') . ' ' . $this->direct_push_end);

            if ($direct_end < $direct_start) {
                $extra_day = 1;
                $direct_end = strtotime(date('Y-m-d') . ' ' . $this->direct_push_end . ' + ' . $extra_day . ' day');
            }
            $now = strtotime(date('Y-m-d H:i:s'));
            if (($now >= $direct_start) && ($now <= $direct_end)) {
                $final_date = strtotime(date('Y-m-d H:i:s') . ' -1 minute');
                $is_scheduled = 1;
            }
        }

        $post_date = date('Y-m-d H:i:s', $final_date);


        $times=  Yii::app()->db->createCommand("SELECT  platform_category_settings.obj_id FROM platform_category_settings
                INNER JOIN settings_obj ON settings_obj.id =platform_category_settings.obj_id
                 WHERE 
                 ( platform_category_settings.category_id = ".$cat_id." or platform_category_settings.category_id IS NULL )  
                 and 
                 platform_category_settings.platform_id = ".$platform_id."
                 and 
                 settings_obj.active = 1
        ")->queryAll();

        if($this->debug)
            echo '[App] : General Settings Platform id :  '.$platform_id.PHP_EOL;

        if(!empty($times))
            $is_scheduled = 0;

        if($this->debug)
            if(!empty($times))
                echo '[App] : This platform there are custom settings '.$platform_id.PHP_EOL;


        if($this->debug)
            if($is_scheduled)
                echo '[App] : scheduled is True'.PHP_EOL;
            else
                echo '[App] : scheduled is False'.PHP_EOL;


        return array($post_date, $is_scheduled ,false);

    }

    public function check_total_videos_without_platform(){

        $video_total=0;
        $cdb = new CDbCriteria();
        $cdb->compare('type', 'video');
        $cdb->compare('created_by',null);
        $cdb->compare('updated_by',null);
        $cdb->addBetweenCondition('created_at', date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59'), 'AND');
        $video_total += MediaNews::model()->count($cdb);
        return $video_total;


    }

    private function AddNews($data)
    {

        $news = new News();

        $news->id = null;

        $news->link = $data['link'];

        $news->link_md5 = $data['link_md5'];

        $news->category_id = $data['category_id'];

        $news->sub_category_id = $data['sub_category_id'];

        $news->title = $data['title'];

        $news->column = $data['column'];

        $news->description = $data['description'];

        $news->body_description = $data['body_description'];

        $news->publishing_date = $data['publishing_date'];

        $news->schedule_date = $data['publishing_date'];

        $news->created_at = date('Y-m-d H:i:s');

        $news->creator = $data['creator'];

        $news->setIsNewRecord(true);


        $check_news = News::model()->findByAttributes(array('link_md5' => $data['link_md5']));

        $go=false;

        $valid = false;

        if (empty($check_news)) {

            $shortUrl = new ShortUrl();

            $news->shorten_url = $shortUrl->short(urldecode($news->link));

            $valid = true;

            if($news->save(true)) {

                $this->new_tags($data);

                $go = true;
            }


            print_r($news->getErrors());



        }  else{
            if($this->debug)
                echo '[Database] : News is already exist ' . $news->id . PHP_EOL;
        }


        if($go && $valid) {

            $id = null;

            if (isset($data['media']['gallery']))
                $id = $this->AddMediaNews($news, 'gallery', $data['media']['gallery']);

            if (isset($data['media']['image']))
                $id = $this->AddMediaNews($news, 'image', $data['media']['image']);

            if (isset($data['media']['video']))
                $id = $this->AddMediaNews($news, 'video', $data['media']['video']);


            if (!empty($id)) {
                $this->get_new_tagsge($news->category->title,$data['tags']);
                $this->newsGenerator($news);
            }else{
                if($this->debug)
                    echo '[Database] : Error save media ' . $news->id . PHP_EOL;
            }
        }else{
            if($this->debug)
                echo '[Database] : Error save news ' . $news->id . PHP_EOL;
        }

    }

    public function add_main_sport_section($news,$data){

        $split = explode('/', $news->link);

        if(trim($split[3] == trim('sports'))) {

            $criteria = new CDbCriteria();
            $criteria->compare('link_md5', $data['link_md5']);
        $criteria->compare('category_id',$news->category->id);
        $check_main_sport_news = News::model()->findAll($criteria);
            if (empty($check_main_sport_news)) {
                $shortUrl = new ShortUrl();

                $news->shorten_url = $shortUrl->short(urldecode($news->link));

                $news->setIsNewRecord(true);
                if($news->save(false)) {
                    $this->new_tags($data);
                    return $news;
                }
            } else {
                echo "This sport section is already exist in main".PHP_EOL;
            }
        }else{
            echo "Sport section main link not sport".PHP_EOL;
        }
        return false;
    }

    public function AddMediaNews($news, $type, $data)
    {
        $id = null;

        if ($type == 'gallery') {
            foreach ($data as $item) {
                if (empty(MediaNews::model()->findByAttributes(array('media' => $item['src'], 'news_id' => $news->id, 'type' => $type)))) {
                    $media = new MediaNews();
                    $media->type = $type;
                    $media->media = $item['src'];
                    $media->news_id = $news->id;
                    $media->setIsNewRecord(true);
                    $media->save(false);
                    if ($id == null)
                        $id = $media->id;
                }
            }
            return $id;
        }

        if (empty(MediaNews::model()->findByAttributes(array('media' => $data, 'news_id' => $news->id, 'type' => $type)))) {
            $media = new MediaNews();
            $media->type = $type;
            $media->media = $data;
            $media->news_id = $news->id;
            $media->setIsNewRecord(true);
            $media->save(false);
            if ($id == null)
                $id = $media->id;

        }
        return $id;




    }

    public function new_tags($data){

        if(!empty($data['tags'])){
            foreach($data['tags'] as $tags) {
                $check_hashtags = Hashtag::model()->findByAttributes(array('category_id' => $data['category_id'], 'title' => $tags));
                if (empty($check_hashtags)) {
                    $new_hastags = New Hashtag();
                    $new_hastags->id = null;
                    $new_hastags->title = $tags;
                    $new_hastags->category_id = $data['category_id'];
                    $new_hastags->created_at = date('Y-m-d H:i:s');
                    $new_hastags->setIsNewRecord(true);
                    if ($new_hastags->save())
                        if($this->debug)
                        echo '[generate] : New hashtags'.$new_hastags->id.PHP_EOL;
                    else{
                        if($this->debug)
                        echo "[generate] : error on saving hashtags".PHP_EOL;
                    }
                }else{
                    if($this->debug)
                    echo "[generate] :Hashtag is already exist".PHP_EOL;
                }
            }
        }
    }

    private function get_new_tagsge($category_title,$tags){


        $this->trim_hash = array();
        $this->hash_tag = array();
        if(!empty($tags)) {
            foreach ($tags as $index => $item) {
                if (trim($item) != trim($category_title)) {
                    $this->trim_hash[$index] = " #" . str_replace(" ", "_", trim($item)) . ' ';
                    $this->hash_tag[$index] = ' ' . trim($item) . ' ';
                    $get_hashtag = false;
                }
            }
        }
    }

    public function Video_downloader($url){



        if($this->debug)
            echo '[generate] : now in downloader video  in news ' . PHP_EOL;

            $rx = '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i';

            $path = Yii::app()->params['webroot'] . '/video/video_' . time() . '.mp4';

                if (preg_match($rx, $url, $matches)) {

                    if($this->debug)
                        echo '[generate] : now in downloader video preg_match  in news ' . PHP_EOL;

                    $download_video_url = "'$url'";

                    exec("/usr/local/bin/youtube-dl -F $download_video_url", $out);

                    if (!empty($out)) {

                        if($this->debug)
                                echo '[generate] : now in downloader video if (!empty($out))   in news ' . PHP_EOL;

                        if (isset(explode(' ', $out[count($out) - 1])[0])) {

                            $id = explode(' ', $out[count($out) - 1])[0];

                            if(exec("/usr/local/bin/youtube-dl -o '$path' $download_video_url -f '$id'", $output)) {
                                if (strpos(end($output), '100%') !== false){
                                        return array(true,$path);
                                }
                            }
                        }elseif(isset(explode(' ', $out[count($out)])[0])){
                            $id = explode(' ', $out[count($out)])[0];

                            exec("/usr/local/bin/youtube-dl -o '$path' $download_video_url -f '$id'", $output);

                            if (strpos(end($output), '100%') !== false)

                                return array(true,$path);
                        }

                    }
                }

        if (is_dir($path))

                 return array(false,false);

            return array(false,false);
    }

    private  function getCountVideo($platform){

        if($this->debug)
            echo '[generate] : now in get Count Video in news ' . PHP_EOL;
        $cdb= new CDbCriteria();
        $cdb->condition = 'is_scheduled = 1 and `generated`="auto" and platform_id = '.$platform.' and DATE_FORMAT(schedule_date,"%Y-%m-%d") = "'.date('Y-m-d').'" and type = "Video"';

        $data = PostQueue::model()->count($cdb);

        if($this->debug)
            echo '[generate] : now in get Count Video in post queue is  '.$data . PHP_EOL;

        return $data;
    }

    private function get_video_in_news($id){



        $data = !empty(MediaNews::model()->findByAttributes(array('news_id'=>$id,'type'=>'video')))?true:false;

        if($this->debug)
            echo '[generate] : now in get Video in news is '.$data. PHP_EOL;

        return $data ;
    }

    private function get_media_news($news){


        $media_array = MediaNews::model()->findByAttributes(array('news_id'=>$news->id,'type'=>'video'));



        if(!empty($media_array->media) && $media_array->converted == 0){

            if($this->debug)
                echo '[generate] : now in get media news is valid check  converted is  : -> '.$media_array->converted.'  in news ' . PHP_EOL;

            /*Start Youtube*/

            if($rx = '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i');

            if (preg_match($rx, $media_array->media, $matches)) {

                if ($this->debug)
                    echo '[generate] : now in preg_match is  : true in news ' . PHP_EOL;

                list($valid, $path) = $this->Video_downloader($media_array->media);

                if ($this->Video_size_check_length($path)) {

                    if ($valid) {
                        $media_array->media = $this->upload_file($path,false);

                        $media_array->converted = 1;

                        if ($media_array->save()) {

                            return array($media_array, true, $path);

                        }
                    }
                }else{

                    echo '[generate] : Video is longer than half an hour' . PHP_EOL;

                    return array(false,false,false);
                }
            }

            /*End Youtube*/
        }elseif(!empty($media_array->media) && $media_array->converted == 1){

            if($this->debug)
                echo '[generate] : now in get media news is valid check  converted is  : -> '.$media_array->converted.'  in news ' . PHP_EOL;
            return array($media_array, true ,null);

        }


        return array(false,false,false);

    }

    private function get_platform($platform = 'Twitter'){

        $id = null;

        if(!empty(array_search($platform, $this->PlatFrom))){

            $id = $this->PlatFrom[array_search($platform, $this->PlatFrom)]->id;

        }else{
            $id = Platform::model()->findByAttributes(array('title'=>$platform));

            $id = !empty($id)?$id->id:null;

        }

        return $id;
    }

    public function get_media_for_twitter($news){
        $cdb = new CDbCriteria();

        $cdb->condition = 'news_id = '.$news->id.' and type != "video"';

        return  MediaNews::model()->find($cdb);
    }

    public function get_media_for_instagram($news){

        $cdb = new CDbCriteria();

        $cdb->condition = 'news_id = '.$news->id.' and type != "video" and type != "gallery"';

        return  MediaNews::model()->find($cdb);
    }

    protected function newsGenerator($news){

        $this->scheduled_counter++;

        $parent = null;

        $category = null;

        $sub_category = null;

        $valid_twitter= false;

        if(isset($news->category->title))
        $category = '#' . trim(str_replace(array('!', '"', ':', '.', '..', '...', ' '), '_', $news->category->title));


        $cdb = new CDbCriteria();

        $cdb->condition = 'news_id = '.$news->id.' and type != "video" and type !="gallery"';

        $data_media_original  = MediaNews::model()->find($cdb);

        $twitter_id = $this->get_platform();

        $facebook_id = $this->get_platform('Facebook');

        if($this->get_video_in_news($news->id)){

            if($this->debug)
                echo '[generate] : now in get video in news ' . PHP_EOL;

            $valid_twitter= true;

           $counter_news_twitter = $this->getCountVideo($twitter_id);

             if( $counter_news_twitter < 5 ){

                 if($this->debug)
                     echo '[generate] : now in $counter_news_twitter : -> '.$counter_news_twitter.' < 5 in news ' . PHP_EOL;

                list($data_media , $valid , $path ) = $this->get_media_news($news);

                 if($valid){

                     if($this->debug)
                         echo '[generate] : now in get media news is valid  : -> '.$valid.'  in news ' . PHP_EOL;


                     if($this->video_size($path)){

                         if($this->debug)
                             echo '[generate] : now in video size is valid  : -> '.$valid.'  in news ' . PHP_EOL;


                         $valid_twitter = false;

                         $data_media_original = $data_media;

                     }
                 }else{
                     if($this->debug)
                         echo '[generate] : now in get media news is valid  : -> '.$valid.'  in news ' .PHP_EOL;
                 }


             }


            $counter_news_facebook = $this->getCountVideo($facebook_id);

            if( $counter_news_facebook < 5 ){

                if($this->debug)
                    echo '[generate] : now in $counter_news_facebook : -> '.$counter_news_facebook.' < 5 in news ' . PHP_EOL;

                list($data_media , $valid , $path ) = $this->get_media_news($news);

                if($valid)
                        $data_media_original = $data_media;

            }


            if(isset($path)){

                if($this->debug)
                    echo '[generate] : Delete file from server {{ '.$path.' }}' . PHP_EOL;
                $this->delete_file_from_server($path);
            }
        }


        if(empty($data_media_original)){

            $cdb = new CDbCriteria();

            $cdb->condition = 'news_id = '.$news->id.' and type != "video"';

            $data_media_original =  MediaNews::model()->find($cdb);
        }


        $new_shorten = $news->shorten_url;

        foreach ($this->PlatFrom as $item) {


            if(!empty($news->link)){

                switch ($item->title){
                    case 'Facebook':
                        $new_shorten = $news->link.'?utm_source=facebook&utm_medium=referral&utm_campaign=sortechs';
                        break;
                    case 'Twitter':
                        $new_shorten =$news->link.'?utm_source=twitter&utm_medium=referral&utm_campaign=sortechs';
                        break;

                    case 'Instagram':
                        $new_shorten =$news->link.'?utm_source=instagram&utm_medium=referral&utm_campaign=sortechs';
                        break;
                }

            }


            if(!empty($new_shorten)){
                $shortUrl = new ShortUrl();
                $new_shorten = $shortUrl->short(urldecode($new_shorten));
            }

            if($item->title == "Twitter" && $valid_twitter)
                $data_media_original = $this->get_media_for_twitter($news);

            if($item->title == "Instagram") {
                $data_media_original = $this->get_media_for_instagram($news);
            }

            $temp = $this->get_template($item, $news->category_id, isset($data_media_original->type)?$data_media_original->type:"Image");


            $title = str_ireplace($this->hash_tag ,$this->trim_hash, trim($news->title));

            $description = str_ireplace($this->hash_tag, $this->trim_hash, trim($news->description));


            $shorten_url = $news->shorten_url;

            $full_creator = $news->creator;

            $text = str_replace(
                array('[title]', '[description]', '[short_link]', '[author]'),
                array($title, $description, $shorten_url, $full_creator),
                $temp['text']
            );

            preg_match_all("/#([^\s]+)/", $text, $matches);

            if(isset($matches[0])){
                $lastPos = 0;
                $positions = array();
                $counter = 0;
                $finds = [];
                foreach ($matches[0] as $match) {
                    while (($lastPos = mb_strpos($text, $match, $lastPos,'utf-8'))!== false) {
                        $positions[$counter]['pos'] = $lastPos;
                        $positions[$counter]['word'] = $match;
                        $positions[$counter]['count'] = mb_strlen($match,'utf-8');
                        $lastPos = $lastPos + mb_strlen($match,'utf-8');
                        $counter++;
                    }
                }

                foreach ($positions as $value) {
                    $word =  mb_substr($text,$value['pos'],$value['count'],'utf-8');
                    if(($this->get_word($finds,trim($word))))
                        $finds[]=trim($word);
                    else
                        $text =  $this->mb_substr_replace($text, str_replace('#','',$word), $value['pos'], $value['count'],'utf-8');
                }
            }

            if ($item->title == 'Facebook' or $item->title == 'Twitter') {

                $text = str_replace('# ', '#', $text);

                $found = true;

                preg_match_all("/#([^\s]+)/", $text, $matches);

                if (!isset($matches[0])) {

                    $matches[0] = array_reverse($matches[0]);
                    $count = 0;
                    foreach ($matches[0] as $hashtag) {


                        if(strpos($text,'[section]')) {
                            if ($count >= 1) {

                                    $text = str_replace($hashtag, str_replace('_', ' ', str_replace('#', '', $hashtag)), $text);
                                    break;


                            }
                        }else{
                            if ($count >= 2) {
                                $found = false;

                                    $text = str_replace($hashtag, str_replace('_', ' ', str_replace('#', '', $hashtag)), $text);

                            }
                        }
                        $count++;
                    }
                    $found =true;
                    if ($count >= 2)
                        $found = false;
                }


                if ($found)
                    $text = str_replace(array('[section]', '[sub_section]'), array($category, $sub_category), $text);
                  else
                    $text = str_replace(array('[section]', '[sub_section]', '|'), array('', '', '',), $text);

            } elseif ($item->title == 'Instagram') {
                $text = str_replace(array('[section]', '[sub_section]', '|'), array('', '', ''), str_replace('# ', '#', $text));

            }


            list($time,$is_scheduled ,$obj) = $this->date($news->category_id,$item->id);

            if($this->debug)
                echo '[Generator] : Now generate '.$item->title.' , Time : '.$time.PHP_EOL;


            if ($item->title == 'Twitter') {

                $text_twitter = $text;
                if ($temp['type'] == 'Preview')
                    if (!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!', $text, $matches))
                        $text = $text . PHP_EOL . $news->shorten_url;


                if ($this->getTweetLength($text_twitter, $temp['type'] == 'Image' ? true : false, $temp['type'] == 'Video' ? true : false) > 141) {
                    $is_scheduled = 0;
                } else {
                    $text = $text_twitter . PHP_EOL;
                    if ($temp['type'] == 'Preview')
                        if (!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!', $text, $matches))
                            $text .= PHP_EOL . $news->shorten_url;
                }
            }

            if ($this->scheduled_counter > $this->per_post_day) {

                if($this->debug)
                    echo '[Generator] : news today '.$this->scheduled_counter.' , { Time }'.$this->per_post_day.PHP_EOL;

                $is_scheduled = 0;
            }

            if ($item->title != 'Instagram') {


                $text = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $text);
                $text = preg_replace('/\n(\s*\n){2,}/', "\n\n", $text);


                    if($this->image_instagram_scheduled){

                        if($this->debug)
                            echo '[Generator] : is scheduled after instagram : '.$is_scheduled.PHP_EOL;

                        $this->image_instagram_scheduled = true;

                    }

            }


            if($this->debug)
                    echo '[Generator] : is scheduled  : '.$item->title.' '.$is_scheduled.PHP_EOL;




            /*if((trim($news->category->title) == trim('رياضة') and $news->category->page_index == 'main'))
                if($this->count_sports($news->category->id) >8)
                    $is_scheduled=0;*/

            if($item->title == 'Instagram'){
                foreach ($this->array_match_pics as $array_matches){
                    if (preg_match('/\b' . $array_matches . '\b/u', '#' . $text, $matches)) {
                        $is_scheduled=0;
                    }
                }
            }

            $PostQueue = new PostQueue();
            $PostQueue->setIsNewRecord(true);
            $PostQueue->id = null;
            $PostQueue->command = false;
            $PostQueue->type = $temp['type'];
            $PostQueue->post = trim($text);
            $PostQueue->schedule_date = $time;
            $PostQueue->catgory_id = $news->category_id;
            $PostQueue->template_id = $temp['id'];
            $PostQueue->main_category_id = $obj ? $obj :null;
            $PostQueue->link = $new_shorten;
            $PostQueue->is_posted = 0;
            $PostQueue->news_id = $news->id;
            $PostQueue->post_id = null;
            $PostQueue->media_url = isset($data_media_original->media)?$data_media_original->media:$this->general_image;
            $PostQueue->settings = $this->is_custom_settings ? 'custom' : 'general';
            $PostQueue->is_scheduled = $is_scheduled;
            $PostQueue->platform_id = $item->id;
            $PostQueue->generated = 'auto';
            $PostQueue->created_at = date('Y-m-d H:i:s');

            if ($parent == null) {

                $PostQueue->parent_id = null;

                if ($PostQueue->save())
                    $parent = $PostQueue->id;
                else
                    $this->send_email('Error on GeneratorsCommand.php ', 'error on post queue save  <a href="' . Yii::app()->params['domain'] . '/postQueue/' . $PostQueue->id . '" target="_blank" style="border:1px solid black;color:white;background-color:darkred">'.$news->link.'</a>');

            } else {
                $PostQueue->parent_id = $parent;
                if (!$PostQueue->save())
                    $this->send_email('Error on GeneratorsCommand.php ', 'error on post queue save  <a href="' . Yii::app()->params['domain'] . '/postQueue/' . $PostQueue->id . '" target="_blank" style="border:1px solid black;color:white;background-color:darkred">'.$news->link.'</a>');
            }


        }

        $news->generated = 1;
        if (!$news->update())
            $this->send_email('Error on GeneratorsCommand.php ', 'error on post News save  <a href="' . Yii::app()->params['domain'] . '/news/' . $news->id . '">'.$news->link.'</a>');


    }

    private function video_size($file){

        Yii::import('application.extensions.getid3.getID3');

        $getID3 = new getID3;

        $file = $getID3->analyze($file);

        if(isset($file['filesize']) && isset($file['playtime_string'])){

            if($file['filesize']<536870912){

                $time = explode(':',$file['playtime_string']);

                if(isset($time[0]) && isset($time[1])){

                    $time[0] = $time[0] * 60;

                    $time = $time[0] + $time[1];

                    if($time<=140){

                        return true;

                    }
                }

            }
        }

        return false;
    }

    private function get_template($platform, $category,$type = null){

        $type = ucfirst($type);

        if($this->debug)
            echo '[Generator] : Type get Template '.$type.PHP_EOL;

        if($type == 'Video') {
            $temp = PostTemplate::model()->findByAttributes(array(
                'platform_id' => $platform->id,
                'catgory_id' => $category,
                'type' => $type,
            ));

            if (!empty($temp)) {

                if($this->debug)
                echo '[generate] : \'platform_id\' => $platform->id,
            \'catgory_id\' => $category,
            \'type\' => $type,' . PHP_EOL . PHP_EOL . PHP_EOL;

                return $temp;
            }

            $cond = new CDbCriteria();
            $cond->condition = '(  platform_id = ' . $platform->id . ' or platform_id is NUll  ) and type="' . $type . '"';
            $temp = PostTemplate::model()->find($cond);

            if (!empty($temp)) {
                if($this->debug)
                echo '[generate] : $cond->condition = \'(  platform_id = \' . $platform->id . \' or platform_id is NUll  ) and type="\'.$type.\'"\';' . PHP_EOL . PHP_EOL . PHP_EOL;
                return $temp;
            }


            $temp = PostTemplate::model()->findByAttributes(array(
                'platform_id' => $platform->id,
                'catgory_id' => $category,
            ));

            if (!empty($temp)) {
                if($this->debug)
                echo '[generate] :  \'platform_id\' => $platform->id,\'catgory_id\' => $category,' . PHP_EOL . PHP_EOL . PHP_EOL;
                return $temp;
            }

            $cond = new CDbCriteria();
            $cond->order = 'RAND()';
            $cond->condition = '( ( platform_id = ' . $platform->id . ' or platform_id is NUll ) and  catgory_id is NULL ) and  type="' . $type . '"';
            $temp = PostTemplate::model()->find($cond);


            if (!empty($temp)) {
                if($this->debug)
                echo '[generate] : $cond->condition = \'( ( platform_id = \' . $platform->id . \' or platform_id is NUll ) and  catgory_id is NULL ) and  type="\'.$type.\'"\';' . PHP_EOL . PHP_EOL . PHP_EOL;
                return $temp;
            }
        }else {


            $cond = new CDbCriteria();
            $cond->order = 'RAND()';
            $cond->condition = '( ( platform_id = ' . $platform->id . ' or platform_id is NUll ) and  catgory_id is NULL ) and type !="Video"';
            $temp = PostTemplate::model()->find($cond);


            if (!empty($temp)) {
                if($this->debug)
                echo '[generate] :$cond->condition = \'( ( platform_id = \' . $platform->id . \' or platform_id is NUll ) and  catgory_id is NULL ) and type !="Video" \';' . PHP_EOL . PHP_EOL . PHP_EOL;
                return $temp;
            }

            $cond = new CDbCriteria();
            $cond->order = 'RAND()';
            $cond->condition = '( ( platform_id = ' . $platform->id . ' or platform_id is NUll ) and  catgory_id is NULL )';
            $temp = PostTemplate::model()->find($cond);


            if (!empty($temp)) {
                if($this->debug)
                echo '[generate] : $cond->condition = \'( ( platform_id = \' . $platform->id . \' or platform_id is NUll ) and  catgory_id is NULL )\';' . PHP_EOL . PHP_EOL . PHP_EOL;
                return $temp;
            }
        }

        if ($platform->title == 'Instagram') {
            isset($temp->id) ? $temp->id : null;
        }

        return Yii::app()->params['templates'];

    }


}