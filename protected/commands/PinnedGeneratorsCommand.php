<?php
//Yii::import('application.commands.BaseCommand');
class PinnedGeneratorsCommand extends BaseCommand{

    private $id = null;

    private $id_fb = null;

    private $post = null;

    private $all_news;

    private $schedule_date;
    protected $details;
    protected $category;
    protected $model;
    protected $trim_hash;
    protected $hash_tag;
    protected $platform;
    protected $platform_id;
    protected $parent = null;
    protected $creator;
    protected $sub_category;
    protected $source;
    protected $news;
    protected $PostQueue;
    protected $post_id;
    private $template_id;

    public function run($args)
    {
        $this->TimeZone();
        $data = $this->News();
        $this->all_news = $this->getAllNews();

        $this->schedule_date = date('Y-m-d H:i:s');


        if(!empty($data)){
            foreach ($data as $value) {

                if(!empty($value->news_id)){

                    $this->check_and_update_news($value->news_id,$value->id,$value->platform_id,$value->template_id);

                    $value=PostQueue::model()->findByPk($value->id);

                }
            }
        }
    }

    public function clearTag($string){

        $text = str_ireplace('# #', '#', $string);
        $text = str_ireplace('##', '#', $text);
        $text = str_ireplace('&#8220;', '', $text);
        $text = str_ireplace('&#8221;', '', $text);
        $text = str_ireplace('&#8230;', '', $text);
        $text = str_ireplace('&#x2014;', '', $text);
        $text = str_ireplace('#8211;', '', $text);
        $text = str_ireplace('&#8211;', '', $text);
        $text = str_ireplace('&nbsp;', '', $text);
        $text = str_ireplace('&#160;', '', $text);
        $text = str_ireplace('. .', '', $text);
        $text = str_ireplace('&#xf2;,', '', $text);
        $text = str_ireplace('&#8211;', '', $text);
        $text = str_ireplace('2&#xb0;', '', $text);
        return str_ireplace('&ndash;', '', $text);
    }

    private function News(){

       return PostQueue::model()->model()->get_pinned_posts();
    }
    private function get_info_feature($link,$id)
    {

        $html = new SimpleHTMLDOM();
        $html_url = $html->file_get_html($link);
        $page_details = PageDetails::model()->findAllByAttributes(array('source_id'=>$id));
        $data = array();
        $data['body_description'] = "";
        foreach($page_details as $detail) {


            //title
            if (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'title') {
                foreach ($this->array_erem_words as $array_matches){
                    if (preg_match('/\b' . $array_matches . '\b/u',$html_url->find($detail->predication, 0)->content, $matches)) {
                        $data['title'] = str_replace($array_matches,'',$html_url->find($detail->predication, 0)->content);
                    }
                }
                if(!empty($data['title']))
                    $data['title'] = trim($this->clear_tags(trim($data['title'])));
                else
                    $data['title'] = trim($this->clear_tags(trim($html_url->find($detail->predication, 0)->content)));
            }
            elseif (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'title') {
                foreach ($this->array_erem_words as $array_matches){
                    if (preg_match('/\b' . $array_matches . '\b/u',$html_url->find($detail->predication, 0)->content, $matches)) {
                        $data['title'] = str_replace($array_matches,'',$html_url->find($detail->predication, 0)->content);
                    }
                }
                if(!empty($data['title']))
                    $data['title'] = trim($this->clear_tags(trim($data['title'])));
                else
                    $data['title'] = trim($this->clear_tags(trim($html_url->find($detail->predication, 0)->plaintext)));
            }


            //End title


            //Description
            if (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'description') {
                $data['description'] = $this->clear_tags(trim($html_url->find($detail->predication, 0)->plaintext));
            }
            elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'description') {
                $data['description'] = $this->clear_tags(trim($html_url->find($detail->predication, 0)->content));
            }


            //End Description
            if (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'creator') {
                $data['author'] = trim($this->clear_tags(trim($html_url->find($detail->predication, 0)->content)));

            }
            elseif (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'creator') {
                $data['author'] = trim($this->clear_tags(trim($html_url->find($detail->predication, 0)->plaintext)));
            }
            //End author
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'body_description') {
                $body_desc = $html_url->find($detail->predication);
                if (isset($body_desc)) {
                    foreach ($body_desc as $bodies) {
                        $data['body_description'] .= $this->clear_tags(trim($bodies->plaintext));
                    }
                }
            }elseif (!empty($html_url->find($detail->predication,0)->plaintext) and isset($html_url->find($detail->predication,0)->plaintext) and $detail->pageType->title == 'body_description') {
                $data['body_description'] = $this->clear_tags(trim($html_url->find($detail->predication,0)->plaintext));
            }

            //image
            if (isset($html_url->find($detail->predication, 0)->src) and !empty($html_url->find($detail->predication, 0)->src) and $detail->pageType->title == 'image') {
                if (strpos($html_url->find($detail->predication, 0)->src, 'http') === 0) {
                    $data['image']['src'] = $html_url->find($detail->predication, 0)->src;

                }else
                    $data['image']['src'] = Yii::app()->params['feedUrl'].$html_url->find($detail->predication, 0)->src;
            } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'image') {
                if (strpos($html_url->find($detail->predication, 0)->content, 'http') === 0)
                    $data['image']['src'] = $html_url->find($detail->predication, 0)->content;
                else
                    $data['image']['src'] = Yii::app()->params['feedUrl'].$html_url->find($detail->predication, 0)->content;
                $data['image']['type']='image';
            }
            //end image


            //gallery
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'gallery') {
                $gall = $html_url->find($detail->predication);
                foreach ($gall as $item) {
                    if (strpos($html_url->find($detail->predication, 0)->src, 'http') === 0) {
                        if($data['image']['src'] != $item->src)
                            $data['gallary']['type']='gallery';
                        $data['gallary']['src'][] =$item->src;
                    }else {
                        if ($data['image']['src'] != Yii::app()->params['feedUrl']. $item->src)
                            $data['gallary']['type']='gallery';
                        $data['gallary']['src'][] = Yii::app()->params['feedUrl'].$item->src;
                    }
                }

            }
            //End gallery



            //Tags
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'tags') {
                $tags_counter = $html_url->find($detail->predication);
                foreach ($tags_counter as $tag) {
                    $tag_space = trim($tag->plaintext);
                    $tag_space = preg_replace("/[\s_]/", "_", $tag_space);
                    /*$body_description =preg_replace('/\b'.$tag->plaintext.'\b/u', '#'.$tag_space, $body_description);
                    $description =preg_replace('/\b'.$tag->plaintext.'\b/u', '#'.$tag_space, $description);*/

                    $data['tags'][] = $this->clear_tags($tag_space);
                }

            }
            //Tags
        }



        return $data;
    }
    protected function check_and_update_news($id,$post_id,$platform_id,$template_id){
        Yii::import('application.modules.features.models.*');

        if(!isset($this->all_news[$id])){
            return;
        }


        $this->platform_id=$platform_id;

        $this->post_id=$post_id;

        $this->template_id = $template_id;

        $this->news = clone $this->all_news[$id];


        $this->details=$this->get_info_feature($this->news->link,$this->news->category_id);
       if($this->news->title != $this->details['title'] || $this->news->description != $this->details['description']) {
           $this->category = Category::model()->findByPk($this->news->category_id);

           $this->news->title = $this->details['title'];

           $this->news->description = isset($this->details['description']) ? $this->details['description'] : null;
           $this->news->creator = isset($this->details['author']) ? $this->details['author'] : null;
           $this->news->schedule_date = $this->schedule_date;
           $this->news->setIsNewRecord(true);
           if ($this->news->save(false)) {

               if (isset($this->details['media']['gallery']))
                   $this->getMediaNews($this->news->id, $this->details['media']['gallery']);

               $this->getMediaNews($this->news->id,$this->details['media']['image']);
               $this->generate_post();
           }
       }


    }
    protected function getMediaNews($id, $data)
    {

        $criteria = new CDbCriteria;
        $criteria->compare('news_id',$id);
        MediaNews::model()->deleteAll($criteria);
       
        $media = new MediaNews();
        if($data['type'] == 'image'){


            $media->command=false;
            $media->id=null;
            $media->news_id = $id;
            $media->media = $data['src'];
            $media->type=$data['type'];
            $media->setIsNewRecord(true);
            if(!$media->save()){

            }


        }elseif($data['type'] == 'gallery'){


            foreach ($data['src'] as $item) {
                $empty_media = MediaNews::model()->findByAttributes(array('news_id'=> $id,'media'=>$item));
                if(empty($empty_media)){
                    $media->id=null;
                    $media->news_id = $id;
                    $media->media = $item;
                    $media->type=$data['type'];
                    $media->setIsNewRecord(true);
                    if(!$media->save()){

                    }
                }

            }
        }

    }

    private  function generate_post(){


        $d = Hashtag::model()->findAll('deleted = 0');

        $this->trim_hash = array();

        $this->hash_tag = array();
        $this->get_new_tags($this->category->title,$this->details['tags']);
        /* foreach ($d as $index=>$item) {
             $this->trim_hash[$index]=" #".str_replace(" ", "_",trim($item->title)).' ';
             $this->hash_tag[$index]=' '.trim($item->title).' ';
         }*/

        $this->parent = null;



        if(isset($this->category->title))
            $this->category->title = '#'.trim(str_replace(array('!','"',':','.','..','...',' '), '_',$this->category->title));

        $this->source = Yii::app()->params['source'].' : ';

        $this->creator = false;


        $this->platform = Platform::model()->findByPk($this->platform_id);
        $this->post();

        $this->news->generated = 1;

        $this->news->save();
    }

    private function get_template($platform,$category){

        $temp  = PostTemplate::model()->findByAttributes(array(
            'platform_id'=>$platform->id,
            'catgory_id'=>$category,

        ),'type != "video"');
        if(!empty($temp))
            return $temp;
        $cond = new CDbCriteria();
        $cond->order = 'RAND()';
        $cond->condition='( ( platform_id = '.$platform->id.' or platform_id is NUll ) and  catgory_id is NULL and type !="Video")';
        $temp  = PostTemplate::model()->find($cond);

        if(!empty($temp))
            return $temp;

        return Yii::app()->params['templates'];
    }

    protected function post(){

        $is_scheduled =1;

        $twitter_is_scheduled =false;

        $media= null;

        //$temp = $this->get_template($this->platform,$this->news->category_id);
        $temp = PostTemplate::model()->findByPk($this->template_id);

        if(empty($temp)){
            $platform = Platform::model()->findByAttributes(array('title'=>$this->platform_name,'deleted'=>0));
            $temp =  PostTemplate::model()->findByAttributes(array('platform_id'=>$platform->id,'type'=>$this->type_post));
        }


        $title =trim($this->news->title);

        $description = trim($this->news->description);

        //if ($item->title != 'Instagram') {
        $title = str_replace($this->hash_tag ,$this->trim_hash, $title);
        $description = str_replace($this->hash_tag, $this->trim_hash, $description);


        //if($this->platform->title != 'Instagram'){
        $title = str_replace($this->hash_tag, $this->trim_hash, trim($this->news->title));

        $description = str_ireplace($this->hash_tag, $this->trim_hash, trim($this->news->description));

        // $description = str_replace($this->hash_tag, $this->trim_hash, $this->news->description);

        //}

        $title =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $title);
        $title = preg_replace('/\n(\s*\n){2,}/', "\n\n", $title);

        $description =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $description);
        $description = preg_replace('/\n(\s*\n){2,}/', "\n\n", $description);

        $shorten_url = $this->news->shorten_url;
        $full_creator = $this->source.$this->news->creator;
        if(!empty(strpos($this->news->creator,':')))
            $full_creator = $this->news->creator;

        $cre= $this->platform->title == 'Twitter'?($this->creator?$full_creator:null):$full_creator;


        $text = str_replace(array('[title]','[description]','[short_link]','[author]',),array($title,$description,$shorten_url,$cre),$temp['text']
        );
        preg_match_all("/#([^\s]+)/", $text, $matches);

        if(isset($matches[0])){
            $lastPos = 0;
            $positions = array();
            $counter = 0;
            $finds = [];
            foreach ($matches[0] as $match) {
                while (($lastPos = mb_strpos($text, $match, $lastPos,'utf-8'))!== false) {
                    $positions[$counter]['pos'] = $lastPos;
                    $positions[$counter]['word'] = $match;
                    $positions[$counter]['count'] = mb_strlen($match,'utf-8');
                    $lastPos = $lastPos + mb_strlen($match,'utf-8');
                    $counter++;
                }
            }

            foreach ($positions as $value) {
                $word =  mb_substr($text,$value['pos'],$value['count'],'utf-8');
                if(($this->get_word($finds,trim($word))))
                    $finds[]=trim($word);
                else
                    $text =  $this->mb_substr_replace($text, str_replace('#','',$word), $value['pos'], $value['count'],'utf-8');
            }
        }

        if ($this->platform->title == 'Facebook' or $this->platform->title == 'Twitter') {

            $text = str_replace('# ', '#', $text);

            $found = true;

            preg_match_all("/#([^\s]+)/", $text, $matches);

            if (isset($matches[0])) {

                $matches[0] = array_reverse($matches[0]);
                $count = 0;
                foreach ($matches[0] as $hashtag) {


                    if(strpos($text,'[section]')) {
                        if ($count >= 1) {
                            $text = str_replace($hashtag, str_replace('_', ' ', str_replace('#', '', $hashtag)), $text);
                            break;

                        }
                    }else{
                        if ($count >= 2) {
                            $found = false;
                            $text = str_replace($hashtag, str_replace('_', ' ', str_replace('#', '', $hashtag)), $text);
                        }
                    }
                    $count++;
                }
                $found =true;
                if ($count >= 2)
                    $found = false;
            }



            if ($found)
                $text = str_replace(array('[section]', '[sub_section]'), array($this->category->title), $text);
            else
                $text = str_replace(array('[section]', '[sub_section]', '|'), array('', '', '',), $text);

        } elseif ($this->platform->title == 'Instagram') {
            $text = str_replace(array('[section]', '[sub_section]', '|'), array('', '', ''), str_replace('# ', '#', $text));

        }


        if(!empty($this->model->image)){
            $newsMedia = MediaNews::model()->findByAttributes(array('media'=>$this->model->image));
            if(!empty($newsMedia)){
                $media = $newsMedia->media;
            }
        }else{
            $newsMedia = MediaNews::model()->findAll('(type = "image" or type = "gallery") and news_id = '.$this->news->id);
            if(!empty($newsMedia)){
                $index_media= rand(0,count($newsMedia)-1);
                if(isset($newsMedia[$index_media])){
                    $media = $newsMedia[$index_media]->media;
                }
            }
        }
        if($this->platform->title == 'Twitter'){

            $newsMedia = MediaNews::model()->find('(type = "image") and news_id = '.$this->news->id);
            if(!empty($newsMedia)){
                $media = $newsMedia->media;
            }

            $text_twitter =$text;

            if($temp['type'] == 'Preview')
                if(!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!',$text,$matches))
                    $text =$text.PHP_EOL.$this->news->shorten_url;

            if($this->getTweetLength($text_twitter,$temp['type'] == 'Image'?true:false,$temp['type'] == 'Video'?true:false) > 141) {
                $is_scheduled = 0;
                $twitter_is_scheduled = true;
            }else{
                $twitter_is_scheduled =false;
                $text = $text_twitter.PHP_EOL;
                if($temp['type'] == 'Preview')
                    if(!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!',$text,$matches))
                        $text .=PHP_EOL.$this->news->shorten_url;
            }

        }else{
            if($twitter_is_scheduled){
                if(!$is_scheduled){
                    $is_scheduled = 1;
                }
            }
        }
        $text = str_replace('# #', '#', $text);
        $text = str_replace('##', '#', $text);
        $text = str_replace('&#8220;', '', $text);
        $text = str_replace('&#8221;', '', $text);
        $text = str_replace('&#8230;', '', $text);
        $text = str_replace('#8211;', '', $text);
        $text = str_replace('&#8211;', '', $text);
        $text = str_replace('&nbsp;', '', $text);
        $text = str_replace('&#160;', '', $text);
        $text = str_replace('&#8211;', '', $text);
        $text = str_replace('&ndash;', '', $text);
        $text = str_replace('&#8230;', '', $text);
        //if($this->platform->title != 'Instagram'){
        $text =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $text);
        $text = preg_replace('/\n(\s*\n){2,}/', "\n\n", $text);

        $this->PostQueue = new PostQueue;
        $this->PostQueue->setIsNewRecord(true);
        $this->PostQueue->id= null;
        $this->PostQueue->type = $temp['type'];
        $this->PostQueue->post = $text;
        $this->PostQueue->schedule_date = $this->schedule_date;
        $this->PostQueue->catgory_id =  $this->news->category_id;
        $this->PostQueue->main_category_id =  $this->news->category_id;
        $this->PostQueue->link = $this->news->shorten_url;
        $this->PostQueue->is_posted = 0;
        $this->PostQueue->news_id = $this->news->id;
        $this->PostQueue->post_id =null;
        $this->PostQueue->media_url =$media;
        $this->PostQueue->settings ='general';
        $this->PostQueue->is_scheduled =$is_scheduled;
        $this->PostQueue->platform_id =$this->platform->id;
        $this->PostQueue->generated ='pinned';
        $this->PostQueue->created_at =date('Y-m-d H:i:s');
        $this->PostQueue->parent_id =null;
        $this->PostQueue->save();

    }


}