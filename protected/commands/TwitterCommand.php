<?php
class TwitterCommand extends BaseCommand{

    private $post = null;
    private $id = null;
    private $error = null;

    public function run($args){
        $this->TimeZone();
        $value =$this->News();


        if(!empty($value)){

            if(!empty($value->news_id)){
                $this->platform_name ='Twitter';
                $this->type_post=$value->type;
                $this->check_and_update_news($value->news_id,$value->id,$value->platform_id,$value->template_id);

                $value=PostQueue::model()->findByPk($value->id);

            }
            //catch the post before posting processing...
            $value->is_posted=3;
            $value->save(false);

            

                $this->id = $value->id;
                $valid = false;
                $id = null;
                switch ($value->type) {
                    case 'Image':
                        $id =$this->image($value);
                        if($id)
                            $valid = true;
                        break;
                    case 'Video':
                        $this->post = $value;
                        $id =$this->video($value);
                        if($id){
                            $valid = true;
                        }
                        break;
                    case 'Text':
                        $id =$this->text($value);
                        if($id)
                            $valid = true;
                        break;
                    case 'Preview':
                        $id =$this->preview($value);
                        if($id)
                            $valid = true;
                        break;
                }
                if($valid){
                    $value->is_posted = 1;
                    $value->post_id = $id;
                    $value->command = false;
                    $value->save();
                }else{
                    $value->errors = $this->error;
                    $value->is_posted = 2;
                    $value->command = false;


                    $value->save();
                }
            }

        die;

    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @return PostQueue the News
     * @var $data PostQueue
     * @var $value PostQueue
     * @var $db PostQueue
     */

    private function News()
    {

        return PostQueue::model()->get_post_twitter();

    }


    private function video($post){


        $is_file =false;
        $page='main';
        if(!empty($post->catgory_id))
            if(isset($post->catgory->page_index))
                $page = $post->catgory->page_index;
        $obj = $this->Obj_twitter($page);

        if(empty($post->media_url)){

            $this->error = 'media empty';



            return false;
        }

        $video = Yii::app()->params['webroot'].'/image/twitter_video.mp4';
        $new_path = Yii::app()->params['webroot'] . '/image/video_converted' .time() .'.mp4';


        if(@file_get_contents(urldecode($post->media_url), 'r')) {

            file_put_contents($video, $this->file_get_contents_curl(urldecode($post->media_url)));
            echo "putted content";
        }
        else{

            $this->error = 'failed to open stream: HTTP request failed! HTTP/1.1 404 Not Found';


            return false;
        }
        if (exec("/usr/bin/ffmpeg -i '$video' -vcodec h264 -acodec aac -strict -2 -s 1280x720 -f mp4  $new_path 2>&1", $outputs, $v)) {
        echo '[generate] : now in convert  video if ' . PHP_EOL;
            $is_file=true;
            $video = $new_path;
        }else{
            echo "could not convert";
        }
        sleep(1);

        if(!$this->Size_video($video)){

            $this->error = 'video large';

            return false;

        }
        echo "size Video";
        $size_bytes = filesize($video);

        $fp = fopen($video, 'r');

        $reply = $obj->media_upload([
            'command' => 'INIT',
            'media_category'=>'tweet_video', //
            'media_type' => 'video/mp4',
            'total_bytes' => $size_bytes

        ]);


        if(!isset($reply->media_id_string)){

            $this->error = 'media video ';


            return false;

        }

        $media_id = $reply->media_id_string;


        $segment_id = 0;

        while (!feof($fp)) {
            $chunk = fread($fp, 3048576);

            $obj->media_upload([
                'command' => 'APPEND',
                'media_id' => $media_id,
                'segment_index' => $segment_id,
                'media' => $chunk
            ]);

            $segment_id++;


        }

        fclose($fp);


        $reply = $obj->media_upload([
            'command' => 'FINALIZE',
            'media_id' => $media_id
        ]);

        $uploaded_video  = false;


        if(isset($reply->processing_info->state) && $reply->processing_info->state == 'pending' && isset($reply->processing_info->check_after_secs)){

            $state = $reply->processing_info->state;

            $check_after_secs= $reply->processing_info->check_after_secs;


            $while = true;

            while ($while){

                sleep($check_after_secs);

                $reply = $obj->media_upload([
                    'command' => 'STATUS',
                    'media_id' => $media_id,
                ]);

                if(isset($reply->processing_info->state) && $reply->processing_info->state == 'succeeded'){
                    $while = false;
                    $uploaded_video = true;

                }

                if(isset($reply->processing_info->state) && $reply->processing_info->state == 'failed'){
                    $this->error = $reply->processing_info->error->message;
                    $while = false;

                }

            }

        }

        if($uploaded_video){

            $reply = $obj->statuses_update([
                'status' => $post->post,
                'media_ids' => $media_id
            ]);


            if(isset($reply->id_str)){

                $this->id =  $reply->id_str;
                if($is_file)
                unlink($new_path);
                return true;
            }
            elseif(isset($reply->errors[key($reply->errors)]->message)){
                $this->error = $reply->errors[key($reply->errors)]->message;


                return false;
            }else{

                return false;
            }

        }else{

            return false;
        }

    }

    private function image($value){
        $params = array(
            'status' => $value->post,
        );

        $page = 'main';
        if(!empty($value->catgory_id))
            if(isset($value->catgory->page_index))
                $page = $value->catgory->page_index;

        $obj = $this->Obj_twitter($page);
        $valid = true;

        if(!empty($value->media_url)){
            $image = Yii::app()->params['webroot'].'/image/twitter_'.time().'.jpg';

            if($this->file_get_contents_curl(urldecode($value->media_url))){
                file_put_contents($image,$this->file_get_contents_curl(urldecode($value->media_url)));
            }else{
                $this->error = 'failed to open stream: HTTP request failed! HTTP/1.1 404 Not Found';
                $valid= false;
            }

            if($valid){
                $params['media[]']= $image;
                $reply = $obj->statuses_updateWithMedia($params);
            }

        }else{
            $reply = $obj->statuses_update($params);
        }

        if($valid){
            if(!empty($reply)){
                $go = $reply->httpstatus != 200 or isset($reply->errors);

                if(isset($reply->id_str)){
                    if(isset($image) && file_exists($image))
                        unlink($image);
                    return $reply->id_str;
                }


                if ($go) {
                    if(isset($image) && file_exists($image))
                        unlink($image);
                    $this->error = $reply->errors[0]->message;

                    return false;
                }
                if(isset($image) && file_exists($image))
                    unlink($image);
                return false;
            }else{
                if(isset($image) && file_exists($image))
                    unlink($image);
                return false;
            }
        }else{
            if(isset($image) && file_exists($image))
                unlink($image);
            echo  $this->error.PHP_EOL;
            return false;
        }
    }

    function file_get_contents_curl($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
        curl_setopt($ch, CURLOPT_URL, $url);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    private function text($value)
    {
        $params = array(
            'status' => $value->post,
        );

        $page = 'main';
        if(!empty($value->catgory_id))
            if(isset($value->catgory->page_index))
                $page = $value->catgory->page_index;

        $obj = $this->Obj_twitter($page);
        $reply = $obj->statuses_update($params);

        $go = $reply->httpstatus != 200 or isset($reply->errors);

        if(isset($reply->id_str)){
            return $reply->id_str;
        }


        if ($go) {

            $this->error = $reply->errors[0]->message;

            return false;
        }

        return false;

    }

    private function preview($value)
    {


        if(preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!',$value->post,$matches))
        {
            $params = array('status' => $value->post);
        }else{
            $params = array('status' => $value->post.PHP_EOL.$value->link);
        }
        $page = 'main';
        if(!empty($value->catgory_id))
            if(isset($value->catgory->page_index))
                $page = $value->catgory->page_index;

        $obj = $this->Obj_twitter($page);


        $reply = $obj->statuses_update($params);

        $go = $reply->httpstatus != 200 or isset($reply->errors);


        if(isset($reply->id_str)){
            return $reply->id_str;
        }


        if ($go) {

            $this->error = $reply->errors[0]->message;

            return false;
        }

        return false;

    }

}