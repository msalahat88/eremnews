<?php
class FillerCommand extends BaseCommand{




    public function run($args){
        $this->TimeZone();
        $criteria=new CDbCriteria;
        $criteria->condition = "DATE_FORMAT(start_date,'%Y-%m-%d') <= '".date('Y-m-d')."' and DATE_FORMAT(end_date,'%Y-%m-%d') >= '".date('Y-m-d')."' and DATE_FORMAT(last_generated,'%Y-%m-%d') != '".date('Y-m-d')."' ORDER BY RAND()";
        $filler = FillerData::model()->findAll($criteria);
        if(!empty($filler)){


            foreach ($filler as $item) {
                $filler_title = null;
                $item->catch = 1;
                $item->last_generated = date('Y-m-d');
                $item->command = false;
                $item->save();

                if($item->page_index == 'main'){
                    $filler_title = 'main_page_filler';
                }elseif($item->page_index == 'other'){
                    $filler_title = 'sport_page_filler';
                }

                $category = Category::model()->findByAttributes(array('title'=>$filler_title,'page_index'=>$item->page_index));
                if(empty($category)) {

                    $category = new Category();
                    $category->id = null;
                    $category->title =$filler_title;
                    $category->page_index = $item->page_index;
                    $category->type ='dom';
                    $category->lang ='ar';
                    $category->url='';
                    $category->created_at = date('Y-m-d H:i:s');
                    $category->setIsNewRecord(true);
                    if(!$category->save()){
                        echo "coludnot save";
                    };
                }

                $PostQueue = new PostQueue();
                $PostQueue->setIsNewRecord(true);
                $PostQueue->command= false;
                $PostQueue->id= null;
                $PostQueue->type = $item->type;
                $PostQueue->post = $item->text;
                $PostQueue->schedule_date = date('Y-m-d '.$item->publish_time);
                $PostQueue->catgory_id = $category->id;
                $PostQueue->media_url = $item->media_url;
                $PostQueue->link = $item->link;
                $PostQueue->is_posted = 0;
                $PostQueue->news_id =null;
                $PostQueue->post_id =null;
                $PostQueue->is_scheduled =1;
                $PostQueue->platform_id =$item->platform_id;
                $PostQueue->generated ='manual';
                $PostQueue->created_at =date('Y-m-d H:i:s');
                if(!$PostQueue->save())
                    $this->send_email($PostQueue,'error on filler data');
            }
        }else{
            $this->reset();
        }
    }

    private function reset(){
        $item = FillerData::model()->findAll('catch=1');
        foreach ($item as $value) {
            $value->catch = 0;
            $value->command = false;
            if(!$value->save())
                $this->send_email("Filler data",'error on filler data reset');
        }


    }
/*
    public function run($args){

        $this->TimeZone();
        $criteria=new CDbCriteria;
       // $criteria->limit = 1;
        $criteria->condition = "DATE_FORMAT(start_date,'%Y-%m-%d') <= '".date('Y-m-d')."' and DATE_FORMAT(end_date,'%Y-%m-%d') >= '".date('Y-m-d')."' and DATE_FORMAT(last_generated,'%Y-%m-%d') != '".date('Y-m-d')."' ORDER BY RAND()";
        $filler = FillerData::model()->findAll($criteria);
        $new_parent_id=null;
        $parent=true;
        $filler_title = 'filler';
        print_r($filler);
        if(!empty($filler)){
            foreach ($filler as $item) {

                $item->catch = 1;
                $item->last_generated = date('Y-m-d');
                $item->command = false;
                $item->save();
                if($item->page_index == 'main'){
                    $filler_title = 'main_page_filler';
                }elseif($item->page_index == 'other'){
                    $filler_title = 'sport_page_filler';
                }

                $category = Category::model()->findByAttributes(array('title'=>$filler_title,'page_index'=>$item->page_index));
                if(empty($category)) {

                    $category = new Category();
                    $category->id = null;
                    $category->title =$filler_title;
                    $category->page_index = $item->page_index;
                    $category->type ='dom';
                    $category->lang ='ar';
                    $category->url='';
                    $category->created_at = date('Y-m-d H:i:s');
                    $category->setIsNewRecord(true);
                    if(!$category->save()){
                        echo "coludnot save";
                    };
                }

                $PostQueue = new PostQueue();
                $PostQueue->setIsNewRecord(true);
                $PostQueue->command= false;
                $PostQueue->id= null;
                $PostQueue->type = $item->type;
                $PostQueue->post = $item->text;
                $PostQueue->schedule_date = date('Y-m-d '.$item->publish_time);
                $PostQueue->catgory_id = $category->id;
                $PostQueue->media_url = $item->media_url;
                $PostQueue->link = $item->link;
                $PostQueue->is_posted = 0;
                $PostQueue->news_id =null;
                $PostQueue->post_id =null;
                $PostQueue->is_scheduled =1;
                $PostQueue->platform_id =$item->platform_id;
                $PostQueue->generated ='manual';
                $PostQueue->parent_id=$new_parent_id;

                $PostQueue->created_at =date('Y-m-d H:i:s');
                if($PostQueue->save())
                {
                    if($parent){
                        $new_parent_id = $PostQueue->id;
                        $parent=false;
                    }
                }
                else
                    $this->send_email($PostQueue,'error on filler data');
            }
        }else{
            $this->reset();
        }
    }

    private function reset(){
        $item = FillerData::model()->findAll('catch=1');
        foreach ($item as $value) {
            $value->catch = 0;
            $value->command = false;
            if(!$value->save())
                $this->send_email($value,'error on filler data reset');
        }
    }*/

}