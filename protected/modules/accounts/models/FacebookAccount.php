<?php

/**
 * This is the model class for table "facebook_account".
 *
 * The followings are the available columns in table 'facebook_account':
 * @property integer $id
 * @property string $link_page
 * @property string $page_id
 * @property string $app_id
 * @property string $secret
 * @property string $token
 * @property string $created_at
 * @property string $updated_at
 * @property integer $is_general
 * @property integer $created_by
 * @property integer $updated_by
 */
class FacebookAccount extends BaseModels
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'facebook_account';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('link_page, page_id, app_id, secret, token', 'required'),
			array('is_general', 'numerical', 'integerOnly'=>true),
			array('link_page, page_id, app_id, secret', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, link_page, page_id, app_id, secret, token, is_general,updated_by,created_by,updated_at,created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'link_page' => 'Link Page',
			'page_id' => 'Page',
			'app_id' => 'App',
			'secret' => 'Secret',
			'token' => 'Token',
			'is_general' => 'Is General',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('link_page',$this->link_page,true);
		$criteria->compare('page_id',$this->page_id,true);
		$criteria->compare('app_id',$this->app_id,true);
		$criteria->compare('secret',$this->secret,true);
		$criteria->compare('token',$this->token,true);
		$criteria->compare('is_general',$this->is_general);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FacebookAccount the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
