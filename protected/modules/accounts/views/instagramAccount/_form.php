<?php
/* @var $this InstagramAccountController*/
/* @var $model InstagramAccount */
/* @var $form TbActiveForm */
?>
<style type="text/css">
	.form-horizontal .control-label {
		text-align: left;
	}
</style>
<div class="form" style="padding-top: 30px">

	<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
		'id'=>'instagram-account-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation'=>true,
		'type' => 'horizontal',
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
	));
	if(!$model->isNewRecord){
		$previously_defined_general = $model->is_general;

	}
	$field = 'col-sm-10';
	$label = 'col-sm-2';

	?>
	<div class="col-sm-12" >

		<?php echo $form->textFieldGroup($model,'link_page',array(
			'labelOptions' => array(
				'class' => 'col-sm-2',
			),
			'wrapperHtmlOptions' => array(
				'class' => 'col-sm-10',
			),
		)); ?>
		<?php echo $form->textFieldGroup($model,'username',array(
			'labelOptions' => array(
				'class' => 'col-sm-2',
			),
			'wrapperHtmlOptions' => array(
				'class' => 'col-sm-10',
			),
		)); ?>
		<?php echo $form->textFieldGroup($model,'password',array(
			'labelOptions' => array(
				'class' => 'col-sm-2',
			),
			'wrapperHtmlOptions' => array(
				'class' => 'col-sm-10',
			),
		)); ?>
		<?php
		$is_general = InstagramAccount::model()->findByAttributes(array('is_general'=>1));
		if(empty($is_general) and $model->isNewRecord){

			echo $form->dropDownListGroup($model,'is_general',array(
				'labelOptions' => array(
					'class' => 'col-sm-2',
				),
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-10',
				),
				'widgetOptions'=>array(
					'data'=>array(0=>'Custom',1=>'General'),
				),
			));}elseif(!$model->isNewRecord){
			echo $form->dropDownListGroup($model,'is_general',array(
				'labelOptions' => array(
					'class' => 'col-sm-2',
				),
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-10',
				),
				'widgetOptions'=>array(
					'data'=>array(0=>'Custom',1=>'General'),
				),
			));
		} ?>

	</div>


	<div class="form-actions  pull-right" style="margin-bottom: 20px;margin-right:20px;">
		<?php $this->widget(
			'booster.widgets.TbButton',
			array(
				'buttonType' => 'submit',
				'context' => 'primary',
				'label' => $model->isNewRecord ? 'Create' : 'Save',
			)
		); ?>

	</div>
	<?php $this->endWidget(); ?>

</div><!-- form -->