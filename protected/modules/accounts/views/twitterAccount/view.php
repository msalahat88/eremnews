<?php
/* @var $this TwitterAccountController */
/* @var $model TwitterAccount */

$this->breadcrumbs=array(
	'Twitter Accounts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TwitterAccount', 'url'=>array('index')),
	array('label'=>'Create TwitterAccount', 'url'=>array('create')),
	array('label'=>'Update TwitterAccount', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TwitterAccount', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TwitterAccount', 'url'=>array('admin')),
);
?>

<h1>View TwitterAccount #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'link_page',
		'twitter_key',
		'secret',
		'token',
		'is_general',
		'token_secret',
	),
)); ?>
