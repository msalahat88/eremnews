<?php
/* @var $this SectionPlatformAccountController */
/* @var $model SectionPlatformAccount */
/* @var $form TbActiveForm */

$this->pageTitle = "";

$this->breadcrumbs=array(
	'sections'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Category', 'url'=>array('index')),
	array('label'=>'Create Category', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('#form-visible').change(function(){
	$(this).submit();
	return true;
});
$('.search-form form').submit(function(){
	$('#category-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-sm-12 pull-right">
						<div class="col-sm-9"><?PHP
							$this->widget(
								'booster.widgets.TbButtonGroup',
								array(
									'size' => 'small',
									'context' => 'info',
									'buttons' => array(
										array(
											'label' => 'Create',
											'buttonType' =>'link',
											'url' => array('/accounts/sectionPlatformAccount/create')
										),
									),
								)
							);?></div>
						<div class="col-sm-3 pull-right">
							<?php echo Yii::app()->params['statement']['previousPage']; ?>
						</div>
					</div>
				</div>
				<div class="box-body">

					<?PHP //echo $this->renderPartial('_search',array('model'=>$model),true) ?>
					<?PHP

					$form=$this->beginWidget('booster.widgets.TbActiveForm', array(
						'action'=>Yii::app()->createUrl($this->route),
						'method'=>'get',
						'id'=>'form-visible',
					));
					?>
					<?php
					$this->endWidget();
					$this->widget('booster.widgets.TbGridView', array(
							'id'=>'section-platform-account-grid',
							'type' => 'striped bordered condensed',
							'template' => '{items}{pager}{summary}',
							'enablePagination' => true,
							'pager' => array(
								'class' => 'booster.widgets.TbPager',
								'nextPageLabel' => 'Next',
								'prevPageLabel' => 'Previous',
								'htmlOptions' => array(
									'class' => 'pull-right',
									'filterClass'=>'asdasdasd',
								),
							),
							'htmlOptions' => array(
								'class' => 'table-responsive'
							),
							'filter' => $model,
							'dataProvider' => $model->search(),
							'columns' => array(
								array(
									'name'=>'section_id',
									'type'=>'raw',
									'value' => '$data->category->title',
								),	array(
									'name'=>'platform_id',
									'type'=>'raw',
									'value' => '$data->platforms->title',
								),	array(
									'name'=>'account_id',
									'type'=>'raw',
									'value'=>function($data){
										if($data->platforms->title == 'Instagram'){
											$data_insta = InstagramAccount::model()->findByPk($data->account_id);
											$data = $data_insta->link_page;
											echo $data;
										}elseif($data->platforms->title == 'Facebook'){
											$data_insta = FacebookAccount::model()->findByPk($data->account_id);
											$data = $data_insta->link_page;
											echo $data;
										}elseif($data->platforms->title == 'Twitter'){
											$data_insta = TwitterAccount::model()->findByPk($data->account_id);
											$data = $data_insta->link_page;
											echo $data;
										}
									}
								),
								array(
									'class' => 'booster.widgets.TbButtonColumn',
									'header' => 'Options',
									//'template' => '{view}{update}{delete}{activate}{deactivate}',
									'template' => '{update}{delete}',
									'buttons' => array(
										'view' => array(
											'label' => 'View',
											'icon' => 'fa fa-eye',
										),
										'update' => array(
											'label' => 'Update',
											'icon' => 'fa fa-pencil-square-o',
										),
										'delete' => array(
											'label' => 'Delete',
											'icon' => 'fa fa-times',
										),
									)
								),
							))
					);?>
				</div>

			</div>
		</div>
	</div>
</section>