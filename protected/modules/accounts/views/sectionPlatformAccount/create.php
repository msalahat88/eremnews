<?php
/* @var $this SectionPlatformAccountController */
/* @var $model SectionPlatformAccount */
/* @var $model_platforms SectionPlatformAccount */

$this->pageTitle = "Sections and platforms | Create";

$this->breadcrumbs = array(
	'sections' => array('admin'),
	'Create',
);
$this->menu = array(
	array('label' => 'List Category', 'url' => array('index')),
	array('label' => 'Manage Category', 'url' => array('admin')),
);
?>
<script>
	$(document).ready(function(){
		$('#SectionPlatformAccount_platform_id').change(function(){
			$('#section-platform-account-form').submit();

		})
	})
</script>

<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-sm-12 pull-right">
						<?php echo Yii::app()->params['statement']['previousPage']; ?>
						<?PHP
						$this->widget(
							'booster.widgets.TbButtonGroup',
							array(
								'size' => 'small',
								'context' => 'info',
								'buttons' => array(
									array(
										'label' => 'Action',
										'items' => array(
											array('label' => 'Create', 'url' => array('create')),
											array('label' => 'Manage', 'url' => array('admin'))
										)
									),
								),
							)
						);
						?>

					</div>
				</div>
				<div class="box-body">
					<?php $this->renderPartial('_form', array('model' => $model,'model_platforms'=>$model_platforms)); ?>
				</div>
			</div>
		</div>
</section>