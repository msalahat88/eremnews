<?php
/* @var $this UserController */
/* @var $model User */

$this->pageTitle = "Users | View";

$this->breadcrumbs=array(
	'Management'=>array('/'.$this->module->id),
	'Users'=>array('index'),
	$model->name,



);
?>
<section class="content">
	<section class="content">
		<?PHP if(Yii::app()->user->hasFlash('create')){ ?>
			<div class="callout callout-success" style="margin-top:20px;">
				<h4>New user have been added successfully. </h4>
			</div>

		<?PHP } if(Yii::app()->user->hasFlash('update')){ ?>
			<div class="callout callout-info" style="margin-top:20px;">
				<h4>user updated successfully. </h4>
			</div>
		<?php } ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-sm-9"><?PHP
						$this->widget(
							'booster.widgets.TbButtonGroup',
							array(
								'size' => 'small',
								'context' => 'info',
								'buttons' => array(
									array(
										'label' => 'Action',
										'items' => array(
 											array('label' => 'Update', 'url'=>array('update', 'id'=>$model->id)),
											array('label' => 'Delete', 'url' => '#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
											array('label' => 'Manage', 'url'=>array('admin'))
										)
									),
									array('label' => 'Create','buttonType'=>'link', 'url'=>array('create'),								'context' => 'info',
										'context' => 'success',
										'htmlOptions' => array('class' => 'btns-positions'), // for inset effect


									),
								),
								/*	'htmlOptions'=>array(
                                        'class'=>'pull-right	'
                                    )*/
							)
						);
						?></div>
					<div class="col-md-3" style="text-align: left;">
						<?php echo Yii::app()->params['statement']['previousPage']; ?>




					</div>
				</div>
				<div class="box-body">
					<?php $this->widget('booster.widgets.TbDetailView', array(
						'data'=>$model,
						'attributes'=>array(
							'id',
							'name',
							'username',
							'created_at'
						),
					)); ?>
				</div>
			</div>
		</div>
</section>