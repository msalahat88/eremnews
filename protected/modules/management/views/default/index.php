<?php
/* @var $this DefaultController */
/* @var $server_cpu DefaultController */
/* @var $server_memory DefaultController */
/* @var $category Category */
/* @var $all_news News */

$this->breadcrumbs=array(
	$this->module->id,
);
$this->pageTitle = "Management";
?>
<section class="content">

	<div class="row">
	<?PHP $this->renderPartial('_info',array('this',$this/*,'server_memory'=>$server_memory,'server_cpu'=>$server_cpu*/)) ?>
	<?php $this->renderPartial('_adv_search_date', array('model' => $model,'this',$this)); ?>
		</div>
		<div class="row">
			<div class="col-sm-12">
<!--			<div class="col-sm-7 pull-right">
			<?php /*echo TbHtml::button('General',array('id'=>'btn_12','class'=>'btn btn-active'))*/?>
			<?php /*echo TbHtml::link('Posts',array('/management/default/total_posts'),array('class'=>'btn btn-link'))*/?>
</div>-->
				<div class="col-sm-6">
			<?PHP $this->renderPartial('_info_category_and_news',array(
		'this'=>$this,
		/*'server_memory'=>$server_memory,
		'server_cpu'=>$server_cpu,*/
		'total_news_posts'=>$total_news_posts,
		'all_news'=>$all_news
		)) ?></div>
				<div class="col-sm-6">
					<?php
					$this->renderPartial('total_posts',array('total_posts'=>$total_posts,'model'=>$model));
					?>
				</div>
			</div>
		</div>
</section>