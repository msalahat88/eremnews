<?php
/* @var $data Category */
/* @var $total DefaultController */
?>

<div class="progress-group">
    <span class="progress-text"><?PHP echo $data['title'] ?></span>
    <span class="progress-number"><b><?php echo $data['countedid']   ?></b>/<?PHP echo $total ?></span>
    <div class="progress sm">
        <div class="progress-bar progress-bar-<?php echo $color ?>" style="width: <?PHP if($data['countedid'] ) echo number_format( ($data['countedid'] /$total) * 100, 2 )  ?>%"></div>
    </div>
</div>
