<?php

class UserController extends BaseController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */


    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('index','view','update','status','create','admin','delete','updatePassword','edit'),
                'users'=>array('admin','super_admin'),
            ),

            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {


        if(isset($_POST['skin']))
            Yii::app()->session['skin'] = $_POST['skin'];
        $model=new User;
        $model->setScenario('create');

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if(isset($_POST['User']))
        {

            if(!empty($_POST['User']['profile_pic']))
            {
                $valid = $this->validateImageUploader($model, 'profile_pic', 'User_profile_pic_' . time());

            }

            $model->attributes=$_POST['User'];


            $valid  = true;
            $model->profile_pic = null;
            $valid = $this->validateProfileImageUploader($model,'profile_pic','user/'.$model->id.'__'.md5('profile_image'.time()).'_'.time());
            $model->attributes=$_POST['User'];
            $model->created_at = date('Y-m-d H:i:s');
            $model->gender = $_POST['User']['gender'];

            $model->background_color=Yii::app()->session['skin'];

            $model->active=1;
            if($valid) {
                if ($model->save()) {
                    Yii::app()->user->setFlash('create','Thank you for add conetns ... . .... ');
                    $this->redirect(array('view', 'id' => $model->id));
                }
            }
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }


    public function actionEdit(){
        if(isset($_POST)){
            if(isset($_POST['name']) && isset($_POST['value']) && isset($_POST['pk']) && isset($_POST['scenario'])){
                $model = $this->loadModel($_POST['pk']);

                if(!empty($_POST['name'])){
                    $name = $_POST['name'];
                    $value = $_POST['value'];
                    $model->$name = $value;
                    if($model->save())
                        echo true;
                    else
                        echo false;
                }
            }

        }
    }


    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        if(!isset(Yii::app()->session['skin'])) {
            Yii::app()->session['skin'] = $model->background_color;

        }
        $model->setScenario('update');


        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        $valid=true;
        if(isset($_POST['User']))
        {

            $Media = $model->profile_pic;



            if (!$this->FileEmpty($model, 'profile_pic')) {
                if (!$this->GetFileType($model, $Media, 'image')) {
                    /*                    $model->addError('profile_pic', 'It seems you Didn\'t upload a proper image file ..');*/
                    $valid = true;
                } else {
                    $model->profile_pic = $Media;
                }
            } else {
                if(!$this->GetFileType($model,'profile_pic','image','upload')){
                    $model->addError('profile_pic',"It seems you Didn't upload a proper image file ..");
                    $valid=false;
                }else
                    $model->profile_pic = $this->uploader($model, 'profile_pic', 'profile_pic_edit_id' . $model->id . '_' . time());
            }


            $model->attributes=$_POST['User'];
            $model->background_color = Yii::app()->session['skin'];
            $model->gender = $_POST['User']['gender'];

            if($valid) {
                if ($model->save()) {
                    Yii::app()->user->setFlash('update','Thank you for add conetns ... . .... ');

                    $this->redirect(array('view','id'=>$model->id));
                }
            }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {

        if($id != Yii::app()->user->id) {
            $model = $this->loadModel($id);
            $model->deleted = 1;
            $model->save();
        }
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $model=new User('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['User']))
            $model->attributes=$_GET['User'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {

        $model=new User('search');
        $model->unsetAttributes();  // clear any default values

        if(isset($_GET['User'])) {
            $this->data_search = $_GET['User'];
            $this->data_search($model);
            $model->attributes = $_GET['User'];
            $model->deleted = 0;
        }
        $this->render('admin', array(
            'model' => $model,
        ));


    }
    public function actionUpdatePassword($id){

        $model = new ProfileForm();
        $user = $this->loadModel($id);
        $model->setScenario('updatePassword');
        $this->performAjaxValidation($model);

        $valid=true;
        if(isset($_POST['ProfileForm'])) {

            if ($_POST['ProfileForm']['new_password'] != $_POST['ProfileForm']['confirm_password']) {
                $model->addError('new_password', "<b>Passwords does not match</b>");
                $valid = false;
            }
            if ($valid) {
                $user->password = md5($_POST['ProfileForm']['new_password']);

                if ($user->save()) {
                    $_POST['updated'] = true;
                    $this->redirect(array('view','id'=>$user->id));

                }
            }
        }
        $this->render('updatePassword',array(
            'model'=>$model,
            'model_id'=>$user
        ));

    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=User::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function actionStatus($id)
    {


        $model=$this->loadModel($id);
        $model->setScenario('activate');

        if($id != Yii::app()->user->id) {
            if ($model->active == 0) {
                $model->active = 1;
            } else {
                $model->active = 0;
            }
        }
        if($model->save()) {
            $this->redirect(array('admin','id'=>$model->id));

        }


    }


    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']))
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
