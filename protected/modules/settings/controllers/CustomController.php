<?php
class CustomController extends SettingsBaseControllers{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('edit','delete','index','create','update','rows','active'),
                'users' => array('admin','super_admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex(){

        $days = DaySettings::model()->getDays();

        $CustomSettingsForm = new CustomSettingsForm();

        $this->performAjaxValidation($CustomSettingsForm);

        if(isset($_POST['CustomSettingsForm'])){


            $data = $_POST['CustomSettingsForm'];

            if(empty($data['name_group'])){
                $CustomSettingsForm->addError('name_group','name group cannot be blank. *required.');
            }else{

                $obj = new SettingsObj();
                $obj->setIsNewRecord(true);
                $obj->name = $data['name_group'];
                $obj->save(false);
                $obj_id = $obj->id;

                $times = array();

                $category_ids = $data['category_ids'];

                /*if(empty($category_ids))
                    $CustomSettingsForm->addError('category_ids','category cannot be blank. *required.');*/

                $platform_id = array();

                $day_id = array();

                $valid = true;

                if(isset($data['platform_ids']) && !empty($data['platform_ids'])){
                    $platform = explode(',',$data['platform_ids']);
                    if (is_array($platform)){
                        foreach ($platform as $item){
                            $que = null;
                            $que = Platform::model()->findByAttributes(array('title'=>$item));
                            if(!empty($que)){
                                $platform_id[] = $que->id;
                            }
                        }
                    }else{
                        $valid=false;
                        $CustomSettingsForm->addError('platform_ids','platform\'s cannot be blank. *required.');
                    }

                }else{
                    $valid=false;
                    $CustomSettingsForm->addError('platform_ids','platform\'s cannot be blank. *required.');
                }

                if(isset($data['day_ids']) && !empty($data['day_ids'])){
                    $day = explode(',',$data['day_ids']);
                    if(is_array($day)){
                        foreach ($day as $item){
                            $que = null;
                            $que = DaySettings::model()->findByAttributes(array('title'=>$item));
                            if(!empty($que)){
                                $day_id[] = $que->id;
                            }
                        }
                    }else{
                        $valid=false;
                        $CustomSettingsForm->addError('day_ids','Day\'s cannot be blank. *required.');
                    }

                }else{
                    $valid=false;
                    $CustomSettingsForm->addError('day_ids','Day\'s cannot be blank. *required.');
                }


                if(isset($data['start_time']) && !empty($data['start_time'])){
                    $start_time = $data['start_time'];

                    foreach ($start_time as $index=>$item){

                        if(!empty($item)){

                            $times[$index]['start_time'] = $item;

                            if(!empty($data['end_time'][$index]))
                                $times[$index]['end_time'] = $data['end_time'][$index];
                            else{
                                $valid=false;
                                $CustomSettingsForm->addError('end_time','End time cannot be blank. *required.');
                            }

                            if(!empty($data['gap_time'][$index]))
                                $times[$index]['gap_time'] = $data['gap_time'][$index];
                            else{
                                $valid=false;
                                $CustomSettingsForm->addError('gap_time','Gap time cannot be blank. *required.');
                            }

                            if(isset($data['is_direct_push']))
                                if(!empty($data['is_direct_push'][$index+1]))
                                    $times[$index]['is_direct_push'] = $data['is_direct_push'][$index+1];

                            if(isset($data['direct_push_start_time']))
                                if(!empty($data['direct_push_start_time'][$index]))
                                    $times[$index]['direct_push_start_time'] = $data['direct_push_start_time'][$index];

                            if(isset($data['direct_push_end_time']))
                                if(!empty($data['direct_push_end_time'][$index]))
                                    $times[$index]['direct_push_end_time'] = $data['direct_push_end_time'][$index];
                        }else{
                            $valid=false;
                            $CustomSettingsForm->addError('start_time','Start time cannot be blank. *required.');
                        }

                    }

                }else{
                    $valid=false;
                    $CustomSettingsForm->addError('start_time','Start time cannot be blank. *required.');
                }

                if(empty($data['end_time'])){
                    $valid=false;
                    $CustomSettingsForm->addError('end_time','End time cannot be blank. *required.');
                }

                if(empty($data['gap_time']) and isset($data['gap_time'])){

                    $valid=false;
                    $CustomSettingsForm->addError('gap_time','Gap time cannot be blank. *required.');
                }

                if( !empty($times) && !empty($platform_id) && !empty($day_id)){
                    foreach ( $platform_id as $item) {
                        $platformModel = new PlatformCategorySettings();
                        $platformModel->setIsNewRecord(true);
                        $platformModel->obj_id = $obj_id;
                        $platformModel->platform_id = $item;
                        $platformModel->category_id = empty($category_ids)?null:$category_ids;
                        $platformModel->created_at = date('Y-m-d H:i:s');
                        if($platformModel->save(true)){
                            $id = $platformModel->id;
                            foreach ($day_id as $item_day) {
                                $days = new DaysSettings();
                                $days->setIsNewRecord(true);
                                $days->platform_category_settings_id = $id;
                                $days->obj_id = $obj_id;
                                $days->day_id = $item_day;
                                $days->created_at = date('Y-m-d H:i:s');
                                if(!$days->save(true))
                                    $valid = false;
                            }
                            foreach ($times as $item_time) {
                                $TimeSettings = new TimeSettings();
                                $TimeSettings->setIsNewRecord(true);
                                $TimeSettings->platform_category_settings_id = $id;
                                $TimeSettings->obj_id = $obj_id;
                                $TimeSettings->start_time = $item_time['start_time'];
                                $TimeSettings->end_time = $item_time['end_time'];
                                $TimeSettings->gap_time = $item_time['gap_time'];
                                $TimeSettings->is_direct_push = isset($item_time['is_direct_push'])?$item_time['is_direct_push']:null;
                                $TimeSettings->direct_push_start_time = isset($item_time['direct_push_start_time'])?$item_time['direct_push_start_time']:null;
                                $TimeSettings->direct_push_end_time = isset($item_time['direct_push_end_time'])?$item_time['direct_push_end_time']:null;
                                $TimeSettings->created_at = date('Y-m-d H:i:s');
                                if(!$TimeSettings->save(true))
                                    $valid = false;
                            }

                        }else
                            $valid = false;
                    }
                    if($valid){

                        Yii::app()->user->setFlash('success', "Data added successfully");
                        $this->redirect(array('/settings'));
                    }else{
                        PlatformCategorySettings::model()->deleteAllByAttributes(array('obj_id' => $obj_id));
                        TimeSettings::model()->deleteAllByAttributes(array('obj_id' => $obj_id));
                        DaysSettings::model()->deleteAllByAttributes(array('obj_id' => $obj_id));
                        SettingsObj::model()->deleteAllByAttributes(array('id' => $obj_id));

                    }
                }
                if(!$valid){
                    PlatformCategorySettings::model()->deleteAllByAttributes(array('obj_id' => $obj_id));
                    TimeSettings::model()->deleteAllByAttributes(array('obj_id' => $obj_id));
                    DaysSettings::model()->deleteAllByAttributes(array('obj_id' => $obj_id));
                    SettingsObj::model()->deleteAllByAttributes(array('id' => $obj_id));
                }

                $CustomSettingsForm->category_ids = $data['category_ids'];
                $CustomSettingsForm->platform_ids = $data['platform_ids'];
                $CustomSettingsForm->day_ids = $data['day_ids'];
                $CustomSettingsForm->name_group = $data['name_group'];
            }

        }

        $this->render('index',array('days'=>$days,'model'=>$CustomSettingsForm));
    }

    public function actionUpdate($id){

        $days = DaySettings::model()->getDays();

        $CustomSettingsForm = new CustomSettingsForm();

        $this->performAjaxValidation($CustomSettingsForm);

        $model = $this->loadModel($id);




        if(isset($_POST['CustomSettingsForm'])) {

            $data = $_POST['CustomSettingsForm'];

            $daysRemove = DaysSettings::model()->deleteAllByAttributes(array('obj_id' => $id));
            $platform_categoryRemove = PlatformCategorySettings::model()->deleteAllByAttributes(array('obj_id' => $id));
            $timeRemove = TimeSettings::model()->deleteAllByAttributes(array('obj_id' => $id));



         //   if ($daysRemove && $platform_categoryRemove && $timeRemove) {

                if (empty($data['name_group'])) {
                    $CustomSettingsForm->addError('name_group', 'name group cannot be blank. *required.');
                } else {


                    $model->name = $data['name_group'];
                    $model->save(false);
                    $obj_id = $model->id;


                    $times = array();

                    $category_ids = $data['category_ids'];



                    $platform_id = array();

                    $day_id = array();

                    $valid = true;

                    if (isset($data['platform_ids']) && !empty($data['platform_ids'])) {
                        $platform = explode(',', $data['platform_ids']);
                        if (is_array($platform)) {
                            foreach ($platform as $item) {
                                $que = null;
                                $que = Platform::model()->findByAttributes(array('title' => $item));
                                if (!empty($que)) {
                                    $platform_id[] = $que->id;
                                }
                            }
                        } else {
                            $CustomSettingsForm->addError('platform_ids', 'platform\'s cannot be blank. *required.');
                        }

                    } else {
                        $CustomSettingsForm->addError('platform_ids', 'platform\'s cannot be blank. *required.');
                    }

                    if (isset($data['day_ids']) && !empty($data['day_ids'])) {
                        $day = explode(',', $data['day_ids']);
                        if (is_array($day)) {
                            foreach ($day as $item) {
                                $que = null;
                                $que = DaySettings::model()->findByAttributes(array('title' => $item));
                                if (!empty($que)) {
                                    $day_id[] = $que->id;
                                }
                            }
                        } else {
                            $CustomSettingsForm->addError('day_ids', 'Day\'s cannot be blank. *required.');
                        }

                    } else {
                        $CustomSettingsForm->addError('day_ids', 'Day\'s cannot be blank. *required.');
                    }


                    if (isset($data['start_time']) && !empty($data['start_time'])) {
                        $start_time = $data['start_time'];

                        foreach ($start_time as $index => $item) {

                            if (!empty($item)) {

                                $times[$index]['start_time'] = $item;

                                if (!empty($data['end_time'][$index]))
                                    $times[$index]['end_time'] = $data['end_time'][$index];
                                else
                                    $CustomSettingsForm->addError('end_time', 'End time cannot be blank. *required.');

                                if (!empty($data['gap_time'][$index]))
                                    $times[$index]['gap_time'] = $data['gap_time'][$index];
                                else
                                    $CustomSettingsForm->addError('gap_time', 'Gap time cannot be blank. *required.');

                                if (isset($data['is_direct_push'])){
                                    if (isset($data['is_direct_push'][$index]) && !empty($data['is_direct_push'][$index]))
                                        $times[$index]['is_direct_push'] = $data['is_direct_push'][$index];
                                }


                                if (isset($data['direct_push_start_time']))
                                    if (!empty($data['direct_push_start_time'][$index]))
                                        $times[$index]['direct_push_start_time'] = $data['direct_push_start_time'][$index];

                                if (isset($data['direct_push_end_time']))
                                    if (!empty($data['direct_push_end_time'][$index]))
                                        $times[$index]['direct_push_end_time'] = $data['direct_push_end_time'][$index];
                            } else
                                $CustomSettingsForm->addError('start_time', 'Start time cannot be blank. *required.');

                        }

                    } else {
                        $CustomSettingsForm->addError('start_time', 'Start time cannot be blank. *required.');
                    }

                    if (!empty($data['end_time'])) {
                        $CustomSettingsForm->addError('end_time', 'End time cannot be blank. *required.');
                    }
                    if (!empty($data['gap_time'])) {
                        $CustomSettingsForm->addError('gap_time', 'Gap time cannot be blank. *required.');
                    }



                    if (!empty($times) && !empty($platform_id) && !empty($day_id)) {
                        foreach ($platform_id as $item) {
                            $platformModel = new PlatformCategorySettings();
                            $platformModel->setIsNewRecord(true);
                            $platformModel->obj_id = $obj_id;
                            $platformModel->platform_id = $item;
                            $platformModel->category_id =  empty($category_ids)?null:$category_ids;
                            $platformModel->created_at = date('Y-m-d H:i:s');
                            if ($platformModel->save(true)) {
                                $id = $platformModel->id;
                                foreach ($day_id as $item_day) {
                                    $days = new DaysSettings();
                                    $days->setIsNewRecord(true);
                                    $days->platform_category_settings_id = $id;
                                    $days->obj_id = $obj_id;
                                    $days->day_id = $item_day;
                                    $days->created_at = date('Y-m-d H:i:s');
                                    if (!$days->save(true))
                                        $valid = false;
                                }
                                foreach ($times as $item_time) {
                                    $TimeSettings = new TimeSettings();
                                    $TimeSettings->setIsNewRecord(true);
                                    $TimeSettings->platform_category_settings_id = $id;
                                    $TimeSettings->obj_id = $obj_id;
                                    $TimeSettings->start_time = $item_time['start_time'];
                                    $TimeSettings->end_time = $item_time['end_time'];
                                    $TimeSettings->gap_time = $item_time['gap_time'];
                                    $TimeSettings->is_direct_push = isset($item_time['is_direct_push']) ? $item_time['is_direct_push'] : null;
                                    $TimeSettings->direct_push_start_time = isset($item_time['direct_push_start_time']) ? $item_time['direct_push_start_time'] : null;
                                    $TimeSettings->direct_push_end_time = isset($item_time['direct_push_end_time']) ? $item_time['direct_push_end_time'] : null;
                                    $TimeSettings->created_at = date('Y-m-d H:i:s');
                                    if (!$TimeSettings->save(true))
                                        $valid = false;
                                }



                            } else
                                $valid = false;
                        }
                        if ($valid) {
                            Yii::app()->user->setFlash('success', "Data updated successfully");
                             $this->redirect(array('custom/update/id/'.$obj_id));
                        }
                    }


                }

            //}
        }

        $this->render('update',array(
            'obj'=>$model,
            'days'=>$days,'model'=>$CustomSettingsForm
        ));
    }
    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    	public function actionDelete($id)
        {
            $model = $this->loadModel($id);

            if($model->active)
                $model->active = 0;
            else
                $model->active = 1;

            $model->save(false);

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('settings'));
        }


        public function actionActive($id)
        {
            $model = $this->loadModel($id);

            if($model->active)
                $model->active = 0;
            else
                $model->active = 1;

            $model->save(false);

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('settings'));
        }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return SettingsObj the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=SettingsObj::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function actionRows()
    {
         Yii::app()->session['timeRow'] += 1;
            $this->renderPartial('_timeRow',array('counter'=>Yii::app()->session['timeRow'] ));
    }


    /**
     * Performs the AJAX validation.
     * @param CustomSettingsForm $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='custom-Settings')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}