<?php

/**
 * This is the model class for table "settings".
 *
 * The followings are the available columns in table 'settings':
 * @property integer $id
 * @property string $start_date_time
 * @property string $end_date_time
 * @property integer $direct_push
 * @property string $direct_push_start
 * @property string $direct_push_end
 * @property integer $how_many_posts
 * @property integer $gap_time
 * @property string $timezone
 * @property integer $generator_is_running
 * @property integer $pinned
 * @property string $updated_at
 * @property string $created_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Settings extends BaseModels
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'settings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('start_date_time, end_date_time, direct_push_start, direct_push_end, timezone, created_at', 'required'),
			array('direct_push, how_many_posts, gap_time, generator_is_running, pinned, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('timezone', 'length', 'max'=>255),
			array('updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, start_date_time, end_date_time, direct_push, direct_push_start, direct_push_end, how_many_posts, gap_time, timezone, generator_is_running, pinned, updated_at, created_at, created_by, updated_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'start_date_time' => 'Start Date Time',
			'end_date_time' => 'End Date Time',
			'direct_push' => 'Direct Push',
			'direct_push_start' => 'Direct Push Start',
			'direct_push_end' => 'Direct Push End',
			'how_many_posts' => 'How Many Posts',
			'gap_time' => 'Gap Time',
			'timezone' => 'Timezone',
			'generator_is_running' => 'Generator Is Running',
			'pinned' => 'Pinned',
			'updated_at' => 'Updated At',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
			'updated_by' => 'Updated By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('start_date_time',$this->start_date_time,true);
		$criteria->compare('end_date_time',$this->end_date_time,true);
		$criteria->compare('direct_push',$this->direct_push);
		$criteria->compare('direct_push_start',$this->direct_push_start,true);
		$criteria->compare('direct_push_end',$this->direct_push_end,true);
		$criteria->compare('how_many_posts',$this->how_many_posts);
		$criteria->compare('gap_time',$this->gap_time);
		$criteria->compare('timezone',$this->timezone,true);
		$criteria->compare('generator_is_running',$this->generator_is_running);
		$criteria->compare('pinned',$this->pinned);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('updated_by',$this->updated_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function all_numbers(){
        $data=array();
        for($i=1;$i<=500;$i++){
            $data[$i]=$i;
        }
        return $data;
    }

    public function posts_per_week(){
        $data=array();
        for($i=1;$i<=7;$i++){
            $data[$i]=$i;
        }
        return $data;
    }
    public function gap_time(){
        $data=array();
        for($i=5;$i<=120;$i+=5){
            if($i == 60)
                $data[$i]=$i.' minute (One hour) ';
            elseif($i == 120)
                $data[$i]=$i.' minute  (Two hour) ';
            else
                $data[$i]=$i.' minute ';
        }
        return $data;
    }



    public function getTimeZone(){
        $locations = array();
        foreach (timezone_identifiers_list() as $zone)
            $locations[$zone]=$zone;

        return $locations;
    }

    public function get_generator_is_running(){

        $settings = $this->findByPk(1);
        if(!empty($settings)){
            return $settings->generator_is_running;
        }
        return false;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Settings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
