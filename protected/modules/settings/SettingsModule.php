<?php

class SettingsModule extends CWebModule
{
    /**
     * This is the model class for table "SettingsModule".
     *
     * @property string $NameModule
     */

    public $NameModule = 'settings';

	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'settings.models.*',
			'settings.components.*',
		));

        if(!class_exists('HelperSettings'))
            throw new CHttpException(404,'The class HelperSettings does not exist.');

        HelperSettings::checkYiiVersion(Yii::getVersion());

        if(!class_exists('BaseController'))
        throw new CHttpException(404,'The class BaseController does not exist.');

        if(!class_exists('BaseModels'))
        throw new CHttpException(404,'The class BaseModels does not exist.');

        if(!class_exists('Settings'))
        throw new CHttpException(404,'The class Settings in file /models does not exist.');

        if(Yii::app()->user->getState('type') != 'admin' and Yii::app()->user->getState('type') != 'super_admin'){
            throw new CHttpException(403, 'You are not authorized to perform this action');
        }

       // HelperSettings::publishCss('3');
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			return true;
		}
		else
			return false;
	}

	 /**
     * @param string $NameModule
      */
    public function setNameModule($NameModule)
    {
        $this->NameModule = $NameModule ;
    }

    public function getNameModule()
    {
        return $this->NameModule;
    }
}
