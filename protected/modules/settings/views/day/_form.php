<?php
/* @var $this DayController */
/* @var $model DaySettings */
/* @var $form TbActiveForm */
?>
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
			'id'=>'Day-form',
			'enableAjaxValidation'=>true,
			'type'=>'vertical',
		)); ?>

		<div class="col-sm-9">
			<?php echo $form->dropDownListGroup($model, 'title', array('widgetOptions' => array('data' => $model->getDays()))); ?>
		</div>

		<div class="col-sm-2">
			<div class="form-actions pull-right" style="margin-top:23px;">
				<?php $this->widget(
					'booster.widgets.TbButton',
					array(
						'buttonType' => 'submit',
						'context' => 'primary',
						'label' =>'Save',
					)
				);
				?>
			</div>
		</div>

		<?php $this->endWidget(); ?>
