<?php
/* @var $this CustomController */
/* @var $days DaySettings */
/* @var $model CustomSettingsForm */
/* @var $form TbActiveForm */
/* @var $obj SettingsObj */
/* @var $day DaysSettings */
/* @var $time TimeSettings */
/* @var $item PlatformCategorySettings */


/*echo '<pre>';
print_r($obj->Days);
print_r($obj->TimeSettings);
print_r($obj->PlatformCategory);*/
$Days = $obj->Days;
  $counter = count($Days);
$count = 1;
foreach ($Days as $day) {
    if($counter > $count)
    $model->day_ids .= $day->Day->title.',';
    else
    $model->day_ids .= $day->Day->title.'';
    $count++;
}


$obj_Platform = $obj->PlatformCategory;

  $counter = count($obj_Platform);
$count = 1;
$platform_category = 0 ;
foreach ($obj_Platform as $item) {
    if($counter > $count)
    $model->platform_ids .= $item->Platform->title.',';
    else
        $model->platform_ids .= $item->Platform->title.'';
    $count++;

    if($platform_category == 0){
        $platform_category = $item->id;
    }
    $model->category_ids = $item->category_id;
}

$model->name_group = $obj->name;
?>


<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
    'id'=>'custom-Settings',
    'enableAjaxValidation'=>true,
    'type'=>'horizontal',
)); ?>

<div class="col-sm-12"><h3 class="page-header">Create Custom Settings</h3></div>
<input type="hidden" value="<?php echo $obj->id?>" name="obj_id">
<div class="col-sm-9 col-sm-push-1">
    <div class="col-sm-12">
        <?php echo $form->textFieldGroup(
            $model,
            'name_group',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-10',
                ),
                'labelOptions' => array(
                    'class' => 'col-sm-2',
                )
            )
        );?>
    </div>
    <div class="col-sm-6">
        <?php echo $form->select2Group(
            $model,
            'platform_ids',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-10',
                ),
                'labelOptions' => array(
                    'class' => 'col-sm-2',
                ),
                'widgetOptions' => array(
                    'asDropDownList' => false,
                    'options' => array(
                        'tags' => array('Facebook','Twitter','Instagram'),
                        'placeholder' => 'Facebook or Twitter or Instagram',
                        /* 'width' => '40%', */
                        'tokenSeparators' => array(',', ' ')
                    )
                )
            )
        );?>
    </div>
    <div class="col-sm-6">
        <?php echo $form->dropDownListGroup($model,'category_ids',array(
            'widgetOptions'=>array(
                'data'=>CHtml::listData(Category::model()->findAll('deleted=0'),'id','title','page_index'),
                'htmlOptions'=>array(
                    'empty'=>'Choose One'
                )
            ),
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-10',
            ),
            'labelOptions' => array(
                'class' => 'col-sm-2',
            ),
        )); ?>
    </div>
    <div class="col-sm-12">
        <?php echo $form->select2Group(
            $model,
            'day_ids',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-11',
                ),
                'labelOptions' => array(
                    'class' => 'col-sm-1',
                ),
                'widgetOptions' => array(
                    'asDropDownList' => false,
                    'options' => array(
                        'tags' => array('Sunday','Monday','Tuesday','Wednesday', 'Thursday', 'Friday', 'Saturday'),
                        'placeholder' => 'Sunday or Monday or Tuesday',
                        /* 'width' => '40%', */
                        'tokenSeparators' => array(',', ' ')
                    )
                )
            )
        );?>
    </div>
</div>

<div class="col-sm-12"><h3 class="page-header">Times</h3></div>

<?php

$TimeSettings = $obj->TimeSettings;

$counter = count($TimeSettings);

$index = 1 ;

$counter_time = 0;

foreach ($TimeSettings as $time) {

    if($platform_category == $time->platform_category_settings_id){

        $model->start_time = $time->start_time;

        $model->end_time = $time->end_time;

        $model->is_direct_push = $time->is_direct_push;

        $model->direct_push_start_time = $time->direct_push_start_time;

        $model->direct_push_end_time = $time->direct_push_end_time;

        $model->gap_time  = $time->gap_time;

        $this->renderPartial('_timeRow_update',array('this'=>$this,'model'=>$model,'counter'=>$counter_time,'remove'=>$index));

        $index = $index == 0 ?1:0;
        $counter_time++;

    }

}
?>
<div id="times">

</div>

<div class="col-sm-12">
    <div class="form-actions pull-right" style="margin-top:23px;">
        <?php $this->widget(
            'booster.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'context' => 'primary',
                'label' =>'Save',
            )
        );
        ?>
    </div>
</div>

<?php $this->endWidget(); ?>
<script>
    $( document ).ready(function() {
        $("input[name^='CustomSettingsForm[direct_push_end_time][]']").datetimepicker({datepicker:false,format:'H:i:00',step:30});
        $("input[name^='CustomSettingsForm[direct_push_start_time][]']").datetimepicker({datepicker:false,format:'H:i:00',step:30});
        $("input[name^='CustomSettingsForm[end_time][]']").datetimepicker({datepicker:false,format:'H:i:00',step:30});
        $("input[name^='CustomSettingsForm[start_time][]']").datetimepicker({datepicker:false,format:'H:i:00',step:30});
    });
</script>
<script>


    $( document ).ready(function() {
        $(".margin_pules1233333333").click(function () {

            $.get('<?php /*echo $this->createUrl('/settings/custom/rows')*/ ?>',{},function (data) {
                $('#times').append(data);
            });

        });
    });
</script>
