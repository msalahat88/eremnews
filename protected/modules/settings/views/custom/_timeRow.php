
<div class="col-sm-12 row_time_<?PHP echo $counter ?>">
    <script>
        $( document ).ready(function() {
            $("input[name^='CustomSettingsForm[direct_push_end_time][]']").datetimepicker({datepicker:false,format:'H:i:00',step:30});
            $("input[name^='CustomSettingsForm[direct_push_start_time][]']").datetimepicker({datepicker:false,format:'H:i:00',step:30});
            $("input[name^='CustomSettingsForm[end_time][]']").datetimepicker({datepicker:false,format:'H:i:00',step:30});
            $("input[name^='CustomSettingsForm[start_time][]']").datetimepicker({datepicker:false,format:'H:i:00',step:30});
        });
        $( document ).ready(function() {
            $(".margin_pules<?PHP echo $counter ?>").click(function () {

                $.get('<?php echo 'custom/rows' ?>',{},function (data) {
                    $('#times').append(data);
                });
            });
            $(".margin_remove<?PHP echo $counter ?>").click(function () {
                $('.'+$(this).data('row')).remove();
            });

            jQuery('#custom-Settings').yiiactiveform(
                {
                    'errorCssClass':'has-error',
                    'successCssClass':'has-success',
                    'inputContainer':'div.form-group',
                    'attributes':[
                        {
                            'id':'CustomSettingsForm_platform_ids_<?php echo $counter?>',
                            'inputID':'CustomSettingsForm_platform_ids_<?php echo $counter?>',
                            'errorID':'CustomSettingsForm_platform_ids_em_<?php echo $counter?>',
                            'model':'CustomSettingsForm',
                            'name':'platform_ids',
                            'enableAjaxValidation':true
                        },{
                            'id':'CustomSettingsForm_category_ids_<?php echo $counter?>',
                            'inputID':'CustomSettingsForm_category_ids_<?php echo $counter?>',
                            'errorID':'CustomSettingsForm_category_ids_em_<?php echo $counter?>',
                            'model':'CustomSettingsForm',
                            'name':'category_ids',
                            'enableAjaxValidation':true
                        },{
                            'id':'CustomSettingsForm_day_ids_<?php echo $counter?>',
                            'inputID':'CustomSettingsForm_day_ids_<?php echo $counter?>',
                            'errorID':'CustomSettingsForm_day_ids_em_<?php echo $counter?>',
                            'model':'CustomSettingsForm',
                            'name':'day_ids',
                            'enableAjaxValidation':true
                        },{
                            'id':'CustomSettingsForm_start_time_<?php echo $counter?>',
                            'inputID':'CustomSettingsForm_start_time_<?php echo $counter?>',
                            'errorID':'CustomSettingsForm_start_time_em_<?php echo $counter?>',
                            'model':'CustomSettingsForm',
                            'name':'start_time','enableAjaxValidation':true
                        },{
                            'id':'CustomSettingsForm_end_time_<?php echo $counter?>',
                            'inputID':'CustomSettingsForm_end_time_<?php echo $counter?>',
                            'errorID':'CustomSettingsForm_end_time_em_<?php echo $counter?>',
                            'model':'CustomSettingsForm',
                            'name':'end_time',
                            'enableAjaxValidation':true
                        },{
                            'id':'CustomSettingsForm_direct_push_start_time_<?php echo $counter?>',
                            'inputID':'CustomSettingsForm_direct_push_start_time_<?php echo $counter?>',
                            'errorID':'CustomSettingsForm_direct_push_start_time_em_<?php echo $counter?>',
                            'model':'CustomSettingsForm',
                            'name':'direct_push_start_time',
                            'enableAjaxValidation':true
                        },{
                            'id':'CustomSettingsForm_direct_push_end_time_<?php echo $counter?>',
                            'inputID':'CustomSettingsForm_direct_push_end_time_<?php echo $counter?>',
                            'errorID':'CustomSettingsForm_direct_push_end_time_em_',
                            'model':'CustomSettingsForm',
                            'name':'direct_push_end_time',
                            'enableAjaxValidation':true
                        },{
                            'id':'CustomSettingsForm_gap_time_<?php echo $counter?>',
                            'inputID':'CustomSettingsForm_gap_time_<?php echo $counter?>',
                            'errorID':'CustomSettingsForm_gap_time_em_<?php echo $counter?>',
                            'model':'CustomSettingsForm',
                            'name':'gap_time',
                            'enableAjaxValidation':true
                        }
                        ],'errorCss':'error'});
            guiders.createGuider({'id':'first','buttons':[{'name':'Start','classString':'tourcolor','onclick':function(){  document.location = '/thenational/settings/update/1.html#guider=SettingsTour';}},{'name':'Exit','onclick':function(){guiders.hideAll();}}],'description':'<b>Ready to start the tour press Start<\/b>','overlay':true,'title':'Tour','xButton':true,'autoFocus':true});
        });
    </script>
    <div class="col-sm-2">
        <div class="form-group">
            <label class="col-sm-12 control-label required" for="CustomSettingsForm_start_time">
                Start Time <span class="required">*</span>
            </label>
            <div class="col-sm-12">
                <input name="CustomSettingsForm[start_time][]" class="form-control" placeholder="Start Time" id="CustomSettingsForm_start_time_<?php echo $counter?>" type="text">
                <div class="help-block error" id="CustomSettingsForm_start_time_em_<?php echo $counter?>" style="display:none">
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <label class="col-sm-12 control-label required" for="CustomSettingsForm_end_time">
                End Time <span class="required">*</span>
            </label>
            <div class="col-sm-12">
                <input name="CustomSettingsForm[end_time][]" class="form-control" placeholder="End Time" id="CustomSettingsForm_end_time_<?php echo $counter?>" type="text">
                <div class="help-block error" id="CustomSettingsForm_end_time_em_<?php echo $counter?>" style="display:none">
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <label class="col-sm-12 control-label" for="CustomSettingsForm_direct_push_start_time">
                Direct Push start
            </label>
            <div class="col-sm-12">
                <div class="input-group">
                    <input name="CustomSettingsForm[direct_push_start_time][]" class="form-control" placeholder="Direct Push start" id="CustomSettingsForm_direct_push_start_time_<?php echo $counter?>" type="text">
                    <span class="input-group-addon">
                       <!-- <input id="ytCustomSettingsForm_is_direct_push" type="hidden" value="0" name="CustomSettingsForm[is_direct_push][]">-->
                        <input name="CustomSettingsForm[is_direct_push][]" id="CustomSettingsForm_is_direct_push_<?php echo $counter?>" value="1" type="checkbox">
                    </span>
                </div>
                <div class="help-block error" id="CustomSettingsForm_direct_push_start_time_em_" style="display:none">

                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <label class="col-sm-12 control-label" for="CustomSettingsForm_direct_push_end_time">
                Direct Push end
            </label>
            <div class="col-sm-12">
                <input name="CustomSettingsForm[direct_push_end_time][]" class="form-control" placeholder="Direct Push end" id="CustomSettingsForm_direct_push_end_time_<?php echo $counter?>" type="text">
                <div class="help-block error" id="CustomSettingsForm_direct_push_end_time_em_" style="display:none"></div>
            </div>
        </div>
    </div>

    <div class="col-sm-2">
        <div class="form-group ">
            <label class="col-sm-12 control-label required" for="CustomSettingsForm_gap_time">
                Gap Time <span class="required">*</span>
            </label>
            <div class="col-sm-12">
                <select name="CustomSettingsForm[gap_time][]" class="form-control" placeholder="Gap Time" id="CustomSettingsForm_gap_time_<?php echo $counter?>">
                    <option value="0">0 minute </option>
                    <option value="5">5 minute </option>
                    <option value="10">10 minute </option>
                    <option value="15">15 minute </option>
                    <option value="20">20 minute </option>
                    <option value="25">25 minute </option>
                    <option value="30">30 minute </option>
                    <option value="35">35 minute </option>
                    <option value="40">40 minute </option>
                    <option value="45">45 minute </option>
                    <option value="50">50 minute </option>
                    <option value="55">55 minute </option>
                    <option value="60">60 minute (One hour) </option>
                    <option value="65">65 minute </option>
                    <option value="70">70 minute </option>
                    <option value="75">75 minute </option>
                    <option value="80">80 minute </option>
                    <option value="85">85 minute </option>
                    <option value="90">90 minute </option>
                    <option value="95">95 minute </option>
                    <option value="100">100 minute </option>
                    <option value="105">105 minute </option>
                    <option value="110">110 minute </option>
                    <option value="115">115 minute </option>
                    <option value="120">120 minute  (Two hour) </option>
                </select><div class="help-block error" id="CustomSettingsForm_gap_time_em_" style="display:none"></div></div></div>    </div>
    <div class="col-sm-2">
        <button class="btn btn-success btn-sm margin_pules margin_pules<?PHP echo $counter ?> btn" name="add_btn" id="add_btn" type="button">+</button>
        <button class="btn btn-danger btn-sm margin_remove margin_remove<?PHP echo $counter ?> btn" name="add_btn" data-row="row_time_<?PHP echo $counter ?>" id="remove_btn" type="button">-</button>
    </div>
</div>