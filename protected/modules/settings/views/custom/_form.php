<?php
/* @var $this CustomController */
/* @var $days DaySettings */
/* @var $model CustomSettingsForm */
/* @var $form TbActiveForm */
?>


            <?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
                'id'=>'custom-Settings',
                'enableAjaxValidation'=>true,
                'type'=>'horizontal',
            )); ?>

<div class="col-sm-12"><h3 class="page-header">Create Custom Settings</h3></div>

            <div class="col-sm-9 col-sm-push-1">
                <div class="col-sm-12">
                    <?php echo $form->textFieldGroup(
                        $model,
                        'name_group',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-10',
                            ),
                            'labelOptions' => array(
                                'class' => 'col-sm-2',
                            )
                        )
                    );?>
                </div>
                <div class="col-sm-6">
                    <?php echo $form->select2Group(
                        $model,
                        'platform_ids',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-10',
                            ),
                            'labelOptions' => array(
                                'class' => 'col-sm-2',
                            ),
                            'widgetOptions' => array(
                                'asDropDownList' => false,
                                'options' => array(
                                    'tags' => array('Facebook','Twitter','Instagram'),
                                    'placeholder' => 'Facebook or Twitter or Instagram',
                                    /* 'width' => '40%', */
                                    'tokenSeparators' => array(',', ' ')
                                )
                            )
                        )
                    );?>
                </div>
                <div class="col-sm-6">
                    <?php echo $form->dropDownListGroup($model,'category_ids',array(
                        'widgetOptions'=>array(
                            'data'=>CHtml::listData(Category::model()->findAll('deleted=0'),'id','title','page_index'),
                            'htmlOptions'=>array(
                                'empty'=>'Choose One'
                            )
                        ),
                        'wrapperHtmlOptions' => array(
                            'class' => 'col-sm-10',
                        ),
                        'labelOptions' => array(
                            'class' => 'col-sm-2',
                        ),
                    )); ?>
                </div>
                <div class="col-sm-12">
                    <?php echo $form->select2Group(
                        $model,
                        'day_ids',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-11',
                            ),
                            'labelOptions' => array(
                                'class' => 'col-sm-1',
                            ),
                            'widgetOptions' => array(
                                'asDropDownList' => false,
                                'options' => array(
                                    'tags' => array('Sunday','Monday','Tuesday','Wednesday', 'Thursday', 'Friday', 'Saturday'),
                                    'placeholder' => 'Sunday or Monday or Tuesday',
                                    /* 'width' => '40%', */
                                    'tokenSeparators' => array(',', ' ')
                                )
                            )
                        )
                    );?>
                </div>
            </div>

            <div class="col-sm-12"><h3 class="page-header">Times</h3></div>

            <?php    $this->renderPartial('_time',array('this'=>$this,'model'=>$model,'form'=>$form,'remove'=>false)); ?>
            <div id="times">

            </div>

            <div class="col-sm-12">
                <div class="form-actions pull-right" style="margin-top:23px;">
                    <?php $this->widget(
                        'booster.widgets.TbButton',
                        array(
                            'buttonType' => 'submit',
                            'context' => 'primary',
                            'label' =>'Save',
                        )
                    );
                    ?>
                </div>
            </div>

            <?php $this->endWidget(); ?>
<script>
    $( document ).ready(function() {
        $("input[name^='CustomSettingsForm[direct_push_end_time][]']").datetimepicker({datepicker:false,format:'H:i:00',step:30});
        $("input[name^='CustomSettingsForm[direct_push_start_time][]']").datetimepicker({datepicker:false,format:'H:i:00',step:30});
        $("input[name^='CustomSettingsForm[end_time][]']").datetimepicker({datepicker:false,format:'H:i:00',step:30});
        $("input[name^='CustomSettingsForm[start_time][]']").datetimepicker({datepicker:false,format:'H:i:00',step:30});
    });
</script>
<script>


    $( document ).ready(function() {
        $(".margin_pules").click(function () {
            
            $.get('<?php echo $this->createUrl('custom/rows') ?>',{},function (data) {
                $('#times').append(data);
            });
        });
    });
</script>
