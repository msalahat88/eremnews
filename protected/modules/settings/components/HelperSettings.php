<?php
class HelperSettings{

    public $NameModule;


    private static function findInModule($parent, $moduleID) {
        if ($parent->getModule($moduleID)) {
            return $parent->getModule($moduleID);
        } else {
            $modules = $parent->getModules();
            foreach ($modules as $mod => $conf) {
                return self::findInModule($parent->getModule($mod), $moduleID);
            }
        }
        return null;
    }

    public static function findModule($moduleID) {
        if (Yii::app()->getModule($moduleID)) {
            return Yii::app()->getModule($moduleID);
        }
        $modules = Yii::app()->getModules();
        foreach ($modules as $mod=>$conf) {
            if (Yii::app()->getModule($mod)) {
                return self::findInModule(Yii::app()->getModule($mod), $moduleID);
            }
        }
        return null;
    }

    public static function translate($source, $text, $lang = null) {
        return self::findModule(self::getNameModule())->tr->translate($source, $text, $lang);
    }

    public static function checkYiiVersion($checkVersion) {

        //remove dev builds
        $version = preg_replace("/[a-z]/", "", Yii::getVersion());
        $yiiVersionNoBuilds = explode("-", $version);
        $checkVersion = explode(".", $checkVersion);
        $yiiVersion = explode(".", $yiiVersionNoBuilds[0]);
        $yiiVersion[2] = isset($yiiVersion[2]) ? $yiiVersion[2]  : "0";
        if ($yiiVersion[0] > $checkVersion[0]) {
            return true;
        } else if ($yiiVersion[0] < $checkVersion[0]) {
            return false;
        } else {
            if ($yiiVersion[1] > $checkVersion[1]) {
                return true;
            } else if ($yiiVersion[1] < $checkVersion[1]) {
                return false;
            } else {
                if ($yiiVersion[2] > $checkVersion[2]) {
                    return true;
                } else if ($yiiVersion[2] == $checkVersion[2]) {
                    return true;
                } else {
                    return false;
                }
            }
        }

    }

    public static function publishCss($css, $forcePublish = false) {
        if (Yii::app()->request->isAjaxRequest && !$forcePublish) {
            return true;
        }
        //Search in default Yii css directory
        $cssFile = Yii::app()->clientScript->isCssFileRegistered(Yii::app()->request->baseUrl . "/css/" . $css);// . DIRECTORY_SEPARATOR . $css;

        if (is_file($cssFile) && !Yii::app()->clientScript->isCssFileRegistered(Yii::app()->request->baseUrl . "/css/" . $css)) {
            $cssUrl = Yii::app()->request->baseUrl . "/css/" . $css;
            Yii::app()->clientScript->registerCssFile($cssUrl);
            self::findModule("srbac")->setCssUrl($cssUrl);
            return true;
        } else {
            // Search in srbac css dir

            $cssFile = Yii::getPathOfAlias("srbac.css") . DIRECTORY_SEPARATOR . $css;
            $cssDir = Yii::getPathOfAlias("srbac.css");
            if (is_file($cssFile)) {
                $published = Yii::app()->assetManager->publish($cssDir);
                $cssFile = $published . "/" . $css;
                if (!Yii::app()->clientScript->isCssFileRegistered($cssFile)) {
                    Yii::app()->clientScript->registerCssFile($cssFile);
                }
                return true;
            } else {
                return false;
            }
        }
    }

    public function setNameModule($NameModule)
    {
        $this->NameModule = $NameModule ;
    }

    public function getNameModule()
    {
        return $this->NameModule;
    }
}