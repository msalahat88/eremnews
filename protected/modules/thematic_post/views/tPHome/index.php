<?php
/* @var $this GeneratorPostController */
/* @var $model GeneratorPost */
/* @var $form TbActiveForm */
?>
<style>
    .col-sm-12.main_image {
        margin-bottom: 20px;
    }
    .ui-dialog .ui-dialog-content {
        border: 0;
        padding: .5em 1em;
        overflow: auto;
        zoom: 1;
    }
    .ui-dialog { z-index: 1000 !important ;}

</style>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script>
    $(document).ready(function () {

        var check_for_link=$.trim($("#GeneratorPost_link").val());
        var confirmed = true;
        var previous_url = $('#GeneratorPost_link').val();
        if(check_for_link.length>0)
        {
            $.post('<?PHP echo CController::createUrl('checkLink') ?>',{link:check_for_link},function(data){
                if(data != "") {
                    $('.link_exist_error').addClass('alert alert-warning');

                    $('.link_exist_error').html(data);
                    $.each($('.facebook_count_items'),function(key,val){
                        if($('.facebook_count_items').length-1 != key){
                            $(this).slice(key).remove();
                        }
                    });
                    $.each($('.twitter_count_items'),function(key,val){

                        if($('.twitter_count_items').length-1 != key){
                            $(this).slice(key).remove();
                        }
                    }); $.each($('.instagram_count_items'),function(key,val){

                        if($('.instagram_count_items').length-1 != key){
                            $(this).slice(key).remove();
                        }
                    });
                }

            });
        }

        $('input[type=checkbox]').change(function(){
            if($('#GeneratorPost_submit_to_all').is(':checked')){
                $('.Platforms').prop('checked',true);
            }
            var input=0;
            $type = $("#ytGeneratorPost_twitter_type");
            var post = $("#GeneratorPost_twitter_post").val();
            var num = 140;
            $.get('<?PHP echo CController::createUrl('getTwitterSize') ?>', {text: post,type:$("#ytGeneratorPost_twitter_type").val()}, function (data) {
                    input = num - data;
                    console.log(input);
                    $("#twitter_counter").text(input);
                    if (input > 0) {
                        $("#schedule").show();
                        $("#post_now_btn").show();
                        $(".error-twitter").hide();
                        $(".error-twitter").hide();
                    }
                    if($('#GeneratorPost_platforms_1').is(':checked') || $('#GeneratorPost_submit_to_all').is(':checked')) {
                        if(input<0) {
                            $("#schedule").hide();
                            $("#post_now_btn").hide();
                            $(".error-twitter").show();
                            $(".error-twitter").show();
                        }
                    }else{
                        $("#schedule").show();
                        $("#post_now_btn").show();
                        $(".error-twitter").hide();
                        $(".error-twitter").hide();
                    }
                }

            );
        });
        var input=0;
        $type = $("#ytGeneratorPost_twitter_type");
        var post = $("#GeneratorPost_twitter_post").val();
        var num = 140;
        $.get('<?PHP echo CController::createUrl('getTwitterSize') ?>', {text: post,type:$("#ytGeneratorPost_twitter_type").val()}, function (data) {
                input = num - data;
                console.log(input);
                $("#twitter_counter").text(input);
                if (input > 0) {
                    $("#schedule").show();
                    $("#post_now_btn").show();
                    $(".error-twitter").hide();
                    $(".error-twitter").hide();
                }
                if($('#GeneratorPost_platforms_1').is(':checked') || $('#GeneratorPost_submit_to_all').is(':checked')) {
                    if(input<0) {
                        $("#schedule").hide();
                        $("#post_now_btn").hide();
                        $(".error-twitter").show();
                        $(".error-twitter").show();
                    }
                }else{
                    $("#schedule").show();
                    $("#post_now_btn").show();
                    $(".error-twitter").hide();
                    $(".error-twitter").hide();
                }
            }

        );
        $('#GeneratorPost_twitter_post').keyup(function (e) {
            var input=0;
            $type = $("#ytGeneratorPost_twitter_type");
            var post = $("#GeneratorPost_twitter_post").val();
            var num = 140;
            $.get('<?PHP echo CController::createUrl('getTwitterSize') ?>', {text: post,type:$("#ytGeneratorPost_twitter_type").val()}, function (data) {
                    input = num - data;
                    console.log(input);
                    $("#twitter_counter").text(input);
                    if (input > 0) {
                        $("#schedule").show();
                        $("#post_now_btn").show();
                        $(".error-twitter").hide();
                        $(".error-twitter").hide();
                    }
                    if($('#GeneratorPost_platforms_1').is(':checked') || $('#GeneratorPost_submit_to_all').is(':checked')) {
                        if(input<0) {
                            $("#schedule").hide();
                            $("#post_now_btn").hide();
                            $(".error-twitter").show();
                            $(".error-twitter").show();
                        }
                    }else{
                        $("#schedule").show();
                        $("#post_now_btn").show();
                        $(".error-twitter").hide();
                        $(".error-twitter").hide();
                    }
                }

            );
        });
        var image_pr = $('#ytGeneratorPost_image');
        var main_image = $('.main_image');

        if(image_pr.val() != ''){

            main_image.append('<img src="'+image_pr.val()+'" class="img-responsive main_image_changer">');
        }
        $('.image_changer').click(function () {
            $('.image_changer').addClass('grayscale');
            $('.main_image_changer').attr('src',$(this).attr('src'));
            $('#ytGeneratorPost_image').val($(this).attr('src'));
            $(this).removeClass('grayscale');

        });

        $("#post_now_btn").click(function(e) {
            $(this).hide();
            $('#schedule').hide();
        });
        $("#schedule").click(function(e) {
            $(this).hide();
            $('#post_now_btn').hide();
        });

        $(window).keydown(function(event){
            if((event.which== 13) && event.target.id!='GeneratorPost_facebook_post' && event.target.id!='GeneratorPost_instagram_post' && event.target.id != 'GeneratorPost_twitter_post') {
                event.preventDefault();
                return false;
            }
        });
        var pasted = false;
        $('#GeneratorPost_link').change(function(){
            //loading
            $('#hidden_status').val(1);

                $('#loading2').css('display', 'block');
                $('#generator-post-form').submit();

        });
        $('#GeneratorPost_link').on('paste', function () {
            $('#hidden_status').val(1);
            setTimeout(function () {
                    $('#loading2').css('display', 'block');
                    $('#generator-post-form').submit();

            }, 100);
        });

        $("#generator-post-form").submit(function(e){

            if(confirmed && $('#hidden_status').val()==1) {
                e.preventDefault();
                var form = $(this);
                var wWidth = $(window).width();
                var dWidth = wWidth * 0.6;
                var wHeight = $(window).height();
                var dHeight = wHeight * 0.5;
                check_for_link = $.trim($("#GeneratorPost_link").val());
                $.post('<?PHP echo CController::createUrl('checkLink') ?>', {link: check_for_link}, function (data_check) {
                    if (data_check != '') {
                        $('.link_exist_error_dialog').html(data_check);

                        $('.link_exist_error_dialog').dialog({
                            modal:true,
                            resizable: false,width: dWidth,height:'auto',title:'This news link already exist are you sure you want to continue ?',
                            position: { my: "center", at: "top+30%", of: window},
                            buttons: {

                                "Accept": {
                                    'text': 'Accept',
                                    click: function () {
                                        jQuery(this).dialog("close");
                                        confirmed = false;
                                        form.submit();
                                    },'class':'btn btn-primary  btn-flat'},
                                "Cancel":{

                                    'text':'Cancel',
                                    click:function(event) {

                                    jQuery(this).dialog("close");
                                    event.preventDefault();
                                    $('#loading2').css('display', 'none');
                                    if(previous_url.length >0){
                                        $('#GeneratorPost_link').val(previous_url);
                                    }else{

                                    $('#GeneratorPost_link').val('');
                                    }
                                },'class':'btn btn-default  btn-flat'}

                                }

                        });
                    }else{
                        confirmed = false;
                        form.submit();
                    }
                    $.each($('.facebook_count'),function(key,val){

                        if($('.facebook_count').length-1 != key){
                            $(this).slice(key).remove();
                        }
                    });
                    $.each($('.twitter_count'),function(key,val){

                        if($('.twitter_count').length-1 != key){
                            $(this).slice(key).remove();
                        }
                    }); $.each($('.instagram_count'),function(key,val){

                        if($('.instagram_count').length-1 != key){
                            $(this).slice(key).remove();
                        }
                    });


                });
            }

        });
        $("#GeneratorPost_submit_to_all").click(function () {
            if ($(this).is(':checked')) {
                $('.Platforms').prop('disabled', true);
            } else {
                $('.Platforms').prop('disabled', false);
            }
        });
        $('#GeneratorPost_time').datetimepicker({
            format: 'Y-m-d H:i', step: 5,
            minDate: new Date('Y-m-d H:i')
        });
        /*$url = $('#GeneratorPost_link');
         $link = $('#GeneratorPost_link');*/

    });

</script>


<section class="content">

    <div class="box box-info">
        <div class="box-header with-border">
            <div class="col-sm-9"></div>
            <div class="col-sm-3" style="text-align: left;">
                <?php echo Yii::app()->params['statement']['previousPage']; ?>
            </div>
        </div>
        <div class="box-body">

                <div class="link_exist_error col-sm-12" style="color:black;">

                </div>
            <div class="link_exist_error_dialog col-sm-12" style="color:black;">

                </div>
            <?php
            echo CHtml::hiddenField('hidden_status',0,array('id'=>'hidden_status'));
            ?>

            <?php echo TbHtml::link('Non thematic',array('/postQueue/create'),array('id'=>'btn_12','class'=>'btn btn-active'))?>
            <?php echo TbHtml::button('Thematic',array('/thematic_post'),array('class'=>'btn btn-active'))?>
            <hr style="margin-top: 20px">


            <?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
                'id' => 'generator-post-form',
                'enableAjaxValidation' => true,
                'type' => 'horizontal',
                'htmlOptions' => array('enctype' => 'multipart/form-data'),
            ));
            echo CHtml::hiddenField('GeneratorPost[image]', $model->image, array('name' => '', 'id' => 'ytGeneratorPost_image'));
            echo CHtml::hiddenField('GeneratorPost[preview]', $model->preview, array('name' => '', 'id' => 'ytGeneratorPost_preview'));
            echo CHtml::hiddenField('GeneratorPost[submits]', $model->submits, array('name' => '', 'id' => 'ytGeneratorPost_submits'));
            echo CHtml::hiddenField('GeneratorPost[title]', $model->title, array('name' => '', 'id' => 'ytGeneratorPost_title'));
            echo CHtml::hiddenField('GeneratorPost[description]', $model->description, array('name' => '', 'id' => 'ytGeneratorPost_description'));
            echo CHtml::hiddenField('GeneratorPost[category_id]', $model->category_id, array('name' => '', 'id' => 'ytGeneratorPost_category_id'));
            echo CHtml::hiddenField('GeneratorPost[sub_category_id]', $model->sub_category_id, array('name' => '', 'id' => 'ytGeneratorPost_sub_category_id'));
            echo CHtml::hiddenField('GeneratorPost[column]', $model->column, array('name' => '', 'id' => 'ytGeneratorPost_column'));
            echo CHtml::hiddenField('GeneratorPost[creator]', $model->creator, array('name' => '', 'id' => 'ytGeneratorPost_creator'));
            echo CHtml::hiddenField('GeneratorPost[link_md5]', $model->link_md5, array('name' => '', 'id' => 'ytGeneratorPost_link_md5'));
            echo CHtml::hiddenField('GeneratorPost[shorten_url]', $model->shorten_url, array('name' => '', 'id' => 'ytGeneratorPost_shorten_url'));
            echo CHtml::hiddenField('GeneratorPost[facebook_type]', $model->facebook_type, array('name' => '', 'id' => 'ytGeneratorPost_facebook_type'));
            echo CHtml::hiddenField('GeneratorPost[instagram_type]', $model->instagram_type, array('name' => '', 'id' => 'ytGeneratorPost_instagram_type'));
            echo CHtml::hiddenField('GeneratorPost[twitter_type]', $model->twitter_type, array('name' => '', 'id' => 'ytGeneratorPost_twitter_type'));
            echo CHtml::hiddenField('GeneratorPost[new_category]', $model->new_category, array('name' => '', 'id' => 'new_category'));
            echo CHtml::hiddenField('GeneratorPost[facebook_temp_id]', $model->facebook_temp_id, array('name' => '', 'id' => 'new_category'));
            echo CHtml::hiddenField('GeneratorPost[twitter_temp_id]', $model->twitter_temp_id, array('name' => '', 'id' => 'new_category'));
            echo CHtml::hiddenField('GeneratorPost[instagram_temp_id]', $model->instagram_temp_id, array('name' => '', 'id' => 'new_category'));
            if(isset($model->gallary))
            foreach ($model->gallary as $gall)
             echo CHtml::hiddenField('GeneratorPost[gallary][]', $gall, array('name' => '', 'id' => 'gallary'));

            ?>

            <div class="col-sm-5 ">
                <?php
                echo $form->urlFieldGroup($model, 'link',
                    array(
                        'labelOptions' => array('class' => 'col-sm-2'),
                        'wrapperHtmlOptions' => array('class' => 'col-sm-10',)
                    ));
                ?>
                <hr>
                <?php echo $form->textFieldGroup($model, 'time',
                    array(
                        'labelOptions' => array(
                            'class' => 'col-sm-2'
                        ),
                        'wrapperHtmlOptions' => array(
                            'class' => 'col-sm-10',
                        ),
                        //  'append' => $form->checkboxGroup($model,'now',array('groupOptions'=>array('style'=>'    margin: -4px;'))),
                        'groupOptions' => array(),
                    )
                );
                ?>
                <hr>
                <?php
                echo $form->checkboxGroup($model, 'pinned', array(
                    'label' => null,
                    'Class_span' => 'col-sm-0',
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-4 col-sm-push-2',
                    )
                ));
                ?>
                <hr>

                <div id="platforms">
                    <?php
                    $model->platforms = array('Facebook' => true, 'Twitter' => true, 'Instagram' => true);

                    echo $form->checkboxListGroup($model, 'platforms', array(
                        'widgetOptions' => array(
                            'data' => CHtml::listData(Platform::model()->findAll('deleted=0'), 'id', 'title'),
                            'htmlOptions' => array(
                                'class' => 'Platforms',

                            ),
                        ),
                        'labelOptions' => array(
                            'class' => 'col-sm-2 platforms'
                        ),
                        'wrapperHtmlOptions' => array(
                            'class' => 'col-sm-8 platforms',
                        ),
                    ));
                    ?>
                </div>
                <div class="col-sm-12 massage" style="display: none">
                    <div class="alert alert-warning">
                        please wait a moment
                    </div>
                </div>
                <div class="form-actions  pull-right">
                    <div class="error-twitter" style="display: none;">
                        <div class="alert alert-warning">
                            <h4>Please check twitter characters count</h4>
                        </div>
                    </div>

                    <?php $this->widget(
                        'booster.widgets.TbButton',
                        array(
                            'buttonType' => 'submit',
                            'context' => 'success',
                            'label' => 'Post now',
                            'htmlOptions' => array('style' => '    margin-right: 12px;','id'=>'post_now_btn','name'=>'post_now_btn')
                        )
                    );
                    ?>
                    <?php

                    if($model->schedule){
                        $this->widget(
                            'booster.widgets.TbButton',
                            array(
                                'buttonType' => 'submit',
                                'context' => 'primary',
                                'label' => 'schedule',

                                'htmlOptions' => array('style' => '', 'id'=>'schedule','name'=>'schedule_btn')
                            )
                        );
                    }
                    else{
                        $this->widget(
                            'booster.widgets.TbButton',
                            array(
                                'buttonType' => 'submit',
                                'context' => 'primary',
                                'label' => 'schedule',

                                'htmlOptions' => array('style' => '', 'id'=>'schedule','name'=>'schedule_btn',   'disabled'=>'disabled',)
                            )
                        );
                    }
                    ?>

                </div>

            </div>
            <div class="col-sm-7 col-xs-12">
                <div class="row">
                    <div id="link-img">
                        <div id="loading2" style="display: none;margin:0 auto;" align="center">
                            <h3 class="alert alert" style="background-color: #ddd">
                                <b>Please wait a moment ...</b><br>
                                The preview might take  some time to process ,<br>
                                You may still push the update anyways.

                            </h3>
                            <p><img src="<?php echo Yii::app()->baseUrl . "/image/updateimg.gif" ?>" class="loading-imge"/></p>
                        </div>
                    </div>
                    <div class="col-sm-12 main_image"></div>
                </div>
                <?php echo $form->textAreaGroup($model, 'facebook_post',
                    array(
                        'labelOptions' => array(
                            'class' => 'col-sm-2'
                        ),
                        'wrapperHtmlOptions' => array(
                            'class' => 'col-sm-10',
                        ),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                                'style'=>'height: 110px;',
                                'placeholder'=>'Facebook',
                                'class'=>'arabic-direction'
                                //'readonly'=>'readonly',
                            )
                        )
                    )
                );
                ?>
                <hr>
                <?php echo $form->textAreaGroup($model, 'instagram_post',
                    array(
                        'labelOptions' => array(
                            'class' => 'col-sm-2'
                        ),
                        'wrapperHtmlOptions' => array(
                            'class' => 'col-sm-10',
                        ),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array('style'=>'height: 110px;',

                                'placeholder'=>'Instagram',
                                'class'=>'arabic-direction'

                                // 'readonly'=>'readonly',
                            )
                        )
                    )
                );
                ?>
                <hr>
                <div class="col-xs-12 col-sm-push-2" id="twitter" style="/*display:none;*/">
                    Number of Characters:
                    <small id="twitter_counter" style="color:red;font-size: 18px">140</small>

                </div>
                <?php echo $form->textAreaGroup($model, 'twitter_post',
                    array(
                        'labelOptions' => array(
                            'class' => 'col-sm-2'
                        ),
                        'wrapperHtmlOptions' => array(
                            'class' => 'col-sm-10',

                        ),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                                'style'=>'height: 110px;',
                                'placeholder'=>'Twitter',
                                'class'=>'arabic-direction'

                                // 'readonly'=>'readonly',

                            )
                        )
                    )
                );
                ?>
            </div>
            <?php $this->endWidget(); ?>

            <div class="row">
                <div class="col-xs-12 title" style="direction: rtl">
                    <h3><b>Title : </b><small style="color:black;">
                            <?php echo $model->title?>
                        </small></h3>
                </div>
                <div class="col-xs-12 description" style="direction: rtl">
                    <h4><b>Description :</b> <small style="color:black;">
                            <?php echo $model->description?>
                        </small></h4>
                </div>
            </div>
            <div class="col-xs-12 images">
                <?php
                if(!empty($model->gallary))
                    foreach ($model->gallary as $item): ?>
                        <div class="col-sm-3">
                            <img src="<?php echo $item?>" class="img-responsive img-thumbnail grayscale image_changer"/>
                        </div>
                        <?php endforeach; ?>
            </div>
        </div>

    </div>
    </div>
</section>




