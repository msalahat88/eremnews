<?php
class TPHomeController extends ThematicPostBaseControllers
{
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('index','previewPost','getTwitterSize','checkLink'),
                'users'=>array(Yii::app()->user->getState('type')),
                array('deny',  // deny all users
                    'users'=>array('*'),
                ),
            ),

        );
    }


    public $gallery_data =null;

    public function news($url){

        $link = Yii::app()->params['feedUrl'];

        if(!isset($url) && empty($url))
            exit();

        $check_link = strpos(' '.$url,$link);

        if($check_link){
            $this->category = explode($link,$url);

            if(is_array($this->category) && isset($this->category[1]) && !empty($this->category[1])){

                $split = explode('/',$this->category[1]);

                if(is_array($split) && isset($split[1]) && !empty($split[1])) {
                    if(trim($split[1]) == trim('category') or empty($split[2])) {
                        $this->model->addError('link', 'Please insert a news link');
                        return false;
                    }
                    if(isset($split[3]) && !empty($split[3]))
                        $this->category = Category::model()->findByAttributes(array('url' => trim(Yii::app()->params['feedUrl'] . '/category/news/arab-word/' . $split[3])));
                    else
                        $this->category = Category::model()->findByAttributes(array('url' => trim(Yii::app()->params['feedUrl'] . '/category/' . $split[1])));
                    if(empty($this->category))
                        $this->category = Category::model()->findByAttributes(array('url' => trim(Yii::app()->params['feedUrl'] . '/category/' . $split[1])));
                    if(empty($this->category))
                        $this->category = Category::model()->findByAttributes(array('url' => trim(Yii::app()->params['feedUrl'] . '/' . $split[1])));
                }
                elseif(empty($split[1])){
                    $this->model->addError('link', 'Please insert a news link');
                    return false;
                }else{
                    $this->category = null;
                }



            }else{
                $this->sub_category = null;
                $this->category = null;
            }


            $info = array();
            if(isset($this->category->id)){
                $this->category_id = $this->category->id;
                $info['category_id'] = $this->category_id;
            }else{
                $this->new_category=$split[1];
                $info['new_category'] = $this->new_category;
            }

            $info = array();
            if(isset($this->category->id)){
                $this->category_id = $this->category->id;
                $info['category_id'] = $this->category_id;
            }else{
                $this->new_category=$split[1];
                $info['new_category'] = $this->new_category;
            }
            if(isset($this->sub_category->id)){
                $this->sub_category_id = $this->sub_category->id;
                $info['sub_category_id'] = $this->sub_category_id;
            }

            $data_info = $this->get_info_feature($url,$this->category_id);

            if(is_array($data_info)){

                $info = array_merge($info,$this->get_info_feature($url,$this->category_id));

                $this->shorten_url =$this->GoogleShort(urldecode($info['link']));
                $info['shorten_url'] = $this->shorten_url;
                $info['link_md5'] = md5(urldecode($info['link']));

                return $this->generator_info($info);
            }

            return false;


        }

        return false;

    }

    public function actionIndex()
    {

        $this->breadcrumbs=array('Thematic');

        $this->setPageTitle(Yii::app()->name.' - Thematic');

        $this->model=new GeneratorPost();

        $this->performAjaxValidation();
        if(isset($_POST['thematic_post_rendered'])){
            $_POST['GeneratorPost']['link'] = trim($_POST['thematic_post_rendered']);
            $_POST['GeneratorPost']['submits'] =0;
        }
        if(isset($_POST['GeneratorPost']['link']) && !empty($_POST['GeneratorPost']['link']))

            if (isset($_POST['post_now_btn']) or isset($_POST['schedule_btn']))

                $_POST['GeneratorPost']['submits'] = 1;


        if(isset($_POST['GeneratorPost']['submits']) && $_POST['GeneratorPost']['submits'] == 0 ){

            if(isset($_POST['GeneratorPost']['link']) && !empty($_POST['GeneratorPost']['link']))
            {

                $this->preview_date();
                $this->model->link = $_POST['GeneratorPost']['link'];
            }

            else
                $this->model->addError('link','Link is not valid');

        }else{


            if(isset($_POST['GeneratorPost']))
            {

                $this->model->attributes  =$_POST['GeneratorPost'];


                $data = $_POST['GeneratorPost'];

                if(!empty($data['link'])){

                    if(isset($_POST['post_now_btn'])){

                        $data['time'] = date('Y-m-d H:i:s');




                        $this->add_new_data_post_now($data);


                    }elseif(isset($_POST['schedule_btn'])){

                        $this->add_new_data($data);

                    }

                }
                else
                    $this->model->addError('link','Link cannot be blank.');
            }
        }

        $this->render('index',array(
            'model'=>$this->model,
        ));
    }


    public function preview_date(){

        $data = $this->news(trim($_POST['GeneratorPost']['link']));


        if($data == false){
            $this->model->addError('link','Please check the link you used.');
            return false;
        }


        $this->model->category_id = isset($data['info']['category_id'])?$data['info']['category_id']:null;
        $this->model->schedule = 1;
        $this->model->new_category = isset($this->new_category)?$this->new_category:null;
        if(!empty($_POST['GeneratorPost']['time']))
        $this->model->time = $_POST['GeneratorPost']['time'];

        foreach ($data as $index => $item) {

            if($index != 'info'){

                foreach ($item as $index_items =>$items) :

                    if($index_items !='platform'){
                        $val = $index.'_'.$index_items;
                        $this->model->$val = $items;
                    }else
                        $this->model->platforms[]= $items;


                endforeach;

            }else{

                foreach ($item as $index_info => $item_info):

                    if($index_info == 'image')
                        $this->model->image = $item_info['src'];
                    elseif($index_info == 'gallary') {
                        $this->model->gallary = $item_info['src'];
                    }
                    else
                        $this->model->$index_info = $item_info;

                endforeach;

            }
        }
    }
    public function actionCheckLink(){
        $array_platforms=array();

        $count_Facebook=1;
        $count_Twitter=1;
        $count_Instagram=1;
        if(isset($_POST['link']) and !empty($_POST['link'])) {
            $link = $_POST['link'];
            $link = explode('?utm_source',$link);
            if (isset($link[0]) and !empty($link[0])) {
                $check_for_link = News::model()->findAllByAttributes(array('link_md5' => md5($link[0])),array('order'=>'id desc'));
                if (!empty($check_for_link)) {
                    foreach($check_for_link as $news_link) {
                        if (!empty($news_link->posts)) {
                            echo '<div class="col-sm-12">';


                            foreach ($news_link->posts as $post_queue_links) {

                                if(!in_array($post_queue_links->platforms->title, $array_platforms)) {
                                    echo '<div class="col-sm-4">';
                                    array_push($array_platforms, $post_queue_links->platforms->title);
                                    echo '<div class="col-sm-12"><h4 style="color:black;text-align: center;">' . $post_queue_links->platforms->title . '</h4></div>';
                                    if ($post_queue_links->is_posted == 0 and $post_queue_links->is_scheduled == 1) {
                                        echo '<div class="col-sm-12"><h5 style="color:black;text-align: center;">this post will be posted at ' . $post_queue_links->schedule_date . ' ' . CHtml::link('Go to post', 'postQueue/view/' . $post_queue_links->id, array('target' => '_blank', 'style' => 'color:#3c8dbc')) . '</h5></div>';
                                    } elseif ($post_queue_links->is_scheduled == 0) {
                                        echo '<div class="col-sm-12"><h5 style="color:black;text-align: center;">this post is in the unscheduled posts ' . ' ' . CHtml::link('Go to post', 'postQueue/view/' . $post_queue_links->id, array('target' => '_blank', 'style' => 'color:#3c8dbc')) . '</h5></div>';
                                    } elseif ($post_queue_links->is_posted == 1) {
                                        $page = $news_link->category->page_index;
                                        if ($page == 'other') {
                                            $page = 'Sport';
                                        }

                                        echo '<div class="col-sm-12"><h5 style="color:black;text-align: center;">This post is already posted at ' . $post_queue_links->schedule_date . '  to ' . $page . ' page' . CHtml::link('Go to post', 'postQueue/view/' . $post_queue_links->id, array('target' => '_blank', 'style' => 'color:#3c8dbc')) . '</h5></div>';
                                    } else {
                                        echo '<div class="col-sm-12"><h5 style="color:black;text-align: center;">This post is already posted at ' . $post_queue_links->schedule_date . ' ' . CHtml::link('Go to post', 'postQueue/view/' . $post_queue_links->id, array('target' => '_blank', 'style' => 'color:#3c8dbc')) . '</h5></div>';
                                    }
                                echo '</div>';
                                }else{
                                   /* if($post_queue_links->platforms->title == 'Facebook'){
                                        $count_Facebook++;
                                    } if($post_queue_links->platforms->title == 'Twitter'){
                                        $count_Twitter++;
                                    } if($post_queue_links->platforms->title == 'Instagram'){
                                        $count_Instagram++;
                                    }*/
                                }

                                    /*if ($post_queue_links->platforms->title == 'Facebook' and $count_Facebook != 1) {
                                        echo '<div class="col-sm-12 facebook_count facebook_count_items"><h5 style="color:black;text-align: center;"> already exist ' . $count_Facebook . ' times</h5></div>';
                                    }
                                    if ($post_queue_links->platforms->title == 'Twitter' and $count_Twitter != 1) {
                                        echo '<div class="col-sm-12 twitter_count twitter_count_items"><h5 style="color:black;text-align: center;"> already exist ' . $count_Twitter . ' times</h5></div>';
                                    }
                                    if ($post_queue_links->platforms->title == 'Instagram' and $count_Instagram != 1) {
                                        echo '<div class="col-sm-12 instagram_count insatagram_count_items"><h5 style="color:black;text-align: center;"> already exist ' . $count_Instagram . ' times</h5></div>';
                                    }*/

                            }
                            echo '</div>';
                        }
                    }
                }
            }
        }elseif(isset($_POST['submit_link']) and !empty($_POST['submit_link'])){
            $link = $_POST['submit_link'];
            $check_for_link = News::model()->findByAttributes(array('link_md5' => md5($link)));
            if(!empty($check_for_link)){
                if(!empty($check_for_link->posts)) {
                    echo 'link already exist';
                }
            }
        }
    }

    public function add_new_data($data){

        if(isset($data['platforms']) and !empty($data['platforms'])) {
            foreach ($data['platforms'] as $item) {
                $byAttribute = Platform::model()->findByPk($item);
                $text = null;
                $type = null;
                if ($byAttribute->title == 'Twitter') {
                    if ($this->getTweetLength($data['twitter_post'], $data['twitter_type'] == 'Image' ? true : false, $data['twitter_type'] == 'Video' ? true : false) > 141) {
                        $this->preview_date();
                        $this->model->addError('twitter_post', 'twitter size must be less than 140 chars long');
                        return false;
                    }
                }
            }
        }else{
            foreach ($data as $index => $values) {
                $this->model->$index = $values;
            }
            $this->model->addError('platforms', 'Platforms cannot be blank');
            return false;
        }
        if(isset($data['link']) and !empty($data['link'])){
            $link = $data['link'];
            $link = explode('?utm_source',$link);
            $data['link'] = $link[0];
        }

        $news = new News();

        $news->id = null;
        $news->title = $this->clear_tags(isset($data['title'])?$data['title']:null);
        $news->link = isset($data['link'])?$data['link']:null;
        $news->description = $this->clear_tags(isset($data['description'])?$data['description']:null);

        if (isset($data['category_id']) and !empty($data['category_id'])) {
            $news->category_id = $data['category_id'];

        } else
            if (isset($data['new_category'])) {
                $new_category = new Category();
                $new_category->id = null;
                $new_category->title = $data['new_category'];
                $new_category->url = Yii::app()->params['feedUrl'] . '/' . $data['new_category'];
                $new_category->active = 0;
                $new_category->page_index='main';
                $new_category->created_at = date('Y-m-d H:i:s');
                $new_category->setIsNewRecord(true);
                if ($new_category->save())
                    $news->category_id = $new_category->id;

            } else {
                if (isset($data['category_id'])) {
                    if (empty(Category::model()->findByPk($data['category_id'])))
                        $data['category_id'] = 15;
                }
            }        $news->sub_category_id = isset($data['sub_category_id'])?$data['sub_category_id']:null;
        $news->creator = $this->clear_tags(isset($data['creator'])?$data['creator']:null);
        $news->column = $this->clear_tags(isset($data['column'])?$data['column']:$news->creator);
        $news->link_md5 = isset($data['link_md5'])?$data['link_md5']:null;
        $news->shorten_url =isset( $data['shorten_url'])? $data['shorten_url']:null;
        $news->publishing_date = empty($data['time'])?date('Y-m-d H:i:s'):$data['time'];
        $news->schedule_date =  empty($data['time'])?date('Y-m-d H:i:s'):$data['time'];
        $news->generated = 0;
        $news->setIsNewRecord(true);
        $news->created_at = date('Y-m-d H:i:s');

        $image = isset($data['image'])?$data['image']:null;

        /*if(empty($image)){
            if(isset($info['info']['gallary']['src'])){
                $image = key($info['info']['gallary']['src']);
            }
        }*/


        if($news->save(false)){

            $media = new MediaNews();
            $media->id = null;
            $media->media = $image;
            $media->news_id = $news->id ;
            $media->type ='image';
            $media->setIsNewRecord(true);
            $media->save(false);
            if(isset($data['gallary'])){
                foreach($data['gallary'] as $gallery){
                    if($image != $gallery) {
                        $media = new MediaNews();
                        $media->id = null;
                        $media->media = $gallery;
                        $media->news_id = $news->id;
                        $media->type ='gallery';
                        $media->setIsNewRecord(true);
                        $media->save(false);
                    }
                }
            }
        }

        $new_shorten = $data['link'];


            if(isset($data['platforms']))
                foreach ($data['platforms'] as $item) {
                    $byAttribute = Platform::model()->findByPk($item);
                    $text = null;
                    $type = null;
                    if($byAttribute->title == 'Facebook') {
                        $text = $data['facebook_post'];
                        $type = $data['facebook_type'];
                        $template_id = $data['facebook_temp_id'];
                        $new_shorten = $data['link'].'?utm_source=facebook&utm_medium=referral&utm_campaign=sortechs';

                    }

                    if($byAttribute->title == 'Twitter') {
                        $type = $data['twitter_type'];
                        $text = $data['twitter_post'];
                        $template_id = $data['twitter_temp_id'];
                        $new_shorten =$data['link'].'?utm_source=twitter&utm_medium=referral&utm_campaign=sortechs';

                    }

                    if($byAttribute->title == 'Instagram') {
                        $text = $data['instagram_post'];
                        $type = $data['instagram_type'];
                        $template_id = $data['instagram_temp_id'];
                        $new_shorten =$data['link'].'?utm_source=instagram&utm_medium=referral&utm_campaign=sortechs';

                    }
                    $new_shorten =$this->GoogleShort($new_shorten);

                    $this->PostQueue = new PostQueue();
                    $this->PostQueue->id= null;
                    $this->PostQueue->setIsNewRecord(true);
                    $this->PostQueue->post = $text;
                    $this->PostQueue->type = $type;
                    $this->PostQueue->schedule_date = empty($news->schedule_date)?date('Y-m-d H:i:s'):$news->schedule_date;
                    $this->PostQueue->catgory_id =  $news->category_id;
                    $this->PostQueue->main_category_id =  null;
                    $this->PostQueue->link = $new_shorten;
                    $this->PostQueue->is_posted = 0;
                    $this->PostQueue->news_id = $news->id;
                    $this->PostQueue->post_id =null;
                    $this->PostQueue->media_url =$image;
                    $this->PostQueue->settings ='custom';
                    $this->PostQueue->is_scheduled =1;
                    $this->PostQueue->template_id =$template_id;
                    $this->PostQueue->platform_id =$byAttribute->id;
                    $this->PostQueue->generated ='thematic';
                    $this->PostQueue->pinned =$data['pinned'];
                    $this->PostQueue->created_at =date('Y-m-d H:i:s');

                    if($this->parent == null){
                        $this->PostQueue->parent_id =null;
                        if($this->PostQueue->save())
                            $this->parent = $this->PostQueue->id;

                    }else{
                        $this->PostQueue->parent_id =$this->parent;
                        $this->PostQueue->save();
                    }
                }


        $news->generated = 1;
        $news->save(false);
        if(!empty($this->parent))
            $this->redirect(array('/postQueue/main'));
    }


    public function add_new_data_post_now($data){
        if(!empty($data['link'])) {
            $info = $this->news($data['link']);
            if(empty($info)){
                $this->model->addError('link', 'Please insert a valid link');
                return false;
            }

            if (isset($_POST['post_now_btn']))
                if(isset($data['platforms']) and !empty($data['platforms'])) {
                    foreach ($data['platforms'] as $item) {
                        $byAttribute = Platform::model()->findByPk($item);
                        $text = null;
                        $type = null;
                        if (empty($data['twitter_post'])) {
                            if ($byAttribute->title == 'Twitter') {
                                if ($this->getTweetLength($info['twitter']['post'], $info['twitter']['type'] == 'Image' ? true : false, $info['twitter']['type'] == 'Video' ? true : false) > 141) {
                                    $this->preview_date();


                                    $this->model->addError('twitter_post', 'twitter size must be less than 140 chars long');
                                    return false;
                                }
                            }
                        } else {
                            if ($byAttribute->title == 'Twitter') {
                                if ($this->getTweetLength($data['twitter_post'], $data['twitter_type'] == 'Image' ? true : false, $data['twitter_type'] == 'Video' ? true : false) > 141) {
                                    foreach ($data as $index => $values) {
                                        $this->model->$index = $values;
                                    }
                                    $this->model->addError('twitter_post', 'twitter size must be less than 140 chars long');
                                    return false;
                                }
                            }
                        }
                    }
                }else{
                        foreach ($data as $index => $values) {
                            $this->model->$index = $values;
                        }
                        $this->model->addError('platforms', 'Platforms cannot be blank');
                        return false;

                }

            if(isset($info['info']['link']) and !empty($info['info']['link'])){
                $link = $info['info']['link'];
                $link = explode('?utm_source',$link);
                $info['info']['link'] = $link[0];
            }

            $news = new News();
            $news->id = null;
            $news->title = isset($info['info']['title']) ? $info['info']['title'] : null;
            $news->link = isset($info['info']['link']) ? $info['info']['link'] : null;
            $news->description = isset($info['info']['description']) ? $info['info']['description'] : null;
            if (isset($info['info']['category_id'])) {
                $news->category_id = $info['info']['category_id'];

            } elseif (isset($info['info']['new_category'])) {
                $new_category = new Category();
                $new_category->id = null;
                $new_category->title = $info['info']['new_category'];
                $new_category->url = Yii::app()->params['feedUrl'] . '/' . $info['info']['new_category'];
                $new_category->active = 0;
                $new_category->created_at = date('Y-m-d H:i:s');
                $new_category->setIsNewRecord(true);
                if ($new_category->save())
                    $news->category_id = $new_category->id;

            } else {
                if (empty(Category::model()->findByPk($info['info']['category_id'])))
                    $info['info']['category_id'] = 15;
                $news->category_id = $info['info']['category_id'];
            }            $news->sub_category_id = isset($info['info']['sub_category_id']) ? $info['info']['sub_category_id'] : null;
            $news->creator = isset($info['info']['creator']) ? $info['info']['creator'] : null;
            $news->column = isset($info['info']['column']) ? $info['info']['column'] : $news->creator;
            $news->link_md5 = isset($info['info']['link_md5']) ? $info['info']['link_md5'] : null;
            $news->shorten_url = isset($info['info']['shorten_url']) ? $info['info']['shorten_url'] : null;
            $news->publishing_date = empty($data['time']) ? date('Y-m-d H:i:s') : $data['time'];
            $news->schedule_date = empty($data['time']) ? date('Y-m-d H:i:s') : $data['time'];
            $news->generated = 0;
            $news->setIsNewRecord(true);
            $news->created_at = date('Y-m-d H:i:s');

            $image = isset($info['info']['image']['src']) ? $info['info']['image']['src'] : null;

            if (empty($image)) {
                if (isset($info['info']['gallary']['src'])) {
                    $image = key($info['info']['gallary']['src']);
                }
            }
            if(isset($data['image']) and !empty($data['image'])){
                $image=$data['image'];
            }

            if($news->save(false)){
                $media = new MediaNews();
                $media->id = null;
                $media->media = $image;
                $media->news_id = $news->id ;
                $media->type ='image';
                $media->setIsNewRecord(true);
                $media->save(false);
                if(isset($data['gallary'])){
                    foreach($data['gallary'] as $gallery){
                        if($image != $gallery) {
                            $media = new MediaNews();
                            $media->id = null;
                            $media->media = $gallery;
                            $media->news_id = $news->id;
                            $media->type ='gallery';
                            $media->setIsNewRecord(true);
                            $media->save(false);
                        }
                    }
                }
            }


            $new_shorten = $data['link'];

                if (isset($data['platforms']))
                    foreach ($data['platforms'] as $item) {
                        $byAttribute = Platform::model()->findByPk($item);
                        $text = null;
                        $type = null;
                        if ($byAttribute->title == 'Facebook') {
                            $template_id = $info['facebook']['temp_id'];
                            $text = $info['facebook']['post'];

                            if (isset($data['facebook_post']) && !empty($data['facebook_post'])) {
                                $text = $data['facebook_post'];
                            }

                            $type = $info['facebook']['type'];

                            if (isset($data['facebook_type']) && !empty($data['facebook_type'])) {
                                $type = $data['facebook_type'];
                            }
                            $new_shorten = $data['link'].'?utm_source=facebook&utm_medium=referral&utm_campaign=sortechs';

                        }

                        if ($byAttribute->title == 'Twitter') {
                            $template_id = $info['twitter']['temp_id'];
                            $text = $info['twitter']['post'];

                            if (isset($data['twitter_post']) && !empty($data['twitter_post'])) {
                                $text = $data['twitter_post'];
                            }

                            $type = $info['twitter']['type'];

                            if (isset($data['twitter_type']) && !empty($data['twitter_type'])) {
                                $type = $data['twitter_type'];
                            }
                            $new_shorten =$data['link'].'?utm_source=twitter&utm_medium=referral&utm_campaign=sortechs';
                        }

                        if ($byAttribute->title == 'Instagram') {
                            $template_id = $info['instagram']['temp_id'];

                            $text = $info['instagram']['post'];

                            if (isset($data['instagram_post']) && !empty($data['instagram_post'])) {
                                $text = $data['instagram_post'];
                            }

                            $type = $info['instagram']['type'];

                            if (isset($data['instagram_type']) && !empty($data['instagram_type'])) {
                                $type = $data['instagram_type'];
                            }
                            $new_shorten =$data['link'].'?utm_source=instagram&utm_medium=referral&utm_campaign=sortechs';

                        }
                        $new_shorten =$this->GoogleShort($new_shorten);


                        $this->PostQueue = new PostQueue();
                        $this->PostQueue->id = null;
                        $this->PostQueue->setIsNewRecord(true);
                        $this->PostQueue->post = $text;
                        $this->PostQueue->type = $type;
                        $this->PostQueue->schedule_date = empty($news->schedule_date) ? date('Y-m-d H:i:s') : $news->schedule_date;
                        $this->PostQueue->catgory_id = $news->category_id;
                        $this->PostQueue->main_category_id = null;
                        $this->PostQueue->link = $new_shorten;
                        $this->PostQueue->is_posted = 0;
                        $this->PostQueue->news_id = $news->id;
                        $this->PostQueue->post_id = null;
                        $this->PostQueue->media_url = $image;
                        $this->PostQueue->settings = 'custom';
                        $this->PostQueue->template_id =$template_id;
                        $this->PostQueue->is_scheduled = 1;
                        $this->PostQueue->platform_id = $byAttribute->id;
                        $this->PostQueue->generated = 'thematic';
                        $this->PostQueue->pinned = $data['pinned'];
                        $this->PostQueue->created_at = date('Y-m-d H:i:s');

                        if ($this->parent == null) {
                            $this->PostQueue->parent_id = null;
                            if ($this->PostQueue->save())
                                $this->parent = $this->PostQueue->id;

                        } else {
                            $this->PostQueue->parent_id = $this->parent;
                            $this->PostQueue->save();
                        }
                    }


            $news->generated = 1;
            $news->save(false);
            if (!empty($this->parent))
                $this->redirect(array('/postQueue/main'));
        }

        else
            $this->model->addError('link','Link cannot be blank.');
    }
    public function actionGetTwitterSize(){
        if(isset($_GET)) {
            echo $this->getTweetLength($_GET['text'], $_GET['type'] == 'Image' ? true : false, $_GET['type'] == 'Video' ? true : false);
        }
    }

    protected function performAjaxValidation()
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='generator-post-form')
        {
            echo CActiveForm::validate($this->model);
            Yii::app()->end();
        }
    }








}