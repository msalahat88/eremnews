<?php

/**
 * This is the model class for table "news".
 *
 * The followings are the available columns in table 'news':
 * @property integer $id
 * @property integer $category_id
 * @property integer $sub_category_id
 * @property string $title
 * @property string $column
 * @property string $link
 * @property string $link_md5
 * @property string $description
 * @property string $body_description
 * @property string $shorten_url
 * @property string $publishing_date
 * @property string $schedule_date
 * @property integer $generated
 * @property string $created_at
 * @property string $creator
 *
 * The followings are the available model relations:
 * @property MediaNews[] $mediaNews
 * @property Category $category
 */
class News extends BaseModels
{
	public $from;
	public $to;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'news';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, title, link, link_md5, publishing_date, schedule_date', 'required'),
			array('category_id, generated', 'numerical', 'integerOnly'=>true),
			array('title, shorten_url', 'length', 'max'=>255),
			array('link_md5', 'length', 'max'=>225),
			array('link_md5', 'unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('created_at,body_description', 'safe'),
			array('id, category_id, title,creator, link, link_md5, description, shorten_url, publishing_date, schedule_date, generated, created_at,from,to,pagination_size', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'mediaNews' => array(self::HAS_MANY, 'MediaNews', 'news_id'),
			'posts' => array(self::HAS_MANY, 'PostQueue', 'news_id'),
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'subCategory' => array(self::BELONGS_TO, 'SubCategories', 'sub_category_id'),
		);
	}
    public function get_all_news_by_date($from,$to){

        return $this->count("`schedule_date` >='$from' and `schedule_date` <='$to'");
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Section',
			'title' => 'Title',
			'link' => 'Link',
			'link_md5' => 'Link Md5',
			'description' => 'Description',
			'creator' => 'creator',
			'sub_category_id' => 'Sub section',
			'shorten_url' => 'Shorten Url',
			'publishing_date' => 'Publishing Date',
			'column' => 'Column',
			'schedule_date' => 'Schedule Date',
			'generated' => 'Generated',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($news = false,$page=false)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		if(isset($this->pagination_size))
			$pages = $this->pagination_size;
		else
			$pages=5;
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('link_md5',$this->link_md5,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('creator',$this->creator,true);
		$criteria->compare('column',$this->column,true);
		$criteria->compare('shorten_url',$this->shorten_url,true);
		$criteria->compare('sub_category_id',$this->sub_category_id,true);

		$criteria->compare('schedule_date',$this->schedule_date,true);
		if($news)
		$criteria->compare('generated',$this->generated);
		else
		$criteria->compare('`generated`',1);

		$criteria->compare('created_at',$this->created_at,true);

if(!empty($this->to) and !empty($this->from))
			$criteria->addBetweenCondition('publishing_date', date('Y-m-d 00:00:00',strtotime($this->from)), date('Y-m-d 23:59:59',strtotime($this->to)), 'AND');
else
	$criteria->compare('publishing_date',$this->publishing_date,true);



		return new CActiveDataProvider($this, array(
			'pagination' => array('pageSize' => $pages),

			'criteria'=>$criteria,
		));


	}
	public function custom_search($addBetweenCondition = false)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.




		$criteria=new CDbCriteria;


		if(!$addBetweenCondition){
			$criteria->compare("schedule_date",$this->schedule_date,true);
		}else{
			$criteria->addBetweenCondition('schedule_date', date('Y-m-d 00:00:00',strtotime($this->from)), date('Y-m-d 23:59:59',strtotime($this->to)), 'AND');
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function get_today_news(){


		$this->command = false;
		$today = date('Y-m-d');
		$yesterday=date('Y-m-d', strtotime(date('Y-m-d').' -1 day'));

		$cdb = new CDbCriteria();

		$cdb->addCondition('schedule_date like "'.$today.'%" or schedule_date like "'.$yesterday.'%" ' );

		return $this->findAll($cdb);
	}


	public function get_all_news(){

		return $this->count();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    public function get_related_text_update($model,$max_desc = false){

        $article=null;
        if(isset($model->news_id)) {
            $textNews = News::model()->find('id =' . $model->news_id);
            if(!empty($textNews->body_description))
                $article=$textNews->body_description;
            elseif(!empty($textNews->description))
            $article = $textNews->description;

            if (isset($article)) {
                echo '<div class=\'col-sm-12\'>';
                if ($max_desc)
                    echo '<h3>Description : <span class="tooltips" style="display: none;">Copied</span></h3><h4 class="arabic-direction"><textarea class="big-line-height" rows="20" cols="100" id="input" readonly>' . trim($article) . '</textarea></h4></div>';

                else
                    echo '<h3>Related description<span class="tooltips" style="display: none;">Copied</span></h3><h5 class="arabic-direction"><textarea class="big-line-height" id="input" style="width:90%;max-width:90%;height:120px;max-height:90%;margin-right:30px;" readonly>' . trim($article) . '</textarea></h5></div>';
            }
        }


    }
    public function total_news_post_query($from,$to){
        $user = Yii::app()->db->createCommand("SELECT count(news.id) as countedid,category.title FROM `news` INNER JOIN category on news.category_id = category.id WHERE `schedule_date` >='$from' and `schedule_date` <='$to'  group by category.title")->queryAll();
        return $user;
    }
}
