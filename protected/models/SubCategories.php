<?php

/**
 * This is the model class for table "sub_categories".
 *
 * The followings are the available columns in table 'sub_categories':
 * @property integer $id
 * @property integer $category_id
 * @property string $title
 * @property string $created_at
 * @property string $url
 * @property string $url_rss
 * @property integer $deleted
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Category $category
 */
class SubCategories extends BaseModels
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sub_categories';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, title, created_at', 'required'),
			array('category_id, deleted', 'numerical', 'integerOnly'=>true),
			array('title,url', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id,url,url_rss, category_id, title, created_at,active, deleted,pagination_size', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		$array = array(
			'id' => 'ID',
			'title' => 'Title',
			'url' => 'Url',
			'category_id' => 'Category',
			'is_rss' => 'Is Rss',
			'deleted' => 'Deleted',
			'shorten_url' => 'shorten url',
			'active' => 'Status',
			'created_at' => 'Created At',
		);
		return array_merge($array,$this->Labels());
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($set = false)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		if(isset($this->pagination_size))
			$pages = $this->pagination_size;
		else
			$pages=5;
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('deleted',0);
		$criteria->compare('active',$this->active);

		if($set)
			return new CActiveDataProvider($this, array(
				'pagination' => array('pageSize' =>12),
				'criteria'=>$criteria,
			));

		return new CActiveDataProvider($this, array(
			'pagination' => array('pageSize' => $pages),
			'criteria'=>$criteria,
		));


	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SubCategories the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
