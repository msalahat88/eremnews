<?php

/**
 * This is the model class for table "profile_pic".
 *
 * The followings are the available columns in table 'profile_pic':
 * @property integer $id
 * @property string $title
 * @property string $media_url
 * @property string $schedule_date
 * @property integer $is_posted
 * @property integer $platform_id
 * @property string $created_at
 * @property string $page_index
 *
 * The followings are the available model relations:
 * @property Platform $platform
 */
class ProfilePic extends BaseModels
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'profile_pic';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, media_url, schedule_date, platform_id,page_index', 'required'),
			array('is_posted', 'numerical', 'integerOnly'=>true),
			array('title, media_url, schedule_date, created_at', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, media_url, schedule_date, is_posted, platform_id, created_at,pagination_size,page_index', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'platform' => array(self::BELONGS_TO, 'Platform', 'platform_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		$array= array(
			'id' => 'ID',
			'title' => 'Title',
			'media_url' => 'Upload image',
			'schedule_date' => 'Schedule Date',
			'is_posted' => 'Is Posted',
			'platform_id' => 'Platform',
			'created_at' => 'Created At',
		);
		return array_merge($array,$this->Labels());

	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		if(isset($this->pagination_size))
			$pages = $this->pagination_size;
		else
			$pages=5;
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('media_url',$this->media_url,true);
		$criteria->compare('schedule_date',$this->schedule_date,true);
		$criteria->compare('is_posted',$this->is_posted);
		$criteria->compare('platform_id',$this->platform_id);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'pagination' => array('pageSize' => $pages),
			'criteria'=>$criteria,
		));
	}

	public function get_profile_pic_facebook(){

		$platform = Platform::model()->findByAttributes(array(
			'title'=>'Facebook',
		));

		$current_date= date('Y-m-d H:i');

		$this->command = false;
		$cdb= new CDbCriteria();
		$cdb->condition = "platform_id = $platform->id and is_posted=0 and schedule_date<='$current_date'";

		return ProfilePic::model()->find($cdb);

	}

	public function get_profile_pic_twitter(){


		$platform = Platform::model()->findByAttributes(array(
			'title'=>'Twitter',
		));

		$current_date= date('Y-m-d H:i');

		$this->command = false;
		$cdb= new CDbCriteria();
		$cdb->condition = "platform_id = $platform->id and is_posted=0 and schedule_date<='$current_date'";

		return ProfilePic::model()->find($cdb);

	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProfilePic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
