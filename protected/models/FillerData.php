<?php

/**
 * This is the model class for table "filler_data".
 *
 * The followings are the available columns in table 'filler_data':
 * @property integer $id
 * @property string $text
 * @property string $media_url
 * @property string $type
 * @property integer $platform_id
 * @property string $start_date
 * @property string $end_date
 * @property string $created_at
 * @property string $link
 * @property string $page_index
 * @property string $youtube
 * @property string $last_generated
 * @property string $publish_time
 * @property string $call_to_action
 * @property string $type_call_to_action
 * @property integer $catch
 *
 * The followings are the available model relations:
 * @property Platform $platform
 */
class FillerData extends BaseModels
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'filler_data';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('text','text','on'=>'create'),
			array('text','text','on'=>'update'),
			array('link','link','on'=>'update'),
			array('link','link','on'=>'create'),
			array('end_date','compare','compareAttribute'=>'start_date','operator'=>'>','message'=>'Start Date must be less than End Date'),


			array('text, type,publish_time, platform_id, start_date, end_date', 'required'),
			array('platform_id', 'numerical', 'integerOnly'=>true),
			array('media_url, created_at,link,youtube', 'length', 'max'=>255),
			array('type', 'length', 'max'=>14),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, text, media_url, type, platform_id, start_date, end_date,type_call_to_action,call_to_action, created_at,last_generated,pagination_size,page_index', 'safe'),

			array('id, text, media_url, type, platform_id,last_generated,start_date, end_date,type_call_to_action,call_to_action,created_at,pagination_size', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'platform' => array(self::BELONGS_TO, 'Platform', 'platform_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		$array =  array(
			'id' => 'ID',
			'text' => 'Post',
			'media_url' => 'Media Url',
			'type' => 'Type',
			'platform_id' => 'Platform',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'publish_time' => 'Publish Time',
			'created_at' => 'Created At',
			'youtube' => 'Youtube',
			'link' => 'Link',
			'type_call_to_action' => 'Call to action type',
			'call_to_action' => 'Call to action',
		);

		return array_merge($array,$this->Labels());
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		if(isset($this->pagination_size))
			$pages = $this->pagination_size;
		else
			$pages=5;
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('media_url',$this->media_url,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('platform_id',$this->platform_id);
		$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('publish_time',$this->publish_time,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('type_call_to_action',$this->type_call_to_action,true);
		$criteria->compare('call_to_action',$this->call_to_action,true);
		$criteria->compare('last_generated',$this->last_generated,true);

		return new CActiveDataProvider($this, array(
			'pagination' => array('pageSize' => $pages),
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FillerData the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function text($attribute){


		if($this->platform->title == 'Twitter'){
			$tcoLengthHttp = 22;
			$tcoLengthHttps = 23;
			$twitterPicLength = 22;
			$twitterVideoLength = 23;
			$urlData = Twitter_Extractor::create($this->text)->extract();
			$tweetLength = mb_strlen($this->text,'utf-8');
			foreach ($urlData['urls_with_indices'] as $url) {
				$tweetLength -= mb_strlen($url['url']);
				$tweetLength += mb_stristr($url['url'], 'https') === 0 ? $tcoLengthHttps : $tcoLengthHttp;
			}
			if ($this->type == 'image'?true:false) $tweetLength += $twitterPicLength;
			if ($this->type == 'video'?true:false) $tweetLength += $twitterVideoLength;
			if( $tweetLength> 140)
			{
				$this->addError($attribute,"<b>oops!!! :( </b>The tweet is longer than 140 characters.".$this->type);
				return false;
			}
		}


		return true;

	}

	public function link($attribute){

		if($this->type == 'preview' and empty($this->link) ){
			$this->addError($attribute,"<b>oops!!! :( </b>Please link cannot be blank.");
			return false;
		}

		return true;
	}
}
