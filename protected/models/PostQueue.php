<?php

/**
 * This is the model class for table "post_queue".
 *
 * The followings are the available columns in table 'post_queue':
 * @property integer $id
 * @property string $post
 * @property string $type
 * @property string $link
 * @property string $youtube
 * @property string $media_url
 * @property string $settings
 * @property string $schedule_date
 * @property integer $catgory_id
 * @property integer $main_category_id
 * @property integer $is_posted
 * @property string $post_id
 * @property integer $is_scheduled
 * @property integer $pinned
 * @property integer $platform_id
 * @property integer $news_id
 * @property string $generated
 * @property integer $parent_id
 * @property string $created_at
 * @property string $errors
 * @property integer $template_id
 ** @property string $call_to_action
 * @property string $type_call_to_action
 * The followings are the available model relations:
 * @property Platform $platforms
 * @property Category $catgory
 * @property ReportFacebook $facebook[]
 * @property ReportTwitter $twitter[]
 * @property ReportInstagram $instagram[]
 * @property News $news
 */
class PostQueue extends BaseModels
{
	public $sorts;
	public $view_posts;
	public $to;
	public $platform;
	public $pinned_posts =false;
	public $pushed_posts = false;
	public $not_posted = false;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'post_queue';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('post','text','on'=>'create'),
			array('post','text','on'=>'update'),
			array('link','link_error','on'=>'create'),
			array('link','link_error','on'=>'update'),
			array('post, type, schedule_date, platform_id', 'required'),
			array('catgory_id, is_posted, is_scheduled, parent_id', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>14),
			array('link, youtube, media_url,errors, post_id', 'length', 'max'=>255),
			array('generated', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('main_category_id,pinned,news_id', 'safe'),
			array('id,main_category_id, post,settings, type,errors, link, youtube, media_url, schedule_date, catgory_id, is_posted, post_id, is_scheduled, platform_id, generated, parent_id, created_at,type_call_to_action,call_to_action', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'platforms' => array(self::BELONGS_TO, 'Platform', 'platform_id'),
			'catgory' => array(self::BELONGS_TO, 'Category', 'catgory_id'),
			'news' => array(self::BELONGS_TO, 'News', 'news_id'),
			'facebook' => array(self::HAS_ONE, 'ReportFacebook', 'row_id'),
			'twitter' => array(self::HAS_ONE, 'ReportTwitter', 'row_id'),
			'instagram' => array(self::HAS_ONE, 'ReportInstagram', 'row_id'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'post' => 'Post',
			'type' => 'Type',
			'link' => 'Link',
			'youtube' => 'Youtube',
			'media_url' => 'Media Url',
			'schedule_date' => 'From',
			'catgory_id' => 'Section',
			'news_id' => 'news id',
			'is_posted' => 'Post Status',
			'post_id' => 'Post ID',
			'errors' => 'errors',
			'is_scheduled' => 'Is Scheduled',
			'platform_id' => 'Platform',
			'generated' => 'Generated',
			'parent_id' => 'Parent',
			'created_at' => 'Created At',
			'type_call_to_action' => 'Call to action type',
			'call_to_action' => 'Call to action',
			'sorts'=>'Sort by',
			'view_posts'=>'View'
		);
	}
	public function search_admin()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('post',$this->post,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('youtube',$this->youtube,true);
		$criteria->compare('media_url',$this->media_url,true);
		$criteria->compare('schedule_date',$this->schedule_date,true);
		$criteria->compare('catgory_id',$this->catgory_id);
		$criteria->compare('is_posted',$this->is_posted);
		$criteria->compare('post_id',$this->post_id,true);
		$criteria->compare('is_scheduled',$this->is_scheduled);
		$criteria->compare('platform_id',$this->platform_id);
		$criteria->compare('news_id',$this->news_id,true);
		$criteria->compare('errors',$this->errors,true);
		$criteria->compare('generated',$this->generated,true);
		$criteria->condition = 'parent_id IS NULL';
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public function custom_search($addBetweenCondition = false,$sortable,$view_post)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.


		if($sortable == null){
			$sortable = 'schedule_date';
		}

		$criteria=new CDbCriteria;


		$criteria->compare('type',$this->type,true);
		$criteria->compare('catgory_id',$this->catgory_id);
		$criteria->compare('platform_id',$this->platform_id);

		if($this->pushed_posts == true) {
			$criteria->addCondition("is_posted = 1");
		}
		if($this->not_posted == true){
			$criteria->addCondition("is_posted != 1");

		}
			if($this->pinned_posts == true){
				$criteria->addCondition('pinned = 1 and is_scheduled =1');
			}

		$criteria->compare('is_scheduled',$this->is_scheduled,true);
		if($sortable == 'newest')
			$criteria->order = 'id  desc';
		else
		$criteria->order = $sortable.'  asc';

		if($view_post == 'eremnews'){
			$criteria->addCondition("`generated`='auto'");

		}elseif($view_post == 'created_by'){
            $criteria->addCondition("`generated` !='auto'");

		}
		if(!$addBetweenCondition){
			$criteria->compare("schedule_date",$this->schedule_date,true);
		}else{
		 $criteria->addBetweenCondition('schedule_date', date('Y-m-d 00:00:00',strtotime($this->schedule_date)), date('Y-m-d 23:59:59',strtotime($this->to)), 'AND');
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function NumberPostMain()
    {
        $criteria = new CDbCriteria;
        $criteria->with = 'catgory';
        $criteria->condition = 'catgory.page_index = :page_index';
        $criteria->params = array('page_index'=>"main");
        $criteria->addCondition("schedule_date like '%" . date('Y-m-d') . "%'");
        $criteria->compare('platform_id', $this->platform_id);
        $criteria->compare('is_scheduled', 1);
        $criteria->compare('is_posted', 0);
            return $this::model()->count($criteria);

    }
    public function NumberPostSport(){
        $criteria = new CDbCriteria;
        $criteria->with = 'catgory';
        $criteria->condition = 'catgory.page_index = :page_index';
        $criteria->params = array('page_index'=>"other");
        $criteria->addCondition("schedule_date like '%" . date('Y-m-d') . "%'");
        $criteria->compare('platform_id', $this->platform_id);
        $criteria->compare('is_scheduled', 1);
        $criteria->compare('is_posted', 0);
        return $this::model()->count($criteria);
    }
    public function NumberPost(){
        $criteria = new CDbCriteria;
        $criteria->addCondition("schedule_date like '%" . date('Y-m-d') . "%'");
        $criteria->compare('platform_id', $this->platform_id);
        $criteria->compare('is_scheduled', 1);
        $criteria->compare('is_posted', 0);
        return $this::model()->count($criteria);
    }


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('post',$this->post,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('youtube',$this->youtube,true);
		$criteria->compare('media_url',$this->media_url,true);
		$criteria->compare('schedule_date',$this->schedule_date,true);
		$criteria->compare('catgory_id',$this->catgory_id);
		$criteria->compare('is_posted',$this->is_posted);
		$criteria->compare('post_id',$this->post_id,true);
		$criteria->compare('is_scheduled',$this->is_scheduled);
		$criteria->compare('platform_id',$this->platform_id);
		$criteria->compare('news_id',$this->news_id,true);
		$criteria->compare('errors',$this->errors,true);
		$criteria->compare('generated',$this->generated,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public function get_post_twitter(){

		$Platform = Platform::model()->find('title = "Twitter"')->id;

		$date = date('Y-m-d H:i');

		$cdb = new CDbCriteria();

		$cdb->addCondition('schedule_date <="' . $date . '" and is_posted=0 and is_scheduled=1 and platform_id='.$Platform);
		$cdb->order = 'schedule_date ASC';

		$this->command = false;

		return $this->find($cdb);
	}


	public function get_post_instagram(){

		$Platform = Platform::model()->find('title = "Instagram"')->id;
		$this->command = false;
		if(!empty($Platform)){
			$date = date('Y-m-d H:i');

			$cdb = new CDbCriteria();

			$this->command = false;
			$cdb->addCondition('schedule_date <="' . $date . '" and is_posted=0 and is_scheduled=1 and platform_id='.$Platform);

			return $this->findAll($cdb);
		}

		return false;
	}

	public function get_post_facebook(){

		$Platform = Platform::model()->find('title = "Facebook"')->id;

		$this->command = false;
		$date = date('Y-m-d H:i:00');

		$cdb = new CDbCriteria();

		$cdb->addCondition('schedule_date <="' . $date . '" and is_posted=0 and is_scheduled=1 and platform_id='.$Platform);

		return $this->findAll($cdb);
	}

	public function get_pinned_posts(){


		$this->command = false;
		$date = date('Y-m-d');
		$yesterday=date('Y-m-d', strtotime(date('Y-m-d').' -1 day'));


		$cdb = new CDbCriteria();

		$cdb->addCondition('(schedule_date like "'.$date.'%" or schedule_date like "'.$yesterday.'%") and is_posted=1 and is_scheduled=1 and pinned=1');

		return $this->findAll($cdb);
	}


	public function text($attribute){


		if($this->platforms->title == 'Twitter'){
			$tcoLengthHttp = 22;
			$tcoLengthHttps = 23;
			$twitterPicLength = 22;
			$twitterVideoLength = 23;
			$urlData = Twitter_Extractor::create($this->post)->extract();
			$tweetLength = mb_strlen($this->post,'utf-8');
			foreach ($urlData['urls_with_indices'] as $url) {
				$tweetLength -= mb_strlen($url['url']);
				$tweetLength += mb_stristr($url['url'], 'https') === 0 ? $tcoLengthHttps : $tcoLengthHttp;
			}
			if ($this->type == 'Image'?true:false) $tweetLength += $twitterPicLength;
			if ($this->type == 'Video'?true:false) $tweetLength += $twitterVideoLength;
			if( $tweetLength> 140)
			{
				$this->addError($attribute,"<b>oops!!! :( </b>The tweet is longer than 140 characters.".$this->type);
				return false;
			}
		}


		return true;

	}
	public function link_error($attribute){

		if($this->type == 'preview' and empty($this->link) ){
			$this->addError('link','<b>oops!!! :( </b> link cannot be blank.');
			return false;
		}


		return true;
	}

	public function GetPost($id,$platform,$parent){

		if($parent == null){
			$this->parent_id = $id;
			$criteria=new CDbCriteria;
			$criteria->compare('parent_id',$this->parent_id);
			return $this->model()->findAll($criteria);

		}else{

			$this->id = $parent;
 			$criteria=new CDbCriteria;
		/*	$criteria->compare('parent_id',$this->parent_id,false,'or');
			$criteria->compare('id',$this->id,false,'or');*/

			$criteria->addCondition('parent_id = '.$this->id. ' and platform_id !='.$platform.' or id = '.$this->id);
			return $this->model()->findAll($criteria);

		}
	}

	public function GetPostRePost($id,$platform,$parent){

		if($parent == null){
			$this->parent_id = $id;
			$criteria=new CDbCriteria;
			$criteria->addCondition('parent_id = '.$this->parent_id .' or id = '.$this->parent_id);
			$criteria->compare('is_posted', 1);
			return $this->model()->findAll($criteria);

		}else{

			$this->id = $parent;
			$criteria=new CDbCriteria;
			/*	$criteria->compare('parent_id',$this->parent_id,false,'or');
                $criteria->compare('id',$this->id,false,'or');*/
			$criteria->compare('is_posted', 1);

			$criteria->addCondition('parent_id = '.$this->id. ' or id = '.$this->id);
			return $this->model()->findAll($criteria);

		}
	}
	public function GetPostImageChanger($id,$platform,$parent,$single_platform=false){

		if($single_platform and $platform !=0){
			$criteria = new CDbCriteria();
			if($parent == null and $this->platform_id == $platform)
				$criteria->addCondition('id = '.$id);
			else
				$criteria->addCondition('parent_id = '.$id .' and platform_id = '.$platform);

			return $this->model()->find($criteria);

		}else {
			if ($parent == null) {
				$this->parent_id = $id;
				$criteria = new CDbCriteria;
				$criteria->addCondition('parent_id = ' . $this->parent_id . ' or id = ' . $this->parent_id);
				return $this->model()->findAll($criteria);

			} else {

				$this->id = $parent;
				$criteria = new CDbCriteria;
				/*	$criteria->compare('parent_id',$this->parent_id,false,'or');
                    $criteria->compare('id',$this->id,false,'or');*/
				$criteria->addCondition('parent_id = ' . $this->id . ' or id = ' . $this->id);
				return $this->model()->findAll($criteria);

			}
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PostQueue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	function get_hashtags($tweet)
	{
		$matches = array();
		preg_match_all('/#[^\s]*/i', $tweet, $matches);
		return $matches[0];
	}
	function get_google_url($Url){
		$matches = array();
		preg_match_all('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!', $Url, $matches);
		return $matches[0];
	}

	function get_urls($post){
		$data = array();
		$urls = array();
		$data = $this->get_google_url($post);
		foreach($data as $data_url){
			$urls[] =  '<a href="'.$data_url.'" target=_blank >'.$data_url.'</a>';
		}

		$items = str_replace($data,$urls,str_replace(PHP_EOL,'<br>',$post));

		return $items;
	}

	function get_hashtags_urls($platform,$post){

		$p = $this->get_urls($post);
		$data = array();
		$hashtag=array();
		$items =null;
		$data = $this->get_hashtags($post);
		foreach($data as $dat){
			$srtipped = str_replace('#','',$dat);
			if($platform == 'Instagram'){

				$hashtag[] = '<a href="https://www.instagram.com/explore/tags/'.$srtipped.'" target="_blank">'.$dat.'</a>';

			}
			elseif($platform== 'Twitter'){
				$hashtag[] = '<a href="https://twitter.com/search?q=	%23'.$srtipped.'" target="_blank">'.$dat.'</a>';

			}elseif($platform == 'Facebook'){
				$hashtag[] = '<a href="https://www.facebook.com/search/top/?q=%23'.$srtipped.'" target="_blank">'.$dat.'</a>';
			}

		}

		$items = str_replace($data,$hashtag,$p);

		return $items;

	}

	public function total_post($platform = 'Facebook' , $generator = 'thematic' ,$schedule_date_from , $schedule_date_to,$editable=false,$type='all'){
        $platform = Platform::model()->find('title = "'.$platform.'"');

        $cdb = new CDbCriteria();
        $cdb->addCondition('is_posted=1');
        if($generator !='all') {
            $cdb->addCondition("`generated` ='" . $generator . "'");
        }
        if($generator=='auto') {
            if ($editable) {
                $cdb->addCondition("`updated_by` IS NOT NULL");
            }else
                $cdb->addCondition("`updated_by` IS NULL");
        }
        if($type !='all'){
            $cdb->addCondition("`type`= '" . $type."'");
        }
        $cdb->addCondition('`platform_id` ='.$platform->id);

        $cdb->addBetweenCondition('schedule_date', date('Y-m-d 00:00:00',strtotime($schedule_date_from)), date('Y-m-d 23:59:59',strtotime($schedule_date_to)));

        return PostQueue::model()->count($cdb);
    }
    public function total_post_query($from,$to){
        $user = Yii::app()->db->createCommand("SELECT count(post_queue.id) as countedid,type,platform.title , `generated` FROM `post_queue` INNER JOIN platform on post_queue.platform_id = platform.id WHERE is_posted=1 and  `schedule_date` >='$from' and `schedule_date` <='$to' group by platform_id,type,`generated`")->queryAll();
        return $user;
    }
}
