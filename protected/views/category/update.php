<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->pageTitle = "Sections | Update";

$this->breadcrumbs = array(
    'sections' => array('admin'),
    $model->title => array('view', 'id' => $model->id),
    'Update',
);
?>
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="col-sm-12 pull-right">
                        <?php echo Yii::app()->params['statement']['previousPage']; ?>

                            <?PHP
                            $this->widget(
                                'booster.widgets.TbButtonGroup',
                                array(
                                    'size' => 'small',
                                    'context' => 'info',
                                    'buttons' => array(
                                        array(
                                            'label' => 'Action',
                                            'items' => array(
                                                array('label' => 'Delete', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
                                                array('label' => 'Manage', 'url' => array('admin'))
                                            )
                                        ),
                                    ),
                                )
                            );
                            ?>

                    </div>
                </div>
                <div class="box-body">
                    <?php $this->renderPartial('_form', array('model' => $model)); ?>
                </div>
            </div>
        </div>
</section>


