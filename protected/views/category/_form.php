<?php
/* @var $this CategoryController */
/* @var $model Category */
/* @var $form TbActiveForm */
?>

	<div class="form">

		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
			'id'=>'category-form',
			// Please note: When you enable ajax validation, make sure the corresponding
			// controller action is handling ajax validation correctly.
			// There is a call to performAjaxValidation() commented in generated controller code.
			// See class documentation of CActiveForm for details on this.
			'enableAjaxValidation'=>true,
			'type' => 'horizontal',
		)); ?>
		<div class="col-sm-2">

			<?php echo $form->textFieldGroup($model,'title'); ?>
		</div>
		<div class="col-sm-2">

			<?php echo $form->dropDownListGroup($model,'page_index',array(
				'widgetOptions'=>array(
					'data'=>array('main'=>'Main page','other'=>'Other page')
				)
			)); ?>
		</div>
		<div class="col-sm-2">

			<?php echo $form->urlFieldGroup($model,'url'); ?>

		</div>
		<div class="col-sm-2">

			<?php echo $form->dropDownListGroup($model,'type',array(
				'widgetOptions'=>array(
					'data'=>array('rss'=>'rss','dom'=>'dom')
				)
			)); ?>

		</div>
		<div class="col-sm-2">

			<?php echo $form->dropDownListGroup($model,'lang',array(
				'widgetOptions'=>array(
					'data'=>array('ar'=>'Arabic','en'=>'English')
				)
			)); ?>

		</div>
		<div class="col-sm-2">

			<?php echo $form->dropDownListGroup($model,'active',array(
				'widgetOptions'=>array(
					'data'=>array('0'=>'Disabled','1'=>'Active')
				)
			)); ?>

		</div>

		<div class="col-sm-2">
			<div class="form-actions  " style="margin-bottom: 20px">
				<?php $this->widget(
					'booster.widgets.TbButton',
					array(
						'buttonType' => 'submit',

						'context' => 'primary',
						'label' => $model->isNewRecord ? 'Create' : 'Save'
					)
				); ?>


			</div>
		</div>
		<?php $this->endWidget(); ?>

	</div><!-- form -->
