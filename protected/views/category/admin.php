<?php
/* @var $this CategoryController */
/* @var $model Category */
/* @var $form TbActiveForm */

$this->pageTitle = "Sections | Admin";

$this->breadcrumbs=array(
	'sections'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Category', 'url'=>array('index')),
	array('label'=>'Create Category', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('#form-visible').change(function(){
	$(this).submit();
	return true;
});
$('.search-form form').submit(function(){
	$('#category-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
/*$(\"input[name='Category[title]']\").attr('class','form-control');

$(\"select[name='Category[active]']\").attr('class','form-control');*/
");
?>

<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-sm-12 pull-right">
						<?php echo Yii::app()->params['statement']['previousPage']; ?>
					</div>
				</div>
				<div class="box-body">

					<?PHP //echo $this->renderPartial('_search',array('model'=>$model),true) ?>
					<?PHP

					$form=$this->beginWidget('booster.widgets.TbActiveForm', array(
						'action'=>Yii::app()->createUrl($this->route),
						'method'=>'get',
						'id'=>'form-visible',
					));
					?>
					<div class="col-sm-2 pull-left page-sizes"  >

						<?php echo $form->dropDownListGroup(
							$model,
							'pagination_size',
							array(

								'widgetOptions'=>array(
									'data'=>$model->pages_size(),
									'htmlOptions'=>array(

									),
								),
								'hint'=>''
							)
						); ?>
					</div>
					<?php
					$this->endWidget();

				/*	$this->widget('CTreeView', array('data' => $model->generateTree(),'persist'=>'cookie'));*/

					$this->widget('booster.widgets.TbGridView', array(
						'id'=>'category-grid',
						'type' => 'striped bordered condensed',
						'template' => '{items}{pager}{summary}',
						'enablePagination' => true,
						'pager' => array(
							'class' => 'booster.widgets.TbPager',
							'nextPageLabel' => 'Next',
							'prevPageLabel' => 'Previous',
							'htmlOptions' => array(
								'class' => 'pull-right',
								'filterClass'=>'asdasdasd',
							),

						),
						'htmlOptions' => array(
							'class' => 'table-responsive'
						),
							'filter' => $model,



						'dataProvider' => $model->search(),

						 /*'filterCssClass' => 'mohammad salahat',*/
						 /*'filterPosition' => 'header',// header / footer // body*/

						'columns' => array(
							/*array(
								'name'=>'id',
								'visible'=>$model->visible_id?true:false,
							),*/
							array(
								'name'=>'url',
								'type'=>'raw',
								'value'=>'CHtml::link($data->url,$data->url,array(\'target\'=>\'_blank\'))',
							),
 							/*array(
								'name'=>'title',
								'type'=>'raw',
								'value'=>'CHtml::link($data->title,$data->url,array(\'target\'=>\'_blank\'))',
								'visible'=>$model->visible_title?true:false,
							),*/

							array(
								'class' => 'booster.widgets.TbEditableColumn',
								'name' => 'title',
								'sortable' => true,
								'editable' => array(
									'url' => $this->createUrl('category/edit'),
									'placement' => 'right',
									'inputclass' => 'span1'
								),
							),

							array(
								'class' => 'booster.widgets.TbEditableColumn',
								'filter'=>array_merge(array(''=>'All status'),array('0'=>'Disabled','1'=>'Active')),
								'name' => 'active',
								'sortable' => true,
								'editable' => array(
									'type' => 'select2',
									'url' => $this->createUrl('category/edit'),
									'source' => array('0'=>'Disabled','1'=>'Active'),
									'placement' => 'right',
									'inputclass' => 'span1'
								),
								'type'=>'raw',
								'value'=>'$data->active?CHtml::tag("span",array("class"=>"label label-success"),"Active",true):CHtml::tag("span",array("class"=>"label label-danger"),"Disabled",true)',
								'visible'=>$model->visible_title?true:false,
							),
							/*array(
								'type'=>'raw',
								'name'=>'active',
								'filter'=>array_merge(array(''=>'All status'),array('0'=>'Disabled','1'=>'Active')),
								 'value'=>'$data->active?CHtml::tag("span",array("class"=>"label label-success"),"Active",true):CHtml::tag("span",array("class"=>"label label-danger"),"Disabled",true)',
								'visible'=>$model->visible_active?true:false,
							),*/
                            /*
							array(
								'name'=>'created_at',
								'visible'=>$model->visible_created_at?true:false,
							),*/
 							array(
								'class' => 'booster.widgets.TbButtonColumn',
								'header' => 'Options',
								//'template' => '{view}{update}{delete}{activate}{deactivate}',
								'template' => '{view}{delete}{activate}{deactivate}',
								'buttons' => array(
									'view' => array(
										'label' => 'View',
										'icon' => 'fa fa-eye',
									),
									'update' => array(
										'label' => 'Update',
										'icon' => 'fa fa-pencil-square-o',
									),
									'delete' => array(
										'label' => 'Delete',
										'icon' => 'fa fa-times',
									),

									'activate' => array(
										'label' => 'Deactivate',
										'url' => 'Yii::app()->createUrl(Yii::app()->controller->id."/status", array("id"=>$data->id))',
										'icon' => 'fa fa-toggle-off',
										'visible' => '$data->active == 1',
										'click' => "function(){

				$.fn.yiiGridView.update('category-grid', {  //change my-grid to your grid's name
				type:'POST',
				url:$(this).attr('href'),
				success:function(data) {
				$.fn.yiiGridView.update('category-grid');}});return false;}",
									),
									'deactivate' => array(
										'label' => 'Activate',
										'url' => 'Yii::app()->createUrl(Yii::app()->controller->id."/status", array("id"=>$data->id))',
										'icon' => 'fa fa-toggle-on',
										'visible' => '$data->active == 0',
										'click' => "function(){
				$.fn.yiiGridView.update('category-grid', {  //change my-grid to your grid's name
				type:'POST',
				url:$(this).attr('href'),
				success:function(data) {
				$.fn.yiiGridView.update('category-grid');}});return false;}"
									),

								)
							),
						))

					);?>
					<style>
						.checkbox {
							display: block;
							min-height: 0px;
							margin-top: 4px;
							margin-bottom: 0px;
							padding-left: 17px;
						}
						.input-group-addon input[type=radio], .input-group-addon input[type=checkbox] {
							margin-top: 0;
						}
						.radio input[type=radio], .radio-inline input[type=radio], .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox] {
							float: left;
							margin-left: -16px;
						}
					</style>



				</div>

			</div>
		</div>
	 </div>
</section>

<!--Tour-->
<?php
$createLink = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=eleven'));

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'firstSection',
		'title'        => 'Title',
		'next'         => 'second',
		'buttons'      => array(
			array('name'=>'Previous','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$createLink';}"),

			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Title of the section and when clicked it render you to section location on the site',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#category-grid_c0',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'second',
		'title'        => 'Active',
		'next'         => 'third',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('firstSection');}"
			),
			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'The status of that section if it is set to active it will generate posts from that url if it is not set to active it will not generate posts from there',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#category-grid_c1',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'third',
		'title'        => 'Option view',
		'next'         => 'fourth',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('second');}"
			),
			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'View this item alone',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '.fa-eye',
		'position'      => 9,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'fourth',
		'title'        => 'Option edit',
		'next'         => 'fifth',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('third');}"
			),
			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'update this item',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '.fa-pencil-square-o',
		'position'      => 9,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php
$continueTour = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=twelve'));

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'fifth',
		'title'        => 'Option remove',

		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('fourth');}"
			),

			array('name'=>'Continue ','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$continueTour';}"),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Delete this item',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '.fa-times',
		'position'      => 9,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<!--endTour-->
