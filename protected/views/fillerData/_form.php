<?php
/* @var $this FillerDataController */
/* @var $model FillerData */
/* @var $form TbActiveForm */
?>
<style type="text/css">
	.form-horizontal .control-label {
		text-align: left;
	}
</style>
<div class="form" style="padding-top: 30px">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'filler-data-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
	'type' => 'horizontal',
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
));

$field = 'col-sm-10';
$label = 'col-sm-2';

?>
	<script>
		$( document ).ready(function() {
			$('#FillerData_publish_time').datetimepicker({
				datepicker:false,
				format:'H:i:00',
				step:30
			});

		});
	</script>

	<input type="hidden" value="<?php echo $model->type; ?>" id="hidden-type" name="hidden-type"/>

	<div class="col-sm-8" style="position: relative;top:10px;">
		<div class="col-sm-1 col-sm-push-1">
			<a class="btn btn-default input-sm" style="background-color:silver;" id="EnglishDirection"><i class="fa fa-align-left "></i></a>
		</div>
		<div class="col-sm-1 col-sm-push-1">
			<a class="btn btn-default input-sm" style="background-color:silver;" id="ArabicDirections"><i class="fa fa-align-right "></i></a>
		</div>
	</div>
	<div class="col-sm-5" >
		<a href="#"   onclick="guiders.show('firstFiller');return false"    ><i class="fa fa-question-circle"></i></a>

		<?php echo $form->textAreaGroup($model,'text',array(
			'labelOptions' => array(
				'class' => 'col-sm-2',
			),
			'wrapperHtmlOptions' => array(
				'class' => 'col-sm-10 arabic-direction',
				'id'=>'PostDir'
			),
			'widgetOptions'=>array(
				'htmlOptions'=>array(
					'maxlength' => 700, 'rows' => 12, 'cols' => 55
				),
			),
		)); ?>

		<div class="col-sm-12 col-sm-push-2" id="twitter" style="display:none;">
			Number of Characters:
			<small id="twitter_counter" style="color:red;font-size: 18px">140</small>

		</div>
	</div>
		<div class="col-sm-7">
			<a href="#"   onclick="guiders.show('SecondFiller');return false"    ><i class="fa fa-question-circle"></i></a>

			<?php
			if($model->id != null){
				echo $form->dropDownListGroup($model,'platform_id',array(
					'widgetOptions'=>array(
						'data'=>CHtml::listData(Platform::model()->findAll('deleted=0'),'id','title'),
						'empty'=>'Choose One',
						'htmlOptions'=>array(
							'onchange'=>'javascript:App.change()',
						),
					),
					'wrapperHtmlOptions' => array(
						'class' => $field,
					),

					'labelOptions' => array(
						'class' => $label,
					),
				));
			}

			else{
				echo $form->checkboxListGroup($model, 'platform_id', array(
					'widgetOptions' => array(
						'data' => CHtml::listData(Platform::model()->findAll('deleted=0'), 'id', 'title'),
						'htmlOptions' => array(
							/*					'onchange'=>'javascript:App.change()',*/
							'class' => 'Platforms'
						),
					),
					'wrapperHtmlOptions' => array(
						'class' => $field,

					),


					'labelOptions' => array(
						'class' => $label,
					),
				));
			} ?>
			<?php
			echo '<a href="#"   onclick="guiders.show(\'ThirdFiller\');return false"    ><i class="fa fa-question-circle"></i></a>
';
			if($model->isNewRecord){
				$array = Yii::app()->params['rule_array']['facebook'];
			}else{
				$array = Yii::app()->params['rule_array'][strtolower($model->platform->title)];
			}
			echo $form->dropDownListGroup($model,'type',array('rows'=>6, 'cols'=>50,
				'widgetOptions'=>array(
					'data'=>$array,
					'empty'=>'Choose One'
				),
				'groupOptions' => array(
					'id'=>'type_filler'
				),
				'labelOptions' => array(
					'class' => $label,
				),
				'wrapperHtmlOptions' => array(
					'class' => $field,
				),
			)); ?>

				<?php echo $form->dropDownListGroup($model,'page_index',array(
					'widgetOptions'=>array(
						'data'=>array('main'=>'Main page','other'=>'Other page')
					),
					'labelOptions' => array(
						'class' => $label,
					),
					'wrapperHtmlOptions' => array(
						'class' => $field,
					),
				)); ?>

			<a href="#"   onclick="guiders.show('fourthFiller');return false"    ><i class="fa fa-question-circle"></i></a>

			<div class="form-group">
				<div class="col-sm-2"><?php echo $form->labelEx($model,'start_date'); ?></div>
				<div class="col-sm-10">
					<?php echo $form->textField($model,'start_date',array('type'=>'text','id'=>'datetimepicker_format_value','class'=>'form-control')); ?>

					<script>
						$( document ).ready(function() {
							$('#datetimepicker_format_value').datetimepicker({
								format:'Y-m-d H:i',step:5,

								minDate: new Date('Y-m-d H:i'),

							});
						});

					</script>
					<?php echo $form->error($model,'start_date'); ?>
				</div>


			</div>
			<a href="#"   onclick="guiders.show('FifthFiller');return false"    ><i class="fa fa-question-circle"></i></a>

			<div class="form-group">
				<div class="col-sm-2"><?php echo $form->labelEx($model,'end_date'); ?></div>
				<div class="col-sm-10">
					<?php echo $form->textField($model,'end_date',array('type'=>'text','id'=>'datetimepicker_format_end_value','class'=>'form-control')); ?>

					<script>
						$( document ).ready(function() {
							$('#datetimepicker_format_end_value').datetimepicker({
								format:'Y-m-d H:i',step:5,

								minDate: new Date('Y-m-d H:i'),

							});
						});

					</script>
					<?php echo $form->error($model,'end_date'); ?>
				</div>


			</div>

			<a href="#"   onclick="guiders.show('sixFiller');return false"    ><i class="fa fa-question-circle"></i></a>

			<?php
			echo $form->textFieldGroup($model,'publish_time',array(

				'wrapperHtmlOptions'=>array(
				'class'=>'col-sm-10'
				),
				'labelOptions'=>array(
					'class'=>'col-sm-2',
				)
			));
			?>
			<?php echo $form->fileFieldGroup($model,'media_url',array('size'=>60,'maxlength'=>255,
				'labelOptions' => array(
					'class' => 'col-sm-2',
					'label' => Yii::t( 'myApp', 'Upload' ),
					'id'=>'change_label'

				),
				'groupOptions' => array(
					'id'=>'file_filler',
					'style'=>'display:none'
				),
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-10',
				),
			)); ?>
			<?php echo $form->textAreaGroup($model,'youtube',array(
				'labelOptions' => array(
					'class' => 'col-sm-2',
				),
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-10',
				),
				'groupOptions' => array(
					'id'=>'youtube_filler',
					'style'=>'display:none'
				),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'maxlength' => 200, 'rows' => 2, 'cols' => 55
					),
				),
			)); ?>

			<?php echo $form->urlFieldGroup($model,'link',array(
				'labelOptions' => array(
					'class' => 'col-sm-2',
				),
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-10',
				),
				'groupOptions' => array(
					'id'=>'link_filler',
					'style'=>'display:none'
				),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'maxlength' => 200, 'rows' => 2, 'cols' => 55
					),
				),
			)); ?>
			<div id="all_call_to_action">

				<input type="checkbox" id="calltoaction" />Enable call to action

				<?php echo $form->dropDownListGroup($model,'type_call_to_action',array(
					'widgetOptions'=>array(
						'data'=>array('SHOP_NOW'=>'SHOP_NOW','BOOK_TRAVEL'=>'BOOK_TRAVEL','LEARN_MORE'=>'LEARN_MORE','SIGN_UP'=>'SIGN_UP','DOWNLOAD'=>'DOWNLOAD','WATCH_MORE'=>'WATCH_MORE','NO_BUTTON'=>'NO_BUTTON','INSTALL_MOBILE_APP'=>'INSTALL_MOBILE_APP','USE_MOBILE_APP'=>'USE_MOBILE_APP','INSTALL_APP'=>'INSTALL_APP','USE_APP'=>'USE_APP','PLAY_GAME'=>'PLAY_GAME','WATCH_VIDEO'=>'WATCH_VIDEO','OPEN_LINK'=>'OPEN_LINK','LISTEN_MUSIC'=>'LISTEN_MUSIC','MOBILE_DOWNLOAD'=>'MOBILE_DOWNLOAD','GET_OFFER'=>'GET_OFFER','GET_OFFER_VIEW'=>'GET_OFFER_VIEW','BUY_NOW'=>'BUY_NOW','BUY_TICKETS'=>'BUY_TICKETS','UPDATE_APP'=>'UPDATE_APP','BET_NOW'=>'BET_NOW','GET_DIRECTIONS'=>'GET_DIRECTIONS','ADD_TO_CART'=>'ADD_TO_CART','ORDER_NOW'=>'ORDER_NOW','SELL_NOW'=>'SELL_NOW'),
						'empty'=>'Choose One',


					),
					'wrapperHtmlOptions' => array(
						'class' => $field,
						/*				'disabled'=>true,*/

					),
					'groupOptions' => array(
						'id'=>'type_call_to_action',

					),

					'labelOptions' => array(
						'class' => $label,

					),
				)); ?>
				<?php
				echo $form->textFieldGroup($model,'call_to_action',array(
					'wrapperHtmlOptions' => array(
						'class' => $field,
						/*				'disabled'=>true,*/

					),
					'labelOptions' => array(
						'class' => $label,

					),
				))
				?>
			</div>

		</div>

	<div class="form-actions  pull-right" style="margin-bottom: 20px;margin-right:20px;">
		<?php $this->widget(
			'booster.widgets.TbButton',
			array(
				'buttonType' => 'submit',
				'context' => 'primary',
				'label' => $model->isNewRecord ? 'Create' : 'Save'
			)
		); ?>

	</div>
<?php $this->endWidget(); ?>

</div><!-- form -->

<!-- Tour -->
<?php
$createLink = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=nine'));

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'firstFiller',
		'title'        => 'Text',
		'next'         => 'second',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'The filler post description',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#FillerData_text',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'SecondFiller',
		'title'        => 'Platform',
		'next'         => 'third',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'The platform where the filler will be posted to',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#FillerData_platform_id',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'ThirdFiller',
		'title'        => 'Type',
		'next'         => 'fourth',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?

		'description'   => 'Your post type(image,video,text,upload from youtube or Preview)',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#FillerData_type',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'fourthFiller',
		'title'        => 'Start time',
		'next'         => 'fifth',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?

		'description'   => 'The date for the post to start posting',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#datetimepicker_format_value',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'FifthFiller',
		'title'        => 'End time',
		'next'         => 'fifth',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?

		'description'   => 'The date for the post to end posting',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#datetimepicker_format_end_value',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'sixFiller',
		'title'        => 'Publish time',
		'next'         => 'six',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?

		'description'   => 'The time you want this post to be posted from the start day you chosen  until the end date',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#FillerData_publish_time',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'six',
		'title'        => 'Category',
		'next'         => 'seven',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?

		'description'   => 'Click to create the post',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#yw1',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>