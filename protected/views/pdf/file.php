<?php
/* @var $this PdfController */
/* @var $model PostQueue */
/* @var $model_parent PostQueue */
/* @var $item PostQueue */
/* @var $form CActiveForm */
$id_news = null;
//137.833333333
$Platform = CHtml::listData(Platform::model()->findAll('deleted=0'), 'id', 'title');

$data = load_data($model_parent);
function load_data($data)
{

    $items = array();
    foreach ($data as $item) {
        $items[] = $item->attributes;
    }
    return $items;
}

function search_in_array($data, $id, $news_id, $Platform)
{

    foreach ($data as $index => $item) {
        if (array_search($id, $item)) {
            if ($data[$index]['news_id'] == $news_id && $data[$index]['platform_id'] == $Platform)
                return $index;
        }
    }

    return false;
}

?>
<html>
<head>
    <meta charset="UTF-8">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <style>

        @import url(http://fonts.googleapis.com/earlyaccess/droidarabickufi.css);

        body {
            font-family: 'Droid Arabic Kufi', sans-serif;

        }
        .container {
            padding-right: 0px;
            padding-left: 0px;
            margin-right: 0px;
            margin-left: 0px;
        }
        @media (min-width: 1200px)
        {
            .container {
                width: 100%;
            }
        }
        .container {
            width: 100%;
        }

        .font-small {
            font-size: 12px;
        }

        .p_header {
            width: 22%;
            display: inline-block;
            text-align: center
        }

        .td_Facebook {
            width: 685px;
            padding: 5px;
            border: 1px solid #D0CECE;
            border-radius: 5px;
        }
        .img{
            width: 70%;display: block;margin-left: auto;margin-right: auto;
        }
        .row {
            padding-top: 4px;
            margin-bottom: 0px;

            position: inherit;
            max-height: 1014px;
            overflow: auto;

            background: url('http://s3.amazonaws.com/sortechs/anazahra/image_PostQueue_1462430647.png');
            background-size: cover;
        }
        ol, ul {
            margin-top: 0;
            margin-bottom: 0px;
        }
        li {
            list-style: none;
            padding: 25px;
            text-align: center;
            border: 1px solid;
            border-radius: 50px;
            margin-bottom: 20px;
            margin-top: 10px;
            height: 80px;
            line-height: 28px;
            font-weight: bold;
            border-color: #00B0F0;
            color: #00B0F0;
            background-color: #fff;
        }
        .active{
            color: #fff;
            border-color: #00B0F0;
            background-color: #00B0F0;
        }
        .img-center{
            display: block;
            margin-right: auto;
            margin-left: auto;
            width: 70%;
            border: 3px solid rgba(0, 176, 240, 0.5)
        }
        .category{
            color: white;
            font-size: 100%;
            background-color: #00B0F0;
            padding: 5px 15px 5px 15px;
            border-radius: 14px;
            border: 1px solid #008BBD;
            font-weight: 900;
            margin-right: 7px;
            margin-bottom: 7px;
        }

        .category_line{
            color: #00B0F0;
            font-size: 100%;
            background-color: #fff;
            border-radius: 14px;
            border: 1px solid #008BBD;
            font-weight: 300;
            margin-right: 7px;
            padding: 5px 15px 5px 15px;
            margin-bottom: 7px;

        }
        .caption_text {
            list-style: none;
            padding: 25px;
            border: 1px solid;
            border-radius: 0px;
            margin-bottom: 20px;
            margin-top: 10px;

            font-weight: bold;
            border-color: #00B0F0;
            color: #00B0F0;
            background-color: #fff;
        }
        .font_size{
            color: #00B0F0;
            font-size: 12px;
            font-weight: 700;
        }
    </style>
</head>
<body>
<div class="container">
    <?PHP foreach ($model as $item) { ?>
        <?PHP /*if ($item->generated = 'auto') {*/ ?>
       <div class="row">
           <div class="col-xs-2">
               <div class="col-xs-8">
                   <ul>
                       <li <?PHP if(date('D', strtotime($item->schedule_date)) == 'Sun') echo 'class="active"'?>>Sun</li>
                       <li <?PHP if(date('D', strtotime($item->schedule_date)) == 'Mon') echo 'class="active"'?>>Mon</li>
                       <li <?PHP if(date('D', strtotime($item->schedule_date)) == 'Tue') echo 'class="active"'?>>Tue</li>
                       <li <?PHP if(date('D', strtotime($item->schedule_date)) == 'Wed') echo 'class="active"'?>>Wed</li>
                       <li <?PHP if(date('D', strtotime($item->schedule_date)) == 'Thu') echo 'class="active"'?>>Thu</li>
                       <li <?PHP if(date('D', strtotime($item->schedule_date)) == 'Fri') echo 'class="active"'?>>Fri</li>
                       <li <?PHP if(date('D', strtotime($item->schedule_date)) == 'Sat') echo 'class="active"'?>>Sat</li>
                   </ul>
               </div>
           </div>
           <div class="col-xs-5">
               <div class="col-xs-12" style="margin-bottom:  2px;">
                   <h6><small class="category">Category : </small><b class="category_line"><?PHP if(isset($item->catgory->title)) echo $item->catgory->title ?></b></h6>
                   <hr>
               </div>
               <div class="col-xs-12" style="margin-bottom:  2px;">
                   <?PHP foreach ($Platform as $index_platform => $item_platform) { ?>
                       <?PHP $index = search_in_array($data, $item->id, $item->news_id, $index_platform); ?>
                       <div class="col-xs-4" style="font-size: 12px;color:#00B0F0;font-weight:700;">
                           <div class="col-xs-12"><?PHP if(isset($model_parent[$index]->platforms->title)) echo $model_parent[$index]->platforms->title ?></div>
                           <div class="col-xs-12">Type : <b><?PHP if(isset($model_parent[$index]->type)) echo $model_parent[$index]->type?></b></div>
                           <div class="col-xs-12">Date : <b><?PHP echo date('Y-m-d', strtotime($model_parent[$index]->schedule_date)) ?></b></div>
                           <div class="col-xs-12">Time : <b><?PHP echo date('h:i A', strtotime($model_parent[$index]->schedule_date)) ?></b></div>
                       </div>
                   <?PHP } ?>

               </div>
               <div class="col-xs-12">
                   <hr>
                   <?PHP if(!empty($item->media_url)){ ?><img src="<?PHP echo $item->media_url ?>" class="img-responsive img-center"/><?PHP } ?>
               </div>
           </div>
           <div class="col-xs-5" style="margin-top: 30px">
               <?PHP foreach ($Platform as $index_platform => $item_platform) { ?>
                   <?PHP $index = search_in_array($data, $item->id, $item->news_id, $index_platform); ?>
                   <div class="col-xs-12">
                       <p class="font_size">Caption - <?PHP echo $item_platform?></p>
                       <p class="caption_text"><?PHP echo $model_parent[$index]->post?></p>
                   </div>
               <?PHP } ?>
           </div>
       </div>
        <?PHP /*}*/ ?>
    <?PHP } ?>
</div>
</body>
</html>