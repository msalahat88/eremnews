<?php
/* @var $this PdfController */
/* @var $model Pdf */

$this->pageTitle = "PDF | View";

$this->breadcrumbs=array('PDF File'=>array('index'),$model->id);
?>
<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">

					<div class="col-md-12 pull-right" style="text-align: left;">
						<?php echo Yii::app()->params['statement']['previousPage']; ?>
						<?PHP
						$this->widget(
							'booster.widgets.TbButtonGroup',
							array(
								'size' => 'small',
								'context' => 'info',
								'buttons' => array(
									array(
										'label' => 'Action',
										'items' => array(
 											array('label' => 'Delete', 'url' => '#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
											array('label' => 'Manage', 'url'=>array('admin'))
										)
									),
									array('label' => 'Create','buttonType'=>'link', 'url'=>array('create'),								'context' => 'info',
										'context' => 'success',
										'htmlOptions' => array('class' => 'btns-positions'), // for inset effect


									),
								),
								'htmlOptions'=>array(
									'class'=>'pull-right	'
								)
							)
						);
						?>
					</div>
				</div>
				<div class="box-body">
					<?php $this->widget('booster.widgets.TbDetailView', array(
						'data'=>$model,
						'attributes'=>array(
							'id',
							'from_date',
							'to_date',
							'media_url',
							'created_at',
						),
					)); ?>
				</div>
			</div>
		</div>
</section>
