<?php
/* @var $this PostTemplateController */
/* @var $model PostTemplate */

$this->pageTitle = "Post template | Create";

$this->breadcrumbs=array(
	'Post Templates'=>array('admin'),
	'Create',
);
Yii::app()->clientScript->registerScript('script', '
	window.App = {};
	App.add=function(value){
		if(value == "[new_line]")
			$("#text_area").val($("#text_area").val()+"\n");
		else
			$("#text_area").val($("#text_area").val()+value+" ");
	};
	App.platform=function(){
		var type_value = $("#PostTemplate_type option:selected").val();
		$.post("getAll",{id:$("#PostTemplate_platform_id").val()}, function( data ) {
			if(data !=\'\') {
				var toAppend = \'\'; data = jQuery.parseJSON( data);
				$.each(data,function(i,o){
					toAppend += \'<option value="\'+i+\'">\'+o+\'</option>\';});
				$("#PostTemplate_type").find("option").remove().end();
				$("#PostTemplate_type").append(toAppend);
				$(\'#PostTemplate_type option[value=\'+type_value+\']\').attr(\'selected\',\'selected\');
			}
		});
	};
	$("#PostTemplate_submitToAll").click(function(){
		if($(this).is(":checked")){
			$.post("getAll",{id:1}, function( data ) {
				if(data !=\'\') {
					var toAppend = \'\'; data = jQuery.parseJSON( data);
					$.each(data,function(i,o){
						toAppend += \'<option value="\'+i+\'">\'+o+\'</option>\';});
					$("#PostTemplate_type").find("option").remove().end();
					$("#PostTemplate_type").append(toAppend);
				}
			});
			$(".platform").hide();
			$(".category").hide();
		}else{
			$(".platform").show();
			$(".category").show();
		}
	});
');
Yii::app()->clientScript->registerCss('mycss', '
 button#yw0 {
margin-right: 17px;
}
button#yw1 {
    margin-right: 16px;
}
');
?>

<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-sm-9"><?PHP
						$this->widget(
							'booster.widgets.TbButtonGroup',
							array(
								'size' => 'small',
								'context' => 'info',
								'buttons' => array(
									array(
										'label' => 'Manage',
										'buttonType' =>'link',
										'url' => array('/PostTemplate/admin')
									),
								),
							)
						);
						?></div>
					<div class="col-md-3" style="padding-top: 19px;text-align: left;">
						<?php echo Yii::app()->params['statement']['previousPage']; ?>
					</div>
				</div>
				<div class="box-body">
					<?php $this->renderPartial('_form', array('model' => $model)); ?>
				</div>
			</div>
		</div>
	</div>
</section>