<?php
/* @var $this CoverPhotoController */
/* @var $model CoverPhoto */

$this->pageTitle = "Cover photo | View";

$this->breadcrumbs=array(
	'Cover Photos'=>array('admin'),
	$model->title,
);
?>
<section class="content">
	<?PHP if(Yii::app()->user->hasFlash('create')){ ?>
		<div class="callout callout-success" style="margin-top:20px;">
			<h4>created successfully. </h4>
		</div>

	<?PHP } if(Yii::app()->user->hasFlash('update')){ ?>
		<div class="callout callout-info" style="margin-top:20px;">
			<h4>updated successfully. </h4>
		</div>
	<?php } ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-sm-9"><?PHP
						$this->widget(
							'booster.widgets.TbButtonGroup',
							array(
								'size' => 'small',
								'context' => 'info',
								'buttons' => array(
									array(
										'label' => 'Action',
										'items' => array(
 											array('label' => 'Update',
												'visible' => $model->is_posted == 0,
												'url'=>array('update', 'id'=>$model->id)),
											array('label' => 'Delete', 'url' => '#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
											array('label' => 'Manage', 'url'=>array('admin'))
										)
									),
									array('label' => 'Create','buttonType'=>'link', 'url'=>array('create'),								'context' => 'info',
										'context' => 'success',
										'htmlOptions' => array('class' => 'btns-positions')),

								),

								/*	'htmlOptions'=>array(
                                        'class'=>'pull-right	'
                                    )*/
							)
						);

						?></div>
					<div class="col-sm-3" style="text-align: left;">
						<?php echo Yii::app()->params['statement']['previousPage']; ?>





					</div>
				</div>
				<div class="box-body">
					<?php $this->widget('booster.widgets.TbDetailView', array(
						'data'=>$model,
						'attributes'=>array(
							'id',
							'title',
							'schedule_date',
							array(
								'name'=>'is_posted',
								'value'=>($model->is_posted==1)?"Yes":(($model->is_posted==2)?"Not Posted (Please retry)":"No"),
							),
							array(
								'name'=>'platform_id',
								'value'=>$model->platform->title,
							),
							array(
								'name'=>'media_url',
								'type'=>'html',
								'value'=>CHtml::image($model->media_url,$model->title,array("class"=>"img-responsive img-tb-grid-view")),
							),
							'created_at',
						),
					)); ?>
				</div>
			</div>
		</div>
</section>

