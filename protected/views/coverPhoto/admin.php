<?php
/* @var $this CoverPhotoController */
/* @var $model CoverPhoto */
$this->pageTitle = "Cover photo | Admin";

$this->breadcrumbs=array(
	'Cover Photos'=>array('admin'),
	'Manage',
);
Yii::app()->clientScript->registerScript('search', "
$('#form-visible').change(function(){
	$(this).submit();
	return true;
});
$('.search-form form').submit(function(){
	$('#cover-photo-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<style>
	.showhideitem{
		display: block;
	}
</style>
<style>
	.checkbox {
		display: block;
		min-height: 0px;
		margin-top: 4px;
		margin-bottom: 0px;
		padding-left: 17px;
	}
	.input-group-addon input[type=radio], .input-group-addon input[type=checkbox] {
		margin-top: 0;
	}
	.radio input[type=radio], .radio-inline input[type=radio], .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox] {
		float: left;
		margin-left: -16px;
	}
</style>

	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<div class="col-sm-9"><?PHP
							$this->widget(
								'booster.widgets.TbButtonGroup',
								array(
									'size' => 'small',
									'context' => 'info',
									'buttons' => array(
										array(
											'label' => 'Create',
											'buttonType' =>'link',
											'url' => array('coverPhoto/create')
										),
									),
									/*'htmlOptions'=>array(
										'class'=>'pull-right	'
									)*/
								)
							);

							?></div>
						<div class="col-sm-3" style="text-align: left;">
							<?php echo Yii::app()->params['statement']['previousPage']; ?>





						</div>
					</div>
					<div class="box-body">
						<?PHP //echo $this->renderPartial('_search',array('model'=>$model),true) ?>

						<?PHP
						$form=$this->beginWidget('booster.widgets.TbActiveForm', array(
							'action'=>Yii::app()->createUrl($this->route),
							'method'=>'get',
							'id'=>'form-visible',
						));
						?>
						<div class="col-sm-2 pull-left page-sizes"  >

							<?php echo $form->dropDownListGroup(
								$model,
								'pagination_size',
								array(

									'widgetOptions'=>array(
										'data'=>$model->pages_size(),
										'htmlOptions'=>array(

										),
									),
									'hint'=>''
								)
							); ?>
						</div>
						<?php
						$this->endWidget();
						$this->widget('booster.widgets.TbGridView', array(
							'id'=>'cover-photo-grid',
							'type' => 'striped bordered condensed',
							'template' => '{pager}{items}{summary}{pager}',
							'enablePagination' => true,
							'pager' => array(
								'class' => 'booster.widgets.TbPager',
								'nextPageLabel' => 'Next',
								'prevPageLabel' => 'Previous',
								'htmlOptions' => array(
									'class' => 'pull-right'
								)
							),
							'htmlOptions' => array(
								'class' => 'table-responsive'
							),
							'dataProvider' => $model->search(),

							'filter' => $model,
 							'columns' => array(

								/*array(
									'name'=>'id',
									'visible'=>$model->visible_id?true:false,

								),*/
								array(
									'class' => 'booster.widgets.TbEditableColumn',
									'name' => 'title',
									'sortable' => true,
									'editable' => array(
										'url' => $this->createUrl('coverPhoto/edit'),
										'placement' => 'right',
										'inputclass' => 'span1'
									),
									'visible'=>$model->visible_title?true:false,
								),
                                array(
                                     'name' => 'schedule_date',
                                ), array(
									'name' => 'page_index',
									'value'=>'$data->page_index=="main"?"Main":"Sport";'
								),

								array(
									'name'=>'is_posted',
									'type'=>'html',
									'value'=>'($data->is_posted==1)?"Yes":(($data->is_posted==2)?"Not Posted (Please retry)":"No")',
									'visible'=>$model->visible_is_posted?true:false,

								),
								array(
									'class' => 'booster.widgets.TbEditableColumn',
									'filter'=>CHtml::listData(Platform::model()->findAll('title != "Instagram"'),'id','title'),
									'name' => 'platform_id',
									'sortable' => true,
									'editable' => array(
										'type' => 'select2',
										'url' => $this->createUrl('coverPhoto/edit'),
										'source' =>CHtml::listData(Platform::model()->findAll('title != "Instagram"'),'id','title'),
										'placement' => 'right',
										'inputclass' => 'span1'
									),
									'type'=>'raw',
									'value'=>'$data->platform->title',
									'visible'=>$model->visible_title?true:false,
								),
								array(
									'name'=>'media_url',
									'type'=>'html',
									'value'=>'CHtml::image($data->media_url,$data->title,array("class"=>"img-responsive img-tb-grid-view"))',
									'htmlOptions'=>array('width'=>'1000px' ),
									'visible'=>$model->visible_media_url?true:false,
								),
							/*	array(
									'name'=>'created_at',
									'visible'=>$model->visible_created_at?true:false,

								),*/

								array(
									'class' => 'booster.widgets.TbButtonColumn',
									'header' => 'Options',
									'template' => '{view}{update}{repost}{delete}',
									'buttons' => array(
										'view' => array(
											'label' => 'View',
											'icon' => 'fa fa-eye',
										),
										'update' => array(
											'label' => 'Update',
											'visible' => '$data->is_posted == 0',

											'icon' => 'fa fa-pencil-square-o',
										),
										'repost' => array(
											'label' => 'Repeat',
											'icon' => 'fa fa-repeat',
											'visible'=>'$data->is_posted > 0',
											'url' => 'CHtml::normalizeUrl(array(Yii::app()->controller->id."/form/id/". $data->id . "&ajax=true"));',
											'click' => '
                                        function(e) {
                                        $("#ajaxModal").remove();
                                        e.preventDefault();
                                        var $this = $(this);
                                        $remote = $this.data("remote") || $this.attr("href");
                                        $modal = $("<div class=\'modal fade in\' style=\'display: block\' id=\'myModal\'><div class=\'modal-dialog\'><div class=\'modal-content\'><div class=\'modal-body\'><h5 align=\'center\'>&nbsp;  Please Wait .. </h5></div></div> </div></div>");
                                      $("body").append($modal);
                                     // $("#myModal").remove();
                                       $modal.modal({backdrop: "static", keyboard: false});
                                     $modal.load($remote);
                                     // $("body").append($modal);
                                    }',
											'options' => array(
												'data-toggle' => 'myModal',
												'style' => 'padding:4px;'
											),

										),
										'delete' => array(
											'label' => 'Delete',
											'icon' => 'fa fa-times',
										),
									)
								),
							)));?>
					</div>
				</div>
			</div>
	</section>
<!-- Tour -->
<?php
$createLink = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=fifteen'));

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'firstCover',
		'title'        => 'Title',
		'next'         => 'second',
		'buttons'      => array(
			array('name'=>'Previous','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$createLink';}"),

			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Title of the cover photo',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#cover-photo-grid_c0',
		'position'      => 11,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'second',
		'title'        => 'Schedule Date',
		'next'         => 'third',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('firstCover');}"
			),
			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'When the Cover will be changed',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#cover-photo-grid_c1',
		'position'      => 11,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'third',
		'title'        => 'Is posted',
		'next'         => 'fourth',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('second');}"
			),
			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Demonstrate whether the cover photo has changed',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#cover-photo-grid_c2',
		'position'      => 11,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'fourth',
		'title'        => 'Platform',
		'next'         => 'fifth',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('third');}"
			),
			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Demonstrate the platform that its cover photo will be changed',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#cover-photo-grid_c3',
		'position'      => 11,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'fifth',
		'title'        => 'Media',
		'next'         => 'six',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('fourth');}"
			),
			array(
				'name'   => 'Next',
				'onclick'=> "js:function(){ $('#yw1').addClass('showhideitem'); guiders.hideAll(); $('.highlight pre').hide(); guiders.show('six'); }"
			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Chosen image to replace cover photo ',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#cover-photo-grid_c4',
		'position'      => 11,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php
$createTemplate = Yii::app()->createUrl('coverPhoto/create',array('#' => 'guider=first'));

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'six',
		'title'        => 'Action create',
		'next'         => 'seven',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('fifth');$('#yw1').removeClass('showhideitem') }"
			),
			array(
				'name'   => 'Next',
				'onclick'=> "js:function(){ $('#yw1').removeClass('showhideitem'); guiders.hideAll(); $('.highlight pre').hide(); guiders.show('sex'); }"
			),
			array('name'=>'Create Tour','classString' => 'tourcolor','onclick'=> "js:function(){   document.location = '$createTemplate'; }"),


			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll(); $('#yw1').removeClass('showhideitem') }"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Create new cover photo',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#yw1',
		'position'      => 9,
		'xButton'       => false,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'sex',
		'title'        => 'Option view',
		'next'         => 'seven',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){ $('#yw1').addClass('showhideitem'); guiders.hideAll(); $('.highlight pre').hide(); guiders.show('fifth'); }"
			),
			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'View this item alone',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '.fa-eye',
		'position'      => 9,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'seven',
		'title'        => 'Option edit',
		'next'         => 'eight',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('sex');}"
			),
			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'update this item',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '.fa-pencil-square-o',
		'position'      => 9,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php
$continueTour = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=fourteen'));

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'eight',
		'title'        => 'Option remove',

		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('seven');}"
			),

			array('name'=>'Continue ','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$continueTour';}"),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Delete this item',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '.fa-times',
		'position'      => 9,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>

<!--endTour-->
