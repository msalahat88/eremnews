<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form TbActiveForm */
?>
<style>
    .glyphicon-ok ,.glyphicon-remove{
        position: relative;
        top: -34px;
        right: 18px;
        float: right;
    }
    .form-group {
        margin-bottom: 34px;
    }
    span.glyphicon.glyphicon-lock {
        position: absolute;
        top: 9px;
        right: 9px;
    }

    span.glyphicon.glyphicon-envelope {
        position: absolute;
        top: 9px;
        right: 9px;
    }

    img.img-responsive {
        margin-left: auto;
        margin-right: auto;
        margin-bottom: 20px;
    }

     html {
         background: url('http://www.sortechs.com/images/bg.gif') no-repeat center center fixed;
         -webkit-background-size: cover;
         -moz-background-size: cover;
         -o-background-size: cover;
         background-size: cover;
     }
    body {
        background-color: rgba(255, 255, 255, 0);
    }
    .footer {
        background-color: #444444;
        color: #fff;
        padding-top: 10px;
        padding-bottom: 28px;
    }
    .login-box, .register-box {
        width: 390px;
        margin: 3% auto;
    }
    .content-wrapper, .right-side {
        min-height: 100%;
        background-color: rgba(255, 255, 255, 0);
        z-index: 800;
    }
    .content-wrapper, .right-side, .main-footer {
        -webkit-transition: -webkit-transform 0.3s ease-in-out, margin 0.3s ease-in-out;
        -moz-transition: -moz-transform 0.3s ease-in-out, margin 0.3s ease-in-out;
        -o-transition: -o-transform 0.3s ease-in-out, margin 0.3s ease-in-out;
        transition: transform 0.3s ease-in-out, margin 0.3s ease-in-out;
        margin-left: 0px;
        z-index: 820;
    }
    .powered {
        text-align: center;
    }
</style>
<div class="login-box">
    <img src="<?php echo Yii::app()->params['domain'] ?>image/SortehsLogo.png" class="img-responsive"/>


    <div class="login-logo">

        <a target="_blank" href="http://www.eremnews.com/">

            <img src="<?php echo Yii::app()->BaseUrl . '/image/general.png';?>" class="img-responsive" style="width: 40%"/>
        </a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Welcome To admin panel</p>
        <?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
            'id'=>'login-form',
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
            ),

        )); ?>

        <?php echo $form->errorSummary($model); ?>

        <?php echo $form->textFieldGroup($model,'username',array('class'=>'input-large span10','placeholder'=>'type username')); ?>

        <?php echo $form->passwordFieldGroup($model,'password',array('class'=>'input-large span10','placeholder'=>'type password')); ?>

        <?php echo $form->checkboxGroup($model,'rememberMe',array('class'=>'remember')); ?>

        <div class="form-actions">
            <?php $this->widget(
                'booster.widgets.TbButton',
                array(
                     'buttonType' => 'submit',
                    'context' => 'primary',
                    'label' => 'Login'
                )
            ); ?>
        </div>

        <?php $this->endWidget(); ?>
    </div>
    <div class="footer">
        <div class="col-sm-12 powered">
            <small>Powered by <a href="http://www.sortechs.com" target="_blank">Sortechs</a></small>
        </div>
    </div>
    <div class="col-md-12">
        <p id="massage_results" style="margin-top: 20px"></p>
    </div>
</div>








