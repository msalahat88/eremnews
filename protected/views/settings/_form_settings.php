<?php
/* @var $this SettingsController */
/* @var $model Settings */
/* @var $form TbActiveForm */
?>
<?php
$form=$this->beginWidget('booster.widgets.TbActiveForm', array(
    'id'=>'settings-custom-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
));
$ones= array();
for($one = 0 ; $one < 51 ;$one++){
    $ones[$one]=$one;
}
?>
<?php echo $form->errorSummary($model); ?>
<div class="col-sm-6">
    <div class="col-sm-12">
        <?php
        $category = CHtml::listData(Category::model()->get_category(false),'id','title') ;
        $html =array('ReadOnly'=>'ReadOnly','disabled'=>'disabled');
        if($model->isNewRecord){

            $category = $model->get_category() ;
            $html = array();
        }
        echo   $form->dropDownListGroup($model,'category_id',array(
            'widgetOptions'=>array(
                'data'=>$category,
                'empty'=>'Choose One',
                'htmlOptions'=>$html

            ),
        )) ; ?>
    </div>

    <div class="col-sm-6">
        <?php echo $form->timeFieldGroup($model,'start_date_time',array(
                'widgetOptions'=>array(
                    'htmlOptions'=>array('class'=>'time_start')
                )
            )
        ); ?>
    </div>
    <div class="col-sm-6">
        <?php echo $form->timeFieldGroup($model,'end_date_time',array(
            'widgetOptions'=>array(
                'htmlOptions'=>array('class'=>'time_end')
            )
        )); ?>
    </div>
    <div class="col-sm-12">
        <?php 	echo $form->dropDownListGroup($model,'gap_time',array(
            'widgetOptions'=>array(
                'data'=>$model->gap_time(),

            ),
        )); ?>
    </div>
</div>
<div class="col-sm-6">
    <div class="col-sm-12">
        <?php
        echo $form->checkboxGroup($model,'direct_push',array('checked'=>false));
        ?>
    </div>
    <div class="col-sm-12 hiddenshowntimes">
        <?php echo $form->timeFieldGroup($model,'direct_push_start',array(
                'widgetOptions'=>array(
                    'htmlOptions'=>array('class'=>'direct_time_start')
                )
            )
        ); ?>
    </div>
    <div class="col-sm-12 hiddenshowntimes">
        <?php echo $form->timeFieldGroup($model,'direct_push_end',array(
            'widgetOptions'=>array(
                'htmlOptions'=>array('class'=>'direct_time_end')
            )
        )); ?>
    </div>
</div>
<div class="col-sm-12">
    <div class="form-actions  pull-right" style="margin-bottom: 20px">
        <?php
        echo !$model->isNewRecord ? CHtml::link('Remove', array('settings/delete','id'=>$model->id),
            array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?','class'=>'btn btn-danger','style'=>'margin-right: 14px;')
        ): '';
        $this->widget(
            'booster.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'context' => 'primary',
                'label' => $model->isNewRecord ? 'Create' : 'Save'
            )
        );

        ?>

    </div>
</div>
<?php $this->endWidget(); ?>
<script>
    $( document ).ready(function() {
        $('#Settings_start_date_time').datetimepicker({
            datepicker:false,
            format:'H:i:00',
            step:30
        });
        $('#Settings_direct_push_start').datetimepicker({
            datepicker:false,
            format:'H:i:00',
            step:30
        });
        $('.time_start').datetimepicker({
            datepicker:false,
            format:'H:i:00',
            step:30
        });
        $('.time_end').datetimepicker({
            datepicker:false,
            format:'H:i:00',
            step:30
        });
        $('#Settings_direct_push_end').datetimepicker({
            datepicker:false,
            format:'H:i:00',
            step:30
        });
        $('#Settings_end_date_time').datetimepicker({
            datepicker:false,
            format:'H:i:00',
            step:30
        });
    });
</script>