<?php
/* @var $this PostQueueController */
/* @var $model PostQueue */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'post'); ?>
		<?php echo $form->textArea($model,'post',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'type'); ?>
		<?php echo $form->textArea($model,'type',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'schedule_date'); ?>
		<?php echo $form->textField($model,'schedule_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'catgory_id'); ?>
		<?php echo $form->textField($model,'catgory_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_posted'); ?>
		<?php echo $form->textField($model,'is_posted'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'post_id'); ?>
		<?php echo $form->textField($model,'post_id',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_scheduled'); ?>
		<?php echo $form->textField($model,'is_scheduled'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'platform_id'); ?>
		<?php echo $form->textField($model,'platform_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'generated'); ?>
		<?php echo $form->textField($model,'generated',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'parent_id'); ?>
		<?php echo $form->textField($model,'parent_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_at'); ?>
		<?php echo $form->textField($model,'created_at'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->