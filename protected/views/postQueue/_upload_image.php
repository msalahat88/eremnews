<?php
/* @var $this PostQueueController */
/* @var $model PostQueue */
/* @var $form TbActiveForm */
?>
<?php

?>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
    'id'=>'post-queue-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'type' => 'horizontal',
    'action'=>Yii::app()->CreateUrl(Yii::app()->controller->id . '/update_image_quick/id/' . $model->id),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
));

?>







<div class="col-sm-2 pull-right">
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">Upload new Image</button>
</div>
<style>
    .without-border{
        border:0px;
        font-size:15px;
    }
</style>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">New Image</h4>
            </div>
            <div class="modal-body">


                        <?php echo $form->fileFieldGroup($model,'media_url',array(
                            'labelOptions' => array(
                                'class' => 'col-sm-2',
                            ),
                            'groupOptions' => array(
                                'id'=>'file_PostQueue',
                            ),
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-10',
                            ),

                        )); ?>



            </div>
            <div class="modal-footer">
                <div class="form-actions col-sm-4">
                    <?php $this->widget(
                        'booster.widgets.TbButton',
                        array(
                            'buttonType' => 'submit',
                     /*       'context' => 'info',*/
                            'icon'=>'fa fa-facebook',

                            'label' => $model->isNewRecord ? 'Upload to facebook' : 'Upload to facebook',
                            'htmlOptions' => array(
                                'class'=>'col-sm-12 without-border',
                                'name'=>'facebook'
                            )


                        )
                    ); ?>


                </div><div class="form-actions col-sm-4">
                    <?php $this->widget(
                        'booster.widgets.TbButton',
                        array(
                            'buttonType' => 'submit',
/*                            'context' => 'primary',*/
                            'icon'=>'fa fa-twitter',

                            'label' => $model->isNewRecord ? 'Upload to twitter' : 'Upload to twitter',
                            'htmlOptions' => array(
                                'class'=>'col-sm-12 without-border',
                                'name'=>'twitter'

                            )

                        )
                    ); ?>


                </div><div class="form-actions  col-sm-4">
                    <?php $this->widget(
                        'booster.widgets.TbButton',
                        array(
                            'buttonType' => 'submit',
/*                            'context' => 'danger',*/
                            'icon'=>'fa fa-instagram',

                            'label' => $model->isNewRecord ? 'Upload to instagram' : 'Upload to instagram',
                            'htmlOptions' => array(
                                'class'=>'col-sm-12 without-border',
                                'name'=>'instagram'

                            )

                        )
                    ); ?>


                </div><div class="form-actions  col-sm-4">
                    <?php $this->widget(
                        'booster.widgets.TbButton',
                        array(
                            'buttonType' => 'submit',
                            'context' => 'info',
                            'label' => $model->isNewRecord ? 'Upload to all' : 'Upload to all',
                            'htmlOptions' => array(
                                'class'=>'col-sm-12 buttons-top',
                                'name'=>'all_platforms'

                            ),

                        )
                    ); ?>


                </div>
                <div class="form-actions  col-sm-4">
                    <?php $this->widget(
                        'booster.widgets.TbButton',
                        array(
                            'buttonType' => 'submit',
                            'context' => 'primary',
                            'label' => $model->isNewRecord ? 'Upload image only' : 'Upload image only',
                            'htmlOptions' => array(
                                'class'=>'col-sm-12 buttons-top',
                                'name'=>'upload_only'

                            )

                        )
                    ); ?>


                </div> <div class="col-sm-4"><button type="button" class="btn btn-default  col-sm-12 buttons-top" data-dismiss="modal">Close</button></div>

            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>