<?PHP /* @var $data PostQueue */
$date = date_create($data->schedule_date);
$date = date_format($date,"Y-m-d  h:i A");
?>
<?php
if($data->is_posted ==3) {
    ?>
    <style>
        .timeline-body{
            padding-bottom: 0px;

        }
        .timeline-footer{
            margin-top:0px;
            padding:0px;
        }
    </style>
    <?php
}
?>
<script>
    window.App = {};



    $(window).ready(function (){


        $("#Edits_<?php echo $data->id ?>").click(function(){

            $(this).hide();
            $(this).parent().find('#edits_textarea_<?php echo $data->id; ?>').show();
            $(this).parent().find('.sub-button').show();

            App.edit_text = function(id){
                $inputs = $('#edits_textarea_<?php echo $data->id; ?>').val();
                $('#edits_textarea_<?php echo $data->id; ?>').keyup(function() {
                    $inputs = $(this).val();

                });

                console.log($inputs);
                $.post('<?PHP echo CController::createUrl('/postQueue/edit_text/'.$data->id) ?>',{id:id,post:$inputs}, function( data ) {

                    $("#Edits_<?php echo $data->id ?>").html(data);
                    $("#Edits_<?php echo $data->id ?>").show();

                    $('#edits_textarea_<?php echo $data->id; ?>').hide();
                    $('#edit_text<?php echo $data->id ?>').hide();
                });

            };

            $(document).click(function(e) {

                if( e.target.id != 'edits_textarea_<?php echo $data->id; ?>' && e.target.id != "Edits_<?php echo $data->id ?>" && e.target.id != 'edit_text<?php echo $data->id ?>' ) {
                    $("#Edits_<?php echo $data->id ?>").show();
                    $('#edits_textarea_<?php echo $data->id; ?>').hide();
                    $('#edit_text<?php echo $data->id ?>').hide();

                }

            });



        });



    });

</script>

<ul class="timeline timeline2">
    <li>
        <?php
        if($data->platforms->title=="Facebook")
            echo "<i class=\"fa fa-video-camera bg-blue\"></i>";
        elseif($data->platforms->title=="Twitter")
            echo "<i class=\"fa fa-video-camera bg-aqua\"></i>";
        else
            echo "<i class=\"fa fa-video-camera bg-purple\"></i>";
        ?>
        <div class="timeline-item" style="<?php if($data->is_posted ==2){ ?>background-color:#efd8d8;color: #424242; <?php } ?>">
            <span class="time"><i class="fa fa-clock-o"></i>

                  <b>    <?PHP if(Yii::app()->controller->id == 'postQueue' and Yii::app()->controller->action->id != 'mainPosted' and Yii::app()->controller->action->id != 'mainPinned') { ?>

                          <?php

                          $this->widget('booster.widgets.TbEditableField',
                              array(

                                  'name' => 'schedule_date',
                                  'type' => 'combodate',
                                  'model' => $data,
                                  'attribute' => 'schedule_date',
                                  'url' => $this->createUrl('/postQueue/edit'),
                                  'placement' => 'bottom',
                                  'mode' => 'inline',
                                  'format' => 'YYYY-MM-DD HH:mm', //in this format date sent to server
                                  'viewformat' => 'MMM DD, YYYY HH:mm', //in this format date is displayed
                                  'template' => 'DD / MMM / YYYY HH:mm', //template for dropdowns
                                  'combodate' => array('minYear' => date('Y'), 'maxYear' => date('Y', strtotime('+5 years'))),

                              )
                          );

                      }else{
                          echo $data->schedule_date;
                      }            ?></b>
            </span>

            <h3 class="timeline-header"><?php echo CHtml::link($data->catgory->title,array('category/view','id'=>$data->catgory->id)) ?>
                <?php
                if($data->is_scheduled !=0){

                if($data->pinned ==0) {
                    ?>
                    <img id="img_pinned_<?php echo $data->id; ?>" data-toggle="tooltip" title="Pin" style="margin-left:10px;cursor: pointer;"
                         onclick="javascript:App.edit_pined('<?php echo $data->id ?>',0)"
                         src="<?php echo Yii::app()->baseUrl . '/image/pin_black.png'; ?>" width="20" height="20"/>
                    <?php
                }

                else{
                    ?>
                    <img id="img_pinned_<?php echo $data->id; ?>" data-toggle="tooltip" title="Unpin" style="margin-left:10px;cursor: pointer;"  onclick="javascript:App.edit_pined('<?php echo $data->id ?>',1)" src="<?php echo Yii::app()->baseUrl.'/image/note2.png'; ?>" width="20" height="20"/>
                <?php } }?>
            </h3>
            <div class="timeline-body arabic-direction">
                <?PHP if(Yii::app()->controller->id == 'postQueue' and Yii::app()->controller->action->id != 'mainPosted' and Yii::app()->controller->action->id != 'mainPinned') { ?>

                    <p class="timeline_p_video" id="Edits_<?php echo $data->id ?>" title="click to edit" data-toggle="tooltip"><?=trim($data->get_hashtags_urls($data->platforms->title,$data->post));?></p>
                <?php }else{
                    ?>
                    <p class="timeline_p_video"> <?=trim($data->get_hashtags_urls($data->platforms->title,$data->post));?></p>
                    <?php
                } ?>
<textarea cols='30' rows='20' name='PostQueue[post]' class='timeline_p_video form-control' id='edits_textarea_<?php echo $data->id ?>' style="display:none;" ><?=trim(strip_tags($data->post));?></textarea>
                <?php echo CHtml::button('Edit text', array('style' => 'display:none;', 'class' => 'btn btn-primary btn-flat btn-xs pull-right sub-button', 'onClick' => 'javascript:App.edit_text('.$data->id.')', 'id' => 'edit_text' . $data->id)); ?>

                <video id="my-video" class="video-js vid-class vjs-default-skin vjs-big-play-centered embed-responsive-item" class="video-class" controls preload="auto"   data-setup="{}">
                    <source src="<?php echo $data->media_url ?>" type='video/mp4'>

                </video>

                <input type="hidden" value="Video" />

                <?php
                if($data->is_posted ==2){ ?>
                    <div class="errors col-sm-12">
                        <?php
                        echo '<p style="color:red;">'.$data->errors.'</p>';
                        ?>
                    </div>
                <?php } ?>

            </div>
            <div class="timeline-footer" style="margin-top:29px;">
                <?php
                if(($data->is_posted==2 || $data->is_posted==3) and Yii::app()->user->getState('type') == 'super_admin'){
                    echo CHtml::button('restore_posted', array('class' => 'btn btn-danger btn-flat btn-xs', 'onClick' => 'javascript:App.restore_posted("#restore_posted' . $data->id . '")', 'id' => 'restore_posted' . $data->id, 'data-url' => Yii::app()->createUrl("postQueue/restore_posted/" . $data->id)));
                }
                if(Yii::app()->controller->action->id != 'mainPinned'){
                ?>
                <?php
                if($data->is_posted ==3){
                    echo "<div class=''><img src='".Yii::app()->baseUrl.'/image/posting.gif'."' class='img-responsive img-thumbnail'/></div>";
                }else {
                ?>
                    <?php echo CHtml::link('<i class="fa fa-eye" style="font-size: 12px"> Show</i>',Yii::app()->createUrl('/postQueue/view/'.$data->id), array('style'=>'', 'class'=>'btn btn-success btn-flat btn-xs')); ?>
                    <?PHP if(Yii::app()->controller->id == 'postQueue' and Yii::app()->controller->action->id != 'mainPosted') { ?>



                    <?php echo CHtml::link('<i class="fa fa-pencil-square-o" style="font-size: 12px">  Edit</i>',Yii::app()->createUrl('/postQueue/update/'.$data->id), array('style'=>'', 'class'=>'btn btn-primary btn-flat btn-xs')); ?>


                    <?PHP if(Yii::app()->controller->id == 'postQueue' and Yii::app()->controller->action->id == 'main'){ ?>

                        <?php echo  CHtml::button('Push Now', array('class'=>'btn btn-info btn-flat btn-xs','onClick'=>'javascript:App.push_post("#push_post'.$data->id.'")','id'=>'push_post'.$data->id,'data-url'=>Yii::app()->createUrl("postQueue/push_post/".$data->id))) ; ?>

                        <?php echo  CHtml::button('UnSchedule', array('style'=>'','class'=>'btn btn-danger btn-flat btn-xs','onClick'=>'javascript:App.remove_post("#remove_post'.$data->id.'")','id'=>'remove_post'.$data->id,'data-url'=>Yii::app()->createUrl("postQueue/delete/".$data->id))) ; ?>

                    <?php  } ?>

                    <?PHP if(Yii::app()->controller->id == 'postQueue' and Yii::app()->controller->action->id == 'mainUn'){ ?>
                        <?php echo CHtml::button('Post', array('style'=>'','class'=>'btn btn-danger btn-flat btn-xs','onClick'=>'javascript:App.activate_post("'.$data->id.'","'.$data->platform_id.'","'.$data->parent_id.'")','id'=>'activate_post'.$data->id,'data-url'=>Yii::app()->createUrl("postQueue/activatePost/".$data->id))); ?>
                    <?PHP } } ?>
                <?PHP if(Yii::app()->controller->id == 'postQueue' and Yii::app()->controller->action->id == 'mainPosted') { ?>

                    <?php echo  CHtml::button('RePost now', array('class'=>'btn btn-info btn-flat btn-xs','onClick'=>'javascript:App.re_post("#re_post'.$data->id.'")','id'=>'re_post'.$data->id,'data-url'=>Yii::app()->createUrl("postQueue/re_post/".$data->id))) ; ?>
                    <?php echo CHtml::link('<i class="fa fa-pencil-square-o" style="font-size: 12px">  Edit and RePost</i>',Yii::app()->createUrl('/postQueue/update_posted/'.$data->id), array('style'=>'', 'class'=>'btn btn-primary btn-flat btn-xs')); ?>
                    <?php
                }}}
                ?>

            </div>
        </div>
        <input type="hidden" value="<?php echo $date ?>" id="time-<?php echo $data->id; ?>"

    </li>
</ul>
