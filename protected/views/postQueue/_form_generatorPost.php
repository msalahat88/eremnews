<?php
/* @var $this GeneratorPostController */
/* @var $model GeneratorPost */
/* @var $form TbActiveForm */
?>
<script>
    $(document).ready(function(){
        $("#GeneratorPost_submit_to_all").click(function(){
            if($(this).is(':checked')){
                $('.Platforms').prop('disabled',true);
            }else{
                $('.Platforms').prop('disabled',false);
            }
        });
        $('#GeneratorPost_time').datetimepicker({
            format:'Y-m-d H:i',step:5,
            minDate: new Date('Y-m-d H:i')
        });

    })
</script>
<div class="form" style="margin-top: 50px">


    <?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'generator-post-form',
        'enableAjaxValidation' => true,
        'type' => 'horizontal',
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>
    <?php echo CHtml::hiddenField('GeneratorPost[image]',$model->image,array('name'=>'','id'=>'ytGeneratorPost_image'))?>
    <?php
    echo $form->urlFieldGroup($model, 'link', array('labelOptions'=>array(
			'class'=>'col-sm-2'
		),
		'wrapperHtmlOptions' => array(
			'class' => 'col-sm-10',
		),
    ));
    ?>


    <?php echo $form->textFieldGroup($model, 'time',
        array(
            'labelOptions' => array(
                'class' => 'col-sm-2'
            ),
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-10',
            ),
          //  'append' => $form->checkboxGroup($model,'now',array('groupOptions'=>array('style'=>'    margin: -4px;'))),
            'groupOptions' => array(

            ),
        )
    );
    ?>

    <?php
    echo $form->checkboxGroup($model, 'pinned', array(
        'widgetOptions' => array(
            'htmlOptions' => array(
                //'checked' => 'checked'
            ),
        ),
        'label'=>null,
        'Class_span'=>'col-sm-0',
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-4 col-sm-push-2',

        ),
    ));
    ?>

    <hr>



    <?php
    echo $form->checkboxGroup($model, 'submit_to_all', array(
        'widgetOptions' => array(
            'htmlOptions' => array(
                'checked' => 'checked'
            ),
        ),
        'label'=>null,
        'Class_span'=>'col-sm-0',
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-4 col-sm-push-2',

        ),
    ));
    ?>


    <div id="platforms">
        <?php
        $model->platforms=array('Facebook'=>true,'Twitter'=>true,'Instagram'=>true);

        echo $form->checkboxListGroup($model, 'platforms', array(
            'widgetOptions' => array(
                'data' => CHtml::listData(Platform::model()->findAll('deleted=0'), 'id', 'title'),
                'htmlOptions' => array(
                    'class' => 'Platforms',
                    'disabled' => true

                ),

            ),
            'labelOptions' => array(
                'class' => 'col-sm-2 platforms'
            ),
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-8 platforms',
            ),
        ));
        ?>
    </div>
<div id="link-img">
    <div id="loading" style="display: none;margin:0 auto;" align="center">
        <p><img src="<?php echo Yii::app()->baseUrl."/image/updateimg.gif" ?>" class="loading-imge"/></p>
    </div>
</div>
    <div class="form-actions  pull-right" style="margin-bottom: 20px;margin-right:20px;">
        <?php $this->widget(
            'booster.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'context' => 'success',
                'label' => 'Post now',
                'htmlOptions'=>array('style'=>'    margin-right: 12px;')
            )
        );
        ?>


        <?php $this->widget(
            'booster.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'context' => 'primary',
                'label' => 'schedule',
                'htmlOptions'=>array('style'=>'    margin-right: 12px;')
            )
        );
        ?>

    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->