<div class="col-sm-5 verticalLine">


    <?php

    if($model->platform_id==2) {
        echo '<input type="hidden" id="' . $model->id . '" class="twitter_hidden_inputs" />';
    }
    if($model->platform_id==1) {
        echo '<input type="hidden" id="' . $model->id . '" class="Facebook_hidden_inputs" />';
    }  if($model->platform_id==3) {
        echo '<input type="hidden" id="' . $model->id . '" class="Instagram_hidden_inputs" />';
    }
    ?>
    <?php
    if ($model->type == 'Image')
        echo CHtml::image($model->media_url, "image", array('class' => 'img-responsive chngerimg','style'=>'margin:0 auto;','id'=>'changer'.$model->id));
    elseif ($model->type == 'Video') {
        ?>
        <video id="my-video" class="video-js vjs-default-skin vjs-big-play-centered embed-responsive-item" controls
               preload="auto" data-setup="{}" style="width:100%;max-height:200px;margin:0 auto;">
            <source src="<?php echo $model->media_url ?>" type='video/mp4'>
        </video>
        <?PHP
    } elseif ($model->type == 'Preview') {
        ?>
        <div class="col-sm-12" style="background-color:#fff;padding-top:12px;">
            <?php echo CHtml::link(CHtml::image($model->media_url, '', array('class' => 'img-thumbnail img-responsive img-class chngerimg','id'=>'changer'.$model->id)), $model->link, array('target' => '_blank')) ?>
        </div>
        <?php if(!empty($model->news)){ ?>
        <div class="col-sm-12" style="background-color:#fff;padding-top:12px;margin-bottom: 20px;">



            <?php echo CHtml::link('<h6>' . $model->news->title . '</h6>' . '<h5><b>' . $this->shorten($model->news->description, 200) . '</b></h5>', $model->link, array('target' => '_blank', 'style' => 'color:#636262;')); ?>

            </div>
        <?php }
    }
    ?>
    <div class="row" style="margin-top: 12px">
        <div class="col-sm-12" style="text-align: center;">
            <?php
            if($model->type == 'Image')
                echo '<i class="fa fa-camera"></i>';
            elseif($model->type == 'Video'){
                echo '<i class="fa fa-video-camera"></i>';
            }elseif($model->type == 'Preview'){
                echo '<i class="fa fa-link"></i>';
            }elseif($model->type == 'Text'){
                echo '<i class="fa fa-file-text-o"></i>';
            }
            ?>

            <?php if($model->link != null){  ?>
                <?php echo CHtml::link('News Link',$model->link,array('class'=>'btn btn-link','target' => '_blank')); ?>
            <?php } ?>
            <input type="hidden" value="<?php echo $model->schedule_date ?>" id="time-<?php echo $model->id; ?>"
            <p> <i class="fa fa-clock-o"></i>
                <?php
                $this->widget('booster.widgets.TbEditableField',
                    array(

                        'name'=>'schedule_date',
                        'type'        => 'combodate',
                        'model'       => $model,
                        'attribute'   => 'schedule_date',
                        'url'         => $this->createUrl('/postQueue/edit'),
/*                        'placement'   => 'right',*/
                        'format'      => 'YYYY-MM-DD HH:mm', //in this format date sent to server
                        'viewformat'  => 'MMM DD, YYYY HH:mm', //in this format date is displayed
                        'template'    => 'DD / MMM / YYYY HH:mm', //template for dropdowns
                        'combodate'   => array('minYear' => 2016, 'maxYear' => 2020),

                    )
                )
                ?>
<!--                --><?php /*echo  $model->schedule_date; */?>
                <?php
                if(isset($model->platform_id))
                    echo '<i class="fa fa-'.strtolower($model->platforms->title).' " style="font-size:22px;"></i>';
                ?>
                <?php
                if(isset($model->catgory_id))
                    echo CHtml::link($model->catgory->title, $model->catgory->url, array('target' => '_blank'));
                ?>
                <?php  if ($model->is_posted == 1) {
           echo  CHtml::button('Re post', array('class'=>'btn btn-info btn-flat btn-xs','onClick'=>'javascript:App.re_post("#re_post'.$model->id.'")','id'=>'re_post'.$model->id,'data-url'=>Yii::app()->createUrl("postQueue/re_post/".$model->id))) ; ?>
                <?php echo CHtml::link('<i class="fa fa-pencil-square-o" style="font-size: 12px">  Edit and re post</i>',Yii::app()->createUrl('/postQueue/update_posted/'.$model->id), array('style'=>'', 'class'=>'btn btn-primary btn-flat btn-xs'));              }else{ ?>
            <div style="<?php if($model->type=='Image' or $model->type == 'Video'){ ?> text-align:center;margin-top:5px; <?php }else{ ?> text-align:center;margin-top:5px; <?php } ?>">

                <?php echo CHtml::link('<i class="fa fa-pencil-square-o" style="font-size:11px;margin:0 auto;display:block">  Edit</i>',Yii::app()->createUrl('/postQueue/update/'.$model->id), array('style'=>'max-height:24px;', 'class'=>'input-sm btn  btn-primary')); ?>

                <?PHP if(Yii::app()->controller->id == 'postQueue' and $model->is_scheduled == 1){ ?>
                    <?php echo  CHtml::button('Push Now', array('style'=>'max-height:24px;font-size:12px;line-height:5px;','class'=>' input-sm btn  btn-info','onClick'=>'javascript:App.push_post("#push_post'.$model->id.'")','id'=>'push_post'.$model->id,'data-url'=>Yii::app()->createUrl("postQueue/push_post/".$model->id))) ; ?>

                    <?php echo  CHtml::button('UnSchedule', array('style'=>'max-height:24px;font-size:12px;line-height:5px;margin-top:1.5px;','class'=>' input-sm btn  btn-danger','onClick'=>'javascript:App.remove_post("#remove_post'.$model->id.'")','id'=>'remove_post'.$model->id,'data-url'=>Yii::app()->createUrl("postQueue/delete/".$model->id))); ?>
                <?php  } ?>
                <?PHP if(Yii::app()->controller->id == 'postQueue' and $model->is_scheduled ==0){ ?>
                    <?php echo CHtml::button('Post', array('style'=>'max-height:24px;font-size:12px;line-height:5px;','class'=>' input-sm btn  btn-warning','onClick'=>'javascript:App.activate_post("'.$model->id.'","'.$model->platform_id.'","'.$model->parent_id.'")','id'=>'activate_post'.$model->id,'data-url'=>Yii::app()->createUrl("postQueue/activatePost/".$model->id))); ?>
                <?PHP }  ?>
            </div>
        <?php } ?>
            </p>
        </div>
    </div>
</div>
<div class="col-sm-7">
    <div class="arabic-direction" style="text-align:center;">
        <h4 class="">


            <?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
                'id'=>'post-queue-form',
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // There is a call to performAjaxValidation() commented in generated controller code.
                // See class documentation of CActiveForm for details on this.
                'type' => 'inline',
                'action'=>Yii::app()->CreateUrl(Yii::app()->controller->id . '/update_text_quick/id/' . $model->id),
            ));

            ?>
  <!--      --><?php

           echo  $form->textareaGroup($model,'post',array('widgetOptions'=>array(
                'htmlOptions'=>array(
                    'minlength' => 1, 'rows' => 20, 'cols' => 30,
                    'class'=>'textarea_edit'
                )),
            ))

?>
            <div class="col-sm-12 buttons-top" style="margin-bottom:5px;">
            <?php $this->widget(
                'booster.widgets.TbButton',
                array(
                    'buttonType' => 'submit',
                    'context' => 'info',

                    'label' => $model->isNewRecord ? 'Update text' : 'Update text',
                    'htmlOptions' => array(
                        'class'=>'col-sm-12 without-border update_text',
                        'style'=>'display:none;'

                    )


                )
            ); ?>
            </div>

            <?php $this->endWidget(); ?>
            </h4>
    </div>
</div>

