<?php
/* @var $this ProfilePicController */
/* @var $model ProfilePic */
/* @var $form TbActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'profile-pic-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
	'type' => 'horizontal',
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
	<a href="#"  onclick="guiders.show('firstProfile');return false"    ><i class="fa fa-question-circle"></i></a>

	<?php echo $form->errorSummary($model); ?>
	<?php echo $form->textFieldGroup($model,'title',array('prepend' => '<i class="fa fa-text-width"></i>')); ?>
	<a href="#"  onclick="guiders.show('second');return false"    ><i class="fa fa-question-circle"></i></a>

	<?php echo $form->fileFieldGroup($model,'media_url',array('size'=>60,'maxlength'=>255,'prepend' => '<i class="fa fa-file"></i>')); ?>
	<a href="#"  onclick="guiders.show('third');return false"    ><i class="fa fa-question-circle"></i></a>

	<div class="form-group">
		<div class="col-sm-3"><?php echo $form->labelEx($model,'schedule_date'); ?></div>
		<div class="col-sm-8">
			<?php echo $form->textField($model,'schedule_date',array('type'=>'text','id'=>'datetimepicker_format_end_value','class'=>'form-control')); ?>

			<script>
				$( document ).ready(function() {
					$('#datetimepicker_format_end_value').datetimepicker({
						format:'Y-m-d H:i',step:5,

						minDate: new Date('Y-m-d H:i'),

					});
				});

			</script>
			<?php echo $form->error($model,'schedule_date'); ?>
		</div>


	</div>
	<?php
	echo $form->dropDownListGroup($model,'page_index',array(
		'widgetOptions'=>array(
			'data'=>array('main'=>'main','other'=>'Sport')
		)
	));
	?>
	<a href="#"  onclick="guiders.show('fourth');return false"    ><i class="fa fa-question-circle"></i></a>

	<?php
	if($model->id != null){
	echo $form->dropDownListGroup($model,'platform_id',array(
		'widgetOptions'=>array(
			'data'=>CHtml::listData(Platform::model()->findAll('title!="Instagram"'),'id','title'),
			'empty'=>'Choose One'
		),
		'prepend' => '<i class="fa fa-tags"></i>'
	)); }else{
		echo $form->checkboxListGroup($model,'platform_id',array(
			'widgetOptions'=>array(
				'data'=>CHtml::listData(Platform::model()->findAll('title!="Instagram"'),'id','title'),
			),
			'prepend' => '<i class="fa fa-tags"></i>',

		));
	} ?>
	<div class="form-actions  pull-right" style="margin-bottom: 20px">
		<?php $this->widget(
			'booster.widgets.TbButton',
			array(
				'buttonType' => 'submit',
				'context' => 'primary',
				'label' => $model->isNewRecord ? 'Create' : 'Save'
			)
		); ?>

	</div>
<?php $this->endWidget(); ?>
</div><!-- form -->
	<!--Tour-->

<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'firstProfile',
		'title'        => 'Title',
		'next'         => 'second',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Profile Pic title',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#ProfilePic_title',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'second',
		'title'        => 'Media',
		'next'         => 'third',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'The image for the profile pic',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#ProfilePic_media_url',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'third',
		'title'        => 'Scheduled date',
		'next'         => 'fourth',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Time for this profile pic to be uploaded',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#ProfilePic_schedule_date',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'fourth',
		'title'        => 'Platform',
		'next'         => 'fifth',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'The platform that you want to change its profile pic',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#ProfilePic_platform_id',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'fifth',
		'title'        => 'Create',
		'next'         => 'six',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('fourth');}"
			),
			array(
				'name'   => 'Next',

			),


			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?

		'description'   => 'Press to create profile pic',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#yw3',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php
$continueTour = Yii::app()->createUrl('profilePic/admin',array('#' => 'guider=sex'));
$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'six',
		'title'        => '',

		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('fifth');}"
			),
			array('name'=>'Continue ','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$continueTour';}"),



			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?

		'description'   => 'You will find your created profile pic in here',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#ProfileManager',
		'position'      => 3,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
