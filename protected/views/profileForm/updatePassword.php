<?php
/* @var $this ProfileFormController */
/* @var $model ProfileForm */



?>
<?php
if(Yii::app()->user->id != $_GET['id']){
    ?>
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">

                    <div class="col-xs-12  alert alert-danger">
                        <div class="col-xs-6  pull-left">
                            <h2>Invalid request</h2>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
}else { ?>

    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <?php
                        if (isset($_POST['updated'])) {
                            echo '<div class="alert alert-info">Updated</div>';
                        }
                        ?>
                        <div class="col-sm-9"><h2>
                                <?php $this->widget(
                                    'booster.widgets.TbButtonGroup',
                                    array(
                                        'size' => 'small',
                                        'context' => 'info',
                                        'buttons' => array(
                                            array(
                                                'label' => 'Update profile',
                                                'buttonType' => 'link',
                                                'url' => array('/profileForm/update/', 'id' => Yii::app()->user->id)
                                            ),
                                        ),
                                    )
                                ); ?></h2></div>

                    </div>
                    <div class="box-body">
                        <?php $this->renderPartial('_passwordForm', array('model' => $model)); ?>
                    </div>
                </div>
            </div>
    </section>
    <?php
}
?>