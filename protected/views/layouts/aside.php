<?PHP if (!Yii::app()->user->isGuest): ?>
    
    <style>
        ul.sidebar-menu li a{
            overflow:hidden;
        }

        ul.sidebar-menu li.treeview:hover{
            overflow: hidden;
        }

    </style>
    <aside class="main-sidebar">
        <section class="sidebar">
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo User::model()->get_image() ?>" class="img-circle"
                         alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?php echo Yii::app()->user->name?></p>
                    <a href="<?php echo Yii::app()->getBaseUrl(true)?>"><i
                            class="fa fa-circle text-success"></i>Home<?php /*echo Yii::app()->controller->id */?></a>
                </div>
            </div>

            <?PHP
            if(isset(Yii::app()->session['mains'])){

                $main_URL = array('/'.Yii::app()->session['mains']);
                if(Yii::app()->session['mains'] == 'postQueue/mainUn'){
                    $main_name = 'Unscheduled Posts';
                }elseif(Yii::app()->session['mains'] == 'postQueue/main'){
                    $main_name = 'Post Queue';
                }elseif(Yii::app()->session['mains'] == 'postQueue/mainPosted'){
                    $main_name = 'Pushed post';
                }elseif(Yii::app()->session['mains'] == 'postQueue/mainPinned'){
                    $main_name = 'Pinned post';
                }

            }else{
                $main_URL = array('/postQueue/main');
                $main_name = 'Post Queue';


            }

            Yii::import('application.modules.user.UserModule');

            $this->widget('zii.widgets.CMenu', array(
                'activeCssClass' => 'active',
                'activateParents' => true,
                'encodeLabel' => false,
                'items' => array(
                    array(
                        'label' => 'MAIN NAVIGATION',
                        'itemOptions' => array(
                            'class' => 'header'
                        ),
                        'linkOption' => array('')
                    ),
                    array(
                        'label' => '<i class="fa fa-dashboard"></i>'.$main_name,
                        'url' => $main_URL,
                        'itemOptions' => array(
                            'id'=>'PostQueueController',
                        ),
                        'linkOption' => array('target' => '_blank'),
                        'active' => Yii::app()->controller->id == 'postQueue' and Yii::app()->controller->action->id == 'main',
                    ),
                    array(
                        'label' => '<i class="fa fa-info"></i>Library<i class="fa fa-angle-left pull-right"></i>',
                        'url' => '#',


                        'active' => (Yii::app()->controller->id == 'postQueue' or Yii::app()->controller->id == 'managePostTemplate' or Yii::app()->controller->id == 'hashtag' or Yii::app()->controller->action->id == 'create')   or Yii::app()->controller->id == 'fillerData' or Yii::app()->controller->id == 'jobs' or Yii::app()->controller->id == 'coverPhoto' or Yii::app()->controller->id == 'profilePic' or Yii::app()->controller->id == 'postTemplate'or Yii::app()->controller->id == 'pdf' or Yii::app()->controller->id == 'news',

                        'items' => array(
                            array(
                                'label' => '<i class="fa fa-windows"></i>Create One Time Post',
                                'url' => array('/postQueue/create'),
                                'itemOptions' => array(
                                    'id'=>'onetimepost',
                                ),
                                'linkOption' => array('target' => '_blank'),
                                'active' =>Yii::app()->controller->id == 'postQueue' and  Yii::app()->controller->action->id == 'create',
                            ),
                            array(
                                'label' => '<i class="fa fa-terminal"></i>Mange Post Template',
                                'url' => array('/postTemplate/admin'),
                                'itemOptions' => array(
                                    'id'=>'posttemplates',
                                ),
                                'linkOption' => array('target' => '_blank'),
                                'active' => Yii::app()->controller->id == 'postTemplate' ,
                            ),



                            array(
                                'label' => '<i class="fa fa-filter"></i>Filler Data',
                                'url' => array('/fillerData/admin'),
                                'itemOptions' => array(
                                    'id'=>'FillerDataitem',
                                ),
                                'linkOption' => array('target' => '_blank'),
                                'active' => Yii::app()->controller->id == 'fillerData',
                            ),
                         /*   array(
                                'label' => '<i class="fa fa-filter"></i>Travel post',
                                'url' => array('/jobs/admin'),
                                'itemOptions' => array(
                                    'id'=>'Jobsitem',
                                ),
                                'linkOption' => array('target' => '_blank'),
                                'active' => Yii::app()->controller->id == 'jobs',
                            ),*/

                            array(
                                'label' => '<i class="fa fa-hashtag"></i> Hashtag',
                                'url' => array('/hashtag/admin'),
                                'itemOptions' => array(
                                    'id'=>'HashtagManager',
                                ),
                                'linkOption' => array('target' => '_blank'),
                                'active' => Yii::app()->controller->id == 'hashtag',
                            ),
                            array(
                                'label' => '<i class="fa fa-image"></i> Cover Photo',
                                'url' => array('/coverPhoto/admin'),
                                'itemOptions' => array(
                                    'id'=>'CoverManager',
                                ),
                                'linkOption' => array('target' => '_blank'),
                                'active' => Yii::app()->controller->id == 'coverPhoto' and  (Yii::app()->controller->action->id ==  'create' or Yii::app()->controller->action->id ==  'admin' or Yii::app()->controller->action->id ==  'update'),
                            ),
                            array(
                                'label' => '<i class="fa fa-image"></i> Profile Pic',
                                'url' => array('/profilePic/admin'),
                                'itemOptions' => array(
                                    'id'=>'CoverManager',
                                ),
                                'linkOption' => array('target' => '_blank'),
                                'active' => Yii::app()->controller->id == 'profilePic' and  (Yii::app()->controller->action->id ==  'create' or Yii::app()->controller->action->id ==  'admin' or Yii::app()->controller->action->id ==  'update'),
                            ),



                            array(
                                'label' => '<i class="fa fa-file-pdf-o"></i>Download PDF',
                                'url' => array('/pdf/index'),
                                'itemOptions' => array(
                                    'id'=>'PDFtour',
                                ),
                                'linkOption' => array('target' => '_blank'),
                                'active' => Yii::app()->controller->id == 'pdf'  ,
                            ),
                        ),


                        'itemOptions' => array(
                            'class' => 'treeview'
                        ),
                        'submenuOptions' => array(
                            'class' => 'treeview-menu'
                        )
                    ),
                    array(
                        'label' => '<i class="fa fa-briefcase"></i>Management<i class="fa fa-angle-left pull-right"></i>',
                        'url' => '#',
                        'active' => Yii::app()->controller->id  == 'default' or Yii::app()->controller->id  == 'split_report' or isset(Yii::app()->controller->module->id)?Yii::app()->controller->module->id:null  == 'management' or Yii::app()->controller->id == 'category' or Yii::app()->controller->id == 'emails' or Yii::app()->controller->id == 'subCategories' or Yii::app()->controller->id == 'settings' or Yii::app()->controller->id == 'news',

                        'items'=>  array(
                            array(
                                'label' => '<i class="fa fa-cog "></i>Settings',
                                'url' => array('/settings'),
                                'itemOptions' => array(
                                    'id'=>'Settingstour',
                                ),
                                'linkOption' => array('target' => '_blank'),
                                'active' => Yii::app()->controller->id == 'settings'  ,
                                'visible'=>Yii::app()->user->isAdmin or Yii::app()->user->getState('type')=='super_admin',
                            ),
                            array(
                                'label' => '<i class="fa fa-user "></i>Users',
                                'url' => array('/management/user/admin'),
                                'visible'=>Yii::app()->user->isAdmin or Yii::app()->user->getState('type')=='super_admin',

                                'linkOption' => array('target' => '_blank'),
                                'active' => Yii::app()->controller->id == 'user',
                            ),
                            array(
                                'label' => '<i class="fa fa-envelope  "></i>Emails',
                                'url' => array('/emails/admin/'),
                                'itemOptions' => array(
                                    'id'=>'emailTour',
                                ),
                                'linkOption' => array('target' => '_blank'),
                                'active' => Yii::app()->controller->id == 'emails',
                            ),
                            array(
                                'label' => '<i class="fa fa-line-chart  "></i>Dashboard',
                                'url' => array('/management/'),

                                'linkOption' => array('target' => '_blank'),
                                'active' => Yii::app()->controller->id  == 'default',
                            ),
                            array(
                                'label' => '<i class="fa fa-pagelines"></i>Sections',
                                'url' => array('/category/admin'),
                                'itemOptions' => array(
                                    'id'=>'sectionsItems',
                                ),
                                'linkOption' => array('target' => '_blank'),
                                'active' => Yii::app()->controller->id == 'category' or Yii::app()->controller->id == 'news',
                                'visible'=>Yii::app()->user->isAdmin or Yii::app()->user->getState('type')=='super_admin',
                            ),
                          /*  array(
                                'label' => '<i class="fa fa-pagelines"></i>Split report',
                                'url' => array('/split_report/'),
                                'linkOption' => array('target' => '_blank'),
                                'active' => Yii::app()->controller->id == 'split_report',
                            ),*/
                        ),
                        'itemOptions' => array(
                            'class' => 'treeview'
                        ),
                        'submenuOptions' => array(
                            'class' => 'treeview-menu'
                        )
                    ),
                ),
                'htmlOptions' => array(
                    'class' => 'sidebar-menu'
                )
            )); ?>

        </section>
    </aside>
<?PHP endif; ?>

<?php

if( Yii::app()->controller->id == 'settings') {

    $this->widget('ext.eguiders.EGuider', array(
            'id' => 'first',
            'next' => 'SettingsTour',
            'title' => 'Tour',
            'buttons' => array(
                array('name' => 'Next'),
                array('name' => 'Exit', 'onclick' => "js:function(){guiders.hideAll();}")
            ), 'description' => '<b>Ready to start the tour press Start</b>',
            'overlay' => true,
            'xButton' => true,
            // look here !! 'show' is true, so that means this guider will be
            // automatically displayed when the page loads
            'autoFocus' => true
        )
    );
}
?>
