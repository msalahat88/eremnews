<?php
/* @var $this SubCategoriesController */
/* @var $model SubCategories */

$this->pageTitle = "Sub sections | Update";

$this->breadcrumbs=array(
	'Sub sections'=>array('admin'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);
?>
<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-md-9">
						<?PHP
						$this->widget(
							'booster.widgets.TbButtonGroup',
							array(
								'size' => 'small',
								'context' => 'info',
								'buttons' => array(
									array(
										'label' => 'Action',
										'items' => array(
											array('label' => 'Delete', 'url' => '#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
											array('label' => 'Manage', 'url'=>array('admin'))
										)
									),
								),/*'htmlOptions'=>array(
								'class'=>'pull-right	'
							)*/
							)
						);

						?></div>
					<div class="col-md-3" style=" text-align: left;">
					 <?php echo Yii::app()->params['statement']['previousPage']; ?>




					</div>
				</div>
				<div class="box-body">
					<?php $this->renderPartial('_form', array('model'=>$model)); ?>
				</div>
			</div>
		</div>
		</div>
</section>
