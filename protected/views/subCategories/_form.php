<?php
/* @var $this SubCategoriesController */
/* @var $model SubCategories */
/* @var $form TbActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'sub-categories-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
	'type' => 'horizontal',
)); ?>
	<a href="#"  onclick="guiders.show('firstSub');return false"    ><i class="fa fa-question-circle"></i></a>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'title'); ?>

	<a href="#"  onclick="guiders.show('secondSub');return false"    ><i class="fa fa-question-circle"></i></a>

	<?php echo $form->dropDownListGroup($model,'category_id',array(
		'widgetOptions'=>array(
			'data'=>CHtml::listData(Category::model()->findAll('deleted=0'),'id','title'),
			'empty'=>'Choose One',
			'htmlOptions'=>array(		"disabled"=>"disabled",	'readonly'=>true)
		),

	)); ?>


	<div class="col-sm-12">
		<div class="col-sm-5">
			<div class="col-sm-2  col-sm-offset-7"  style="margin-top:10px;">
				<a href="#"  onclick="guiders.show('thirdSub');return false"    ><i class="fa fa-question-circle"></i></a>
			</div>
			<div class="col-sm-3  col-sm-pull-1">
				<?php echo $form->checkboxGroup($model,'active'); ?>
			</div>
		</div>
	</div>
	<div class="form-actions  pull-right" style="margin-bottom: 20px">
		<?php $this->widget(
			'booster.widgets.TbButton',
			array(
				'buttonType' => 'submit',
				'context' => 'primary',
				'label' => $model->isNewRecord ? 'Create' : 'Save'
			)
		); ?>


	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
	<!--Tour-->
<?php
$createLink = Yii::app()->createUrl('subCategories',array('#' => 'guider=firstSection'));

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'firstSub',
		'title'        => 'Title',
		'next'         => 'second',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'The title of the sub section',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#SubCategories_title',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'secondSub',
		'title'        => 'Section',
		'next'         => 'third',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'The section where this sub section was constructed from',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#SubCategories_category_id',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'thirdSub',
		'title'        => 'Active',
		'next'=>'fourth',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'The status of this category if its checked the program will get posts from this section',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#SubCategories_active',
		'position'      => 11,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php
$finished = Yii::app()->createUrl('subCategories',array('#' => 'guider=third'));

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'fourth',
		'title'=>'Finally',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('third');}"
			),
			array('name'=>'Next','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$finished';}"),


			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Your item will be there ',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#subsectionsItems',
		'position'      => 3,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
