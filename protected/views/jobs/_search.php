<?php
/* @var $form TbActiveForm */
$form=$this->beginWidget('booster.widgets.TbActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'id'=>'form-visible',
)); ?>
    <div class="row">
        <div class="col-sm-2">
            <?php echo $form->textFieldGroup($model,'id',array(
                'prepend' =>$form->checkboxGroup($model,'visible_id',array('inline'=>true,)),
                'widgetOptions'=>array('htmlOptions'=>array('disabled'=>$model->visible_id?false:true))
            )); ?>
        </div>

        <div class="col-sm-2">
            <?php echo $form->textFieldGroup($model,'text', array(
                'prepend' => $form->checkboxGroup($model,'visible_text',array('inline'=>true)),
                'widgetOptions'=>array('htmlOptions'=>array('disabled'=>$model->visible_text?false:true))
            )); ?>
        </div>

        <div class="col-sm-2">
            <?php echo $form->dropDownListGroup($model,'type',array(
                'prepend' => $form->checkboxGroup($model,'visible_type',array('inline'=>true)),
                'widgetOptions'=>array(
                    'data'=>Yii::app()->params['rule_array']['facebook'],
                    'htmlOptions'=>array(
                        'disabled'=>$model->visible_type?false:true,
                        'empty'=>'Choose One'
                    )
                ),
            ));?>
        </div>


        <div class="col-sm-2">
            <?php echo $form->datePickerGroup($model,'start_date',array(
                    'prepend'=>$form->checkboxGroup($model,'visible_start_date',array('inline'=>true)),
                    'widgetOptions'=>array(
                        'options' => array('language'=>'en','format'=>'yyyy-mm-dd','viewformat'=>'yyyy-mm-dd',),
                        'htmlOptions'=>array('disabled'=>$model->visible_start_date?false:true)
                    )
                )
            ); ?>
        </div>
        <div class="col-sm-2">
            <?php echo $form->datePickerGroup($model,'end_date',array(
                    'prepend'=>$form->checkboxGroup($model,'visible_end_date',array('inline'=>true)),
                    'widgetOptions'=>array(
                        'options' => array('language'=>'en','format'=>'yyyy-mm-dd','viewformat'=>'yyyy-mm-dd',),
                        'htmlOptions'=>array('disabled'=>$model->visible_end_date?false:true)
                    )
                )
            ); ?>
        </div>

        <div class="col-sm-2">
            <?php echo $form->textFieldGroup($model,'publish_time',array(
                    'prepend'=>$form->checkboxGroup($model,'visible_publish_time',array('inline'=>true)),
                    'widgetOptions'=>array(
                        'options' => array('language'=>'en','format'=>'yyyy-mm-dd','viewformat'=>'yyyy-mm-dd',),
                        'htmlOptions'=>array('disabled'=>$model->visible_publish_time?false:true,'readOnly'=>true)
                    )
                )
            ); ?>
        </div>

    </div>

   <div class="row">
       <div class="col-sm-2 col-sm-push-2">
           <?php echo $form->dropDownListGroup($model,'platform_id',array(
               'prepend' => $form->checkboxGroup($model,'visible_platform_id',array('inline'=>true)),
               'widgetOptions'=>array(
                   'data'=>Platform::model()->get_all_by_array(),
                   'htmlOptions'=>array(
                       'disabled'=>$model->visible_platform_id?false:true,
                       'empty'=>'Choose One'
                   )
               ),
           ));?>
       </div>

       <div class="col-sm-2 col-sm-push-2">
           <?php echo $form->datePickerGroup($model,'created_at',array(
                   'prepend'=>$form->checkboxGroup($model,'visible_created_at',array('inline'=>true)),
                   'widgetOptions'=>array(
                       'options' => array('language'=>'en','format'=>'yyyy-mm-dd','viewformat'=>'yyyy-mm-dd',),
                       'htmlOptions'=>array('disabled'=>$model->visible_created_at?false:true)
                   )
               )
           ); ?>
       </div>

       <div class="col-sm-2 col-sm-push-2">
           <?php echo $form->datePickerGroup($model,'last_generated',array(
                   'prepend'=>$form->checkboxGroup($model,'visible_last_generated',array('inline'=>true)),
                   'widgetOptions'=>array(
                       'options' => array('language'=>'en','format'=>'yyyy-mm-dd','viewformat'=>'yyyy-mm-dd',),
                       'htmlOptions'=>array('disabled'=>$model->visible_last_generated?false:true)
                   )
               )
           ); ?>
       </div>
       <div class="col-sm-2 col-sm-push-2">
           <?php
           echo $form->checkboxGroup(
               $model,
               'visible_media_url',
               array(
                   'inline'=>true,
                   //'labelOptions'=>array('class'=>'btn btn-primary'),
                   'widgetOptions'=>array(
                       'htmlOptions'=>array(
                           'style'=>''
                       ),
                   ),
                   'div_checkbox'=>array(
                       'style'=>'margin-top:27px;'
                   )
               )
           );



           ?>

       </div>
   </div>

    <div class="col-sm-2 pull-left page-sizes"  >

        <?php echo $form->dropDownListGroup(
            $model,
            'pagination_size',
            array(

                'widgetOptions'=>array(
                    'data'=>$model->pages_size(),
                    'htmlOptions'=>array(

                    ),
                ),
                'hint'=>''
            )
        ); ?>
    </div>

<?php $this->endWidget(); ?>